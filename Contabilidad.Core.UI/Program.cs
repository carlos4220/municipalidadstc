﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Contabilidad.Core.UI.Forms;
using Contabilidad.Core.Managers;

namespace Contabilidad.Core.UI
{
	static class Program
	{

		private static Manager manager;

		public static Manager getManager()
		{
			return manager;
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			manager = new Manager(Path.GetDirectoryName(Application.ExecutablePath));

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MenuPrincipal());
		}
	}
}
