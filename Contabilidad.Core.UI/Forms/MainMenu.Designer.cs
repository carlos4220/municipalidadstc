﻿namespace Contabilidad.Core.UI.Forms
{
	partial class MainMenu
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.Panel loadDummyTextPanel;
			System.Windows.Forms.Panel loadDummyPanel;
			this.waitingText = new System.Windows.Forms.Label();
			this.waitingProgress = new System.Windows.Forms.ProgressBar();
			this.loadPanel = new System.Windows.Forms.Panel();
			this.mainPanel = new System.Windows.Forms.Panel();
			this.mainFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.buttonDiario = new System.Windows.Forms.Button();
			this.buttonMayor = new System.Windows.Forms.Button();
			this.buttonSaldos = new System.Windows.Forms.Button();
			this.buttonNomenclatura = new System.Windows.Forms.Button();
			this.buttonPartidas = new System.Windows.Forms.Button();
			this.buttonPolizas = new System.Windows.Forms.Button();
			this.buttonBalanceGeneral = new System.Windows.Forms.Button();
			loadDummyTextPanel = new System.Windows.Forms.Panel();
			loadDummyPanel = new System.Windows.Forms.Panel();
			loadDummyTextPanel.SuspendLayout();
			loadDummyPanel.SuspendLayout();
			this.loadPanel.SuspendLayout();
			this.mainPanel.SuspendLayout();
			this.mainFlowPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// loadDummyTextPanel
			// 
			loadDummyTextPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
			loadDummyTextPanel.Controls.Add(this.waitingText);
			loadDummyTextPanel.Location = new System.Drawing.Point(80, 85);
			loadDummyTextPanel.Name = "loadDummyTextPanel";
			loadDummyTextPanel.Size = new System.Drawing.Size(200, 28);
			loadDummyTextPanel.TabIndex = 2;
			// 
			// waitingText
			// 
			this.waitingText.Dock = System.Windows.Forms.DockStyle.Fill;
			this.waitingText.Location = new System.Drawing.Point(0, 0);
			this.waitingText.Name = "waitingText";
			this.waitingText.Size = new System.Drawing.Size(200, 28);
			this.waitingText.TabIndex = 1;
			this.waitingText.Text = "Iniciando...";
			this.waitingText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// loadDummyPanel
			// 
			loadDummyPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
			loadDummyPanel.Controls.Add(loadDummyTextPanel);
			loadDummyPanel.Controls.Add(this.waitingProgress);
			loadDummyPanel.Location = new System.Drawing.Point(191, 109);
			loadDummyPanel.Name = "loadDummyPanel";
			loadDummyPanel.Size = new System.Drawing.Size(360, 259);
			loadDummyPanel.TabIndex = 3;
			// 
			// waitingProgress
			// 
			this.waitingProgress.Location = new System.Drawing.Point(80, 119);
			this.waitingProgress.MarqueeAnimationSpeed = 50;
			this.waitingProgress.Name = "waitingProgress";
			this.waitingProgress.Size = new System.Drawing.Size(200, 20);
			this.waitingProgress.Step = 1;
			this.waitingProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
			this.waitingProgress.TabIndex = 0;
			// 
			// loadPanel
			// 
			this.loadPanel.BackColor = System.Drawing.SystemColors.Control;
			this.loadPanel.Controls.Add(loadDummyPanel);
			this.loadPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.loadPanel.Location = new System.Drawing.Point(0, 0);
			this.loadPanel.Name = "loadPanel";
			this.loadPanel.Size = new System.Drawing.Size(742, 476);
			this.loadPanel.TabIndex = 0;
			// 
			// mainPanel
			// 
			this.mainPanel.Controls.Add(this.mainFlowPanel);
			this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainPanel.Location = new System.Drawing.Point(0, 0);
			this.mainPanel.Name = "mainPanel";
			this.mainPanel.Size = new System.Drawing.Size(742, 476);
			this.mainPanel.TabIndex = 3;
			this.mainPanel.Visible = false;
			// 
			// mainFlowPanel
			// 
			this.mainFlowPanel.Controls.Add(this.buttonDiario);
			this.mainFlowPanel.Controls.Add(this.buttonMayor);
			this.mainFlowPanel.Controls.Add(this.buttonSaldos);
			this.mainFlowPanel.Controls.Add(this.buttonNomenclatura);
			this.mainFlowPanel.Controls.Add(this.buttonPartidas);
			this.mainFlowPanel.Controls.Add(this.buttonPolizas);
			this.mainFlowPanel.Controls.Add(this.buttonBalanceGeneral);
			this.mainFlowPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainFlowPanel.Location = new System.Drawing.Point(0, 0);
			this.mainFlowPanel.Name = "mainFlowPanel";
			this.mainFlowPanel.Size = new System.Drawing.Size(742, 476);
			this.mainFlowPanel.TabIndex = 5;
			// 
			// buttonDiario
			// 
			this.buttonDiario.Location = new System.Drawing.Point(15, 15);
			this.buttonDiario.Margin = new System.Windows.Forms.Padding(15);
			this.buttonDiario.Name = "buttonDiario";
			this.buttonDiario.Size = new System.Drawing.Size(150, 26);
			this.buttonDiario.TabIndex = 0;
			this.buttonDiario.Text = "Libro Diario";
			this.buttonDiario.UseVisualStyleBackColor = true;
			this.buttonDiario.Click += new System.EventHandler(this.buttonDiario_Click);
			// 
			// buttonMayor
			// 
			this.buttonMayor.Location = new System.Drawing.Point(195, 15);
			this.buttonMayor.Margin = new System.Windows.Forms.Padding(15);
			this.buttonMayor.Name = "buttonMayor";
			this.buttonMayor.Size = new System.Drawing.Size(150, 26);
			this.buttonMayor.TabIndex = 1;
			this.buttonMayor.Text = "Libro Mayor";
			this.buttonMayor.UseVisualStyleBackColor = true;
			this.buttonMayor.Click += new System.EventHandler(this.buttonMayor_Click);
			// 
			// buttonSaldos
			// 
			this.buttonSaldos.Location = new System.Drawing.Point(375, 15);
			this.buttonSaldos.Margin = new System.Windows.Forms.Padding(15);
			this.buttonSaldos.Name = "buttonSaldos";
			this.buttonSaldos.Size = new System.Drawing.Size(150, 26);
			this.buttonSaldos.TabIndex = 2;
			this.buttonSaldos.Text = "Balance de Saldos";
			this.buttonSaldos.UseVisualStyleBackColor = true;
			this.buttonSaldos.Click += new System.EventHandler(this.buttonSaldos_Click);
			// 
			// buttonNomenclatura
			// 
			this.buttonNomenclatura.Location = new System.Drawing.Point(555, 15);
			this.buttonNomenclatura.Margin = new System.Windows.Forms.Padding(15);
			this.buttonNomenclatura.Name = "buttonNomenclatura";
			this.buttonNomenclatura.Size = new System.Drawing.Size(150, 26);
			this.buttonNomenclatura.TabIndex = 3;
			this.buttonNomenclatura.Text = "Nomenclatura";
			this.buttonNomenclatura.UseVisualStyleBackColor = true;
			this.buttonNomenclatura.Click += new System.EventHandler(this.buttonNomenclatura_Click);
			// 
			// buttonPartidas
			// 
			this.buttonPartidas.Location = new System.Drawing.Point(15, 71);
			this.buttonPartidas.Margin = new System.Windows.Forms.Padding(15);
			this.buttonPartidas.Name = "buttonPartidas";
			this.buttonPartidas.Size = new System.Drawing.Size(150, 26);
			this.buttonPartidas.TabIndex = 4;
			this.buttonPartidas.Text = "Partidas";
			this.buttonPartidas.UseVisualStyleBackColor = true;
			this.buttonPartidas.Click += new System.EventHandler(this.buttonPartidas_Click);
			// 
			// buttonPolizas
			// 
			this.buttonPolizas.Location = new System.Drawing.Point(195, 71);
			this.buttonPolizas.Margin = new System.Windows.Forms.Padding(15);
			this.buttonPolizas.Name = "buttonPolizas";
			this.buttonPolizas.Size = new System.Drawing.Size(150, 26);
			this.buttonPolizas.TabIndex = 5;
			this.buttonPolizas.Text = "Polizas";
			this.buttonPolizas.UseVisualStyleBackColor = true;
			this.buttonPolizas.Click += new System.EventHandler(this.buttonPolizas_Click);
			// 
			// buttonBalanceGeneral
			// 
			this.buttonBalanceGeneral.Location = new System.Drawing.Point(375, 71);
			this.buttonBalanceGeneral.Margin = new System.Windows.Forms.Padding(15);
			this.buttonBalanceGeneral.Name = "buttonBalanceGeneral";
			this.buttonBalanceGeneral.Size = new System.Drawing.Size(150, 26);
			this.buttonBalanceGeneral.TabIndex = 6;
			this.buttonBalanceGeneral.Text = "Balance General";
			this.buttonBalanceGeneral.UseVisualStyleBackColor = true;
			this.buttonBalanceGeneral.Click += new System.EventHandler(this.buttonBalanceGeneral_Click);
			// 
			// MainMenu
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(742, 476);
			this.Controls.Add(this.loadPanel);
			this.Controls.Add(this.mainPanel);
			this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.MinimumSize = new System.Drawing.Size(395, 313);
			this.Name = "MainMenu";
			this.Text = "Menu Principal";
			this.Click += new System.EventHandler(this.buttonPartidas_Click);
			loadDummyTextPanel.ResumeLayout(false);
			loadDummyPanel.ResumeLayout(false);
			this.loadPanel.ResumeLayout(false);
			this.mainPanel.ResumeLayout(false);
			this.mainFlowPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel loadPanel;
		private System.Windows.Forms.ProgressBar waitingProgress;
		private System.Windows.Forms.Label waitingText;
		private System.Windows.Forms.Panel mainPanel;
		private System.Windows.Forms.Button buttonNomenclatura;
		private System.Windows.Forms.Button buttonSaldos;
		private System.Windows.Forms.Button buttonMayor;
		private System.Windows.Forms.Button buttonDiario;
		private System.Windows.Forms.FlowLayoutPanel mainFlowPanel;
		private System.Windows.Forms.Button buttonPartidas;
		private System.Windows.Forms.Button buttonPolizas;
		private System.Windows.Forms.Button buttonBalanceGeneral;
	}
}