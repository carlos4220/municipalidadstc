﻿namespace Contabilidad.Core.UI.Forms
{
    partial class ModificarYEliminarPoliza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModificarYEliminarPoliza));
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnFiltrar = new System.Windows.Forms.Button();
			this.FechaHasta = new System.Windows.Forms.DateTimePicker();
			this.label24 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.FechaDesde = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.dgvPolizas = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.btnUpdatePoliza = new System.Windows.Forms.Button();
			this.txtNumberPoliza = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.btnRemove = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.label11 = new System.Windows.Forms.Label();
			this.txtLevel1 = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.txtLevel2 = new System.Windows.Forms.TextBox();
			this.txtLevel3 = new System.Windows.Forms.TextBox();
			this.txtLevel4 = new System.Windows.Forms.TextBox();
			this.txtAmount = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.txtNameAccount = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.cmbTipoSaldo = new System.Windows.Forms.ComboBox();
			this.label12 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.txtNumber = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.btnEliminarPoliza = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.label22 = new System.Windows.Forms.Label();
			this.txtTotalAcreedor = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.txtTotalDeudor = new System.Windows.Forms.TextBox();
			this.dgvAccountsAcreedor = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label18 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.dgvAccountsDeudor = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.DateTimePoliza = new System.Windows.Forms.DateTimePicker();
			this.label20 = new System.Windows.Forms.Label();
			this.Number = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.txtQuienAutoriza = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.txtDescription = new System.Windows.Forms.TextBox();
			this.cmbTipoPoliza = new System.Windows.Forms.ComboBox();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvPolizas)).BeginInit();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAccountsAcreedor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvAccountsDeudor)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnFiltrar);
			this.panel1.Controls.Add(this.FechaHasta);
			this.panel1.Controls.Add(this.label24);
			this.panel1.Controls.Add(this.label23);
			this.panel1.Controls.Add(this.FechaDesde);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.dgvPolizas);
			this.panel1.Controls.Add(this.btnUpdatePoliza);
			this.panel1.Controls.Add(this.txtNumberPoliza);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Location = new System.Drawing.Point(10, 10);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1224, 237);
			this.panel1.TabIndex = 0;
			// 
			// btnFiltrar
			// 
			this.btnFiltrar.Location = new System.Drawing.Point(1133, 93);
			this.btnFiltrar.Name = "btnFiltrar";
			this.btnFiltrar.Size = new System.Drawing.Size(75, 23);
			this.btnFiltrar.TabIndex = 7;
			this.btnFiltrar.Text = "Filtrar";
			this.btnFiltrar.UseVisualStyleBackColor = true;
			this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
			// 
			// FechaHasta
			// 
			this.FechaHasta.CustomFormat = "dd/MM/yyy";
			this.FechaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.FechaHasta.Location = new System.Drawing.Point(1021, 96);
			this.FechaHasta.Name = "FechaHasta";
			this.FechaHasta.Size = new System.Drawing.Size(99, 23);
			this.FechaHasta.TabIndex = 6;
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label24.Location = new System.Drawing.Point(1041, 64);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(41, 17);
			this.label24.TabIndex = 5;
			this.label24.Text = "Hasta";
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label23.Location = new System.Drawing.Point(910, 64);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(45, 17);
			this.label23.TabIndex = 4;
			this.label23.Text = "Desde";
			// 
			// FechaDesde
			// 
			this.FechaDesde.CustomFormat = "dd/MM/yyy";
			this.FechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.FechaDesde.Location = new System.Drawing.Point(890, 96);
			this.FechaDesde.Name = "FechaDesde";
			this.FechaDesde.Size = new System.Drawing.Size(99, 23);
			this.FechaDesde.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(950, 47);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(93, 17);
			this.label2.TabIndex = 2;
			this.label2.Text = "Filtro de Fecha";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(872, 175);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(117, 17);
			this.label3.TabIndex = 1;
			this.label3.Text = "Numero de Poliza ";
			// 
			// dgvPolizas
			// 
			this.dgvPolizas.AllowUserToAddRows = false;
			this.dgvPolizas.AllowUserToDeleteRows = false;
			this.dgvPolizas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvPolizas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column5});
			this.dgvPolizas.Location = new System.Drawing.Point(13, 64);
			this.dgvPolizas.MultiSelect = false;
			this.dgvPolizas.Name = "dgvPolizas";
			this.dgvPolizas.ReadOnly = true;
			this.dgvPolizas.Size = new System.Drawing.Size(843, 170);
			this.dgvPolizas.TabIndex = 1;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "Numero";
			this.Column1.Name = "Column1";
			this.Column1.ReadOnly = true;
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Fecha";
			this.Column2.Name = "Column2";
			this.Column2.ReadOnly = true;
			// 
			// Column3
			// 
			this.Column3.HeaderText = "Descripcion";
			this.Column3.Name = "Column3";
			this.Column3.ReadOnly = true;
			this.Column3.Width = 350;
			// 
			// Column5
			// 
			this.Column5.HeaderText = "Redactador";
			this.Column5.Name = "Column5";
			this.Column5.ReadOnly = true;
			this.Column5.Width = 250;
			// 
			// btnUpdatePoliza
			// 
			this.btnUpdatePoliza.Location = new System.Drawing.Point(1133, 175);
			this.btnUpdatePoliza.Name = "btnUpdatePoliza";
			this.btnUpdatePoliza.Size = new System.Drawing.Size(75, 23);
			this.btnUpdatePoliza.TabIndex = 3;
			this.btnUpdatePoliza.Text = "Actualizar";
			this.btnUpdatePoliza.UseVisualStyleBackColor = true;
			this.btnUpdatePoliza.Click += new System.EventHandler(this.btnUpdatePoliza_Click);
			// 
			// txtNumberPoliza
			// 
			this.txtNumberPoliza.Location = new System.Drawing.Point(996, 175);
			this.txtNumberPoliza.Name = "txtNumberPoliza";
			this.txtNumberPoliza.Size = new System.Drawing.Size(100, 23);
			this.txtNumberPoliza.TabIndex = 2;
			this.txtNumberPoliza.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(405, 4);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(177, 30);
			this.label1.TabIndex = 0;
			this.label1.Text = "Listado de Polizas";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(125, 212);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(200, 17);
			this.label4.TabIndex = 75;
			this.label4.Text = "Operaciones Sobre Movimientos";
			// 
			// btnRemove
			// 
			this.btnRemove.Location = new System.Drawing.Point(221, 249);
			this.btnRemove.Name = "btnRemove";
			this.btnRemove.Size = new System.Drawing.Size(75, 23);
			this.btnRemove.TabIndex = 74;
			this.btnRemove.Text = "Eliminar";
			this.btnRemove.UseVisualStyleBackColor = true;
			this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(128, 249);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(75, 23);
			this.btnAdd.TabIndex = 73;
			this.btnAdd.Text = "Agregar";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(50, 97);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(49, 15);
			this.label11.TabIndex = 72;
			this.label11.Text = "Codigo:";
			// 
			// txtLevel1
			// 
			this.txtLevel1.Location = new System.Drawing.Point(105, 94);
			this.txtLevel1.Name = "txtLevel1";
			this.txtLevel1.Size = new System.Drawing.Size(29, 23);
			this.txtLevel1.TabIndex = 59;
			this.txtLevel1.Text = "0";
			this.txtLevel1.TextChanged += new System.EventHandler(this.txtLevel1_TextChanged);
			this.txtLevel1.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(191, 97);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(12, 15);
			this.label6.TabIndex = 71;
			this.label6.Text = "-";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(242, 97);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(12, 15);
			this.label9.TabIndex = 70;
			this.label9.Text = "-";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(140, 97);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(12, 15);
			this.label10.TabIndex = 69;
			this.label10.Text = "-";
			// 
			// txtLevel2
			// 
			this.txtLevel2.Location = new System.Drawing.Point(156, 94);
			this.txtLevel2.Name = "txtLevel2";
			this.txtLevel2.Size = new System.Drawing.Size(29, 23);
			this.txtLevel2.TabIndex = 60;
			this.txtLevel2.Text = "0";
			this.txtLevel2.TextChanged += new System.EventHandler(this.txtLevel1_TextChanged);
			this.txtLevel2.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// txtLevel3
			// 
			this.txtLevel3.Location = new System.Drawing.Point(207, 94);
			this.txtLevel3.Name = "txtLevel3";
			this.txtLevel3.Size = new System.Drawing.Size(29, 23);
			this.txtLevel3.TabIndex = 61;
			this.txtLevel3.Text = "0";
			this.txtLevel3.TextChanged += new System.EventHandler(this.txtLevel1_TextChanged);
			this.txtLevel3.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// txtLevel4
			// 
			this.txtLevel4.Location = new System.Drawing.Point(258, 94);
			this.txtLevel4.Name = "txtLevel4";
			this.txtLevel4.Size = new System.Drawing.Size(29, 23);
			this.txtLevel4.TabIndex = 62;
			this.txtLevel4.Text = "0";
			this.txtLevel4.TextChanged += new System.EventHandler(this.txtLevel1_TextChanged);
			this.txtLevel4.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// txtAmount
			// 
			this.txtAmount.Location = new System.Drawing.Point(105, 178);
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Size = new System.Drawing.Size(182, 23);
			this.txtAmount.TabIndex = 66;
			this.txtAmount.Text = "0";
			this.txtAmount.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(53, 181);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(46, 15);
			this.label5.TabIndex = 68;
			this.label5.Text = "Monto:";
			// 
			// txtNameAccount
			// 
			this.txtNameAccount.Location = new System.Drawing.Point(105, 138);
			this.txtNameAccount.Name = "txtNameAccount";
			this.txtNameAccount.Size = new System.Drawing.Size(339, 23);
			this.txtNameAccount.TabIndex = 63;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(51, 141);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(48, 15);
			this.label7.TabIndex = 67;
			this.label7.Text = "Cuenta:";
			// 
			// cmbTipoSaldo
			// 
			this.cmbTipoSaldo.FormattingEnabled = true;
			this.cmbTipoSaldo.Location = new System.Drawing.Point(105, 46);
			this.cmbTipoSaldo.Name = "cmbTipoSaldo";
			this.cmbTipoSaldo.Size = new System.Drawing.Size(139, 21);
			this.cmbTipoSaldo.TabIndex = 58;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(17, 49);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(82, 15);
			this.label12.TabIndex = 65;
			this.label12.Text = "Tipo de Saldo:";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(125, 5);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(140, 17);
			this.label8.TabIndex = 64;
			this.label8.Text = "Datos de Movimientos";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.txtNumber);
			this.panel2.Controls.Add(this.button2);
			this.panel2.Controls.Add(this.btnEliminarPoliza);
			this.panel2.Controls.Add(this.btnSave);
			this.panel2.Controls.Add(this.label22);
			this.panel2.Controls.Add(this.txtTotalAcreedor);
			this.panel2.Controls.Add(this.label17);
			this.panel2.Controls.Add(this.txtTotalDeudor);
			this.panel2.Controls.Add(this.dgvAccountsAcreedor);
			this.panel2.Controls.Add(this.label18);
			this.panel2.Controls.Add(this.label21);
			this.panel2.Controls.Add(this.dgvAccountsDeudor);
			this.panel2.Controls.Add(this.DateTimePoliza);
			this.panel2.Controls.Add(this.label20);
			this.panel2.Controls.Add(this.Number);
			this.panel2.Controls.Add(this.label19);
			this.panel2.Controls.Add(this.txtQuienAutoriza);
			this.panel2.Controls.Add(this.label14);
			this.panel2.Controls.Add(this.label13);
			this.panel2.Controls.Add(this.txtDescription);
			this.panel2.Controls.Add(this.cmbTipoPoliza);
			this.panel2.Controls.Add(this.label15);
			this.panel2.Controls.Add(this.label16);
			this.panel2.Controls.Add(this.cmbTipoSaldo);
			this.panel2.Controls.Add(this.label4);
			this.panel2.Controls.Add(this.btnRemove);
			this.panel2.Controls.Add(this.btnAdd);
			this.panel2.Controls.Add(this.label11);
			this.panel2.Controls.Add(this.label8);
			this.panel2.Controls.Add(this.txtLevel1);
			this.panel2.Controls.Add(this.label12);
			this.panel2.Controls.Add(this.label6);
			this.panel2.Controls.Add(this.label7);
			this.panel2.Controls.Add(this.label9);
			this.panel2.Controls.Add(this.txtNameAccount);
			this.panel2.Controls.Add(this.label10);
			this.panel2.Controls.Add(this.label5);
			this.panel2.Controls.Add(this.txtLevel2);
			this.panel2.Controls.Add(this.txtAmount);
			this.panel2.Controls.Add(this.txtLevel3);
			this.panel2.Controls.Add(this.txtLevel4);
			this.panel2.Location = new System.Drawing.Point(10, 253);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1224, 504);
			this.panel2.TabIndex = 76;
			// 
			// txtNumber
			// 
			this.txtNumber.AutoSize = true;
			this.txtNumber.Location = new System.Drawing.Point(639, 38);
			this.txtNumber.Name = "txtNumber";
			this.txtNumber.Size = new System.Drawing.Size(0, 15);
			this.txtNumber.TabIndex = 100;
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(1090, 88);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(103, 32);
			this.button2.TabIndex = 99;
			this.button2.Text = "Imprimir Poliza";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// btnEliminarPoliza
			// 
			this.btnEliminarPoliza.Location = new System.Drawing.Point(1090, 28);
			this.btnEliminarPoliza.Name = "btnEliminarPoliza";
			this.btnEliminarPoliza.Size = new System.Drawing.Size(103, 33);
			this.btnEliminarPoliza.TabIndex = 98;
			this.btnEliminarPoliza.Text = "Eliminar Poliza";
			this.btnEliminarPoliza.UseVisualStyleBackColor = true;
			this.btnEliminarPoliza.Click += new System.EventHandler(this.btnEliminarPoliza_Click);
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(1090, 439);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(100, 33);
			this.btnSave.TabIndex = 97;
			this.btnSave.Text = "Guardar Poliza";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(82, 482);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(33, 15);
			this.label22.TabIndex = 96;
			this.label22.Text = "Total";
			// 
			// txtTotalAcreedor
			// 
			this.txtTotalAcreedor.Location = new System.Drawing.Point(665, 479);
			this.txtTotalAcreedor.Name = "txtTotalAcreedor";
			this.txtTotalAcreedor.Size = new System.Drawing.Size(173, 23);
			this.txtTotalAcreedor.TabIndex = 95;
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(632, 482);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(33, 15);
			this.label17.TabIndex = 94;
			this.label17.Text = "Total";
			// 
			// txtTotalDeudor
			// 
			this.txtTotalDeudor.Location = new System.Drawing.Point(116, 479);
			this.txtTotalDeudor.Name = "txtTotalDeudor";
			this.txtTotalDeudor.Size = new System.Drawing.Size(171, 23);
			this.txtTotalDeudor.TabIndex = 93;
			// 
			// dgvAccountsAcreedor
			// 
			this.dgvAccountsAcreedor.AllowUserToAddRows = false;
			this.dgvAccountsAcreedor.AllowUserToDeleteRows = false;
			this.dgvAccountsAcreedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvAccountsAcreedor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
			this.dgvAccountsAcreedor.Location = new System.Drawing.Point(575, 322);
			this.dgvAccountsAcreedor.MultiSelect = false;
			this.dgvAccountsAcreedor.Name = "dgvAccountsAcreedor";
			this.dgvAccountsAcreedor.ReadOnly = true;
			this.dgvAccountsAcreedor.Size = new System.Drawing.Size(494, 150);
			this.dgvAccountsAcreedor.TabIndex = 92;
			// 
			// dataGridViewTextBoxColumn1
			// 
			this.dataGridViewTextBoxColumn1.HeaderText = "Codigo";
			this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
			this.dataGridViewTextBoxColumn1.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn2
			// 
			this.dataGridViewTextBoxColumn2.HeaderText = "Cuenta";
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			this.dataGridViewTextBoxColumn2.ReadOnly = true;
			this.dataGridViewTextBoxColumn2.Width = 200;
			// 
			// dataGridViewTextBoxColumn3
			// 
			this.dataGridViewTextBoxColumn3.HeaderText = "Monto";
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.ReadOnly = true;
			this.dataGridViewTextBoxColumn3.Width = 150;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(722, 295);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(120, 15);
			this.label18.TabIndex = 91;
			this.label18.Text = "Cuentas (ACREEDOR)";
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(167, 295);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(107, 15);
			this.label21.TabIndex = 90;
			this.label21.Text = "Cuentas (DEUDOR)";
			// 
			// dgvAccountsDeudor
			// 
			this.dgvAccountsDeudor.AllowUserToAddRows = false;
			this.dgvAccountsDeudor.AllowUserToDeleteRows = false;
			this.dgvAccountsDeudor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvAccountsDeudor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
			this.dgvAccountsDeudor.Location = new System.Drawing.Point(32, 322);
			this.dgvAccountsDeudor.MultiSelect = false;
			this.dgvAccountsDeudor.Name = "dgvAccountsDeudor";
			this.dgvAccountsDeudor.ReadOnly = true;
			this.dgvAccountsDeudor.Size = new System.Drawing.Size(494, 150);
			this.dgvAccountsDeudor.TabIndex = 89;
			// 
			// dataGridViewTextBoxColumn4
			// 
			this.dataGridViewTextBoxColumn4.HeaderText = "Codigo";
			this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
			this.dataGridViewTextBoxColumn4.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn5
			// 
			this.dataGridViewTextBoxColumn5.HeaderText = "Cuenta";
			this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
			this.dataGridViewTextBoxColumn5.ReadOnly = true;
			this.dataGridViewTextBoxColumn5.Width = 200;
			// 
			// dataGridViewTextBoxColumn6
			// 
			this.dataGridViewTextBoxColumn6.HeaderText = "Monto";
			this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
			this.dataGridViewTextBoxColumn6.ReadOnly = true;
			this.dataGridViewTextBoxColumn6.Width = 150;
			// 
			// DateTimePoliza
			// 
			this.DateTimePoliza.CustomFormat = "dd/MM/yyy";
			this.DateTimePoliza.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.DateTimePoliza.Location = new System.Drawing.Point(623, 251);
			this.DateTimePoliza.Name = "DateTimePoliza";
			this.DateTimePoliza.Size = new System.Drawing.Size(200, 23);
			this.DateTimePoliza.TabIndex = 88;
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(576, 254);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(41, 15);
			this.label20.TabIndex = 87;
			this.label20.Text = "Fecha:";
			// 
			// Number
			// 
			this.Number.AutoSize = true;
			this.Number.Location = new System.Drawing.Point(623, 38);
			this.Number.Name = "Number";
			this.Number.Size = new System.Drawing.Size(0, 15);
			this.Number.TabIndex = 86;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(531, 38);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(101, 15);
			this.label19.TabIndex = 85;
			this.label19.Text = "Numero de Poliza";
			// 
			// txtQuienAutoriza
			// 
			this.txtQuienAutoriza.Location = new System.Drawing.Point(624, 217);
			this.txtQuienAutoriza.Name = "txtQuienAutoriza";
			this.txtQuienAutoriza.Size = new System.Drawing.Size(292, 23);
			this.txtQuienAutoriza.TabIndex = 84;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(529, 220);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(89, 15);
			this.label14.TabIndex = 83;
			this.label14.Text = "Quien Autoriza:";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(545, 129);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(72, 15);
			this.label13.TabIndex = 82;
			this.label13.Text = "Descripcion:";
			// 
			// txtDescription
			// 
			this.txtDescription.Location = new System.Drawing.Point(623, 126);
			this.txtDescription.Multiline = true;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(293, 72);
			this.txtDescription.TabIndex = 81;
			// 
			// cmbTipoPoliza
			// 
			this.cmbTipoPoliza.FormattingEnabled = true;
			this.cmbTipoPoliza.Location = new System.Drawing.Point(623, 82);
			this.cmbTipoPoliza.Name = "cmbTipoPoliza";
			this.cmbTipoPoliza.Size = new System.Drawing.Size(139, 21);
			this.cmbTipoPoliza.TabIndex = 80;
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(533, 85);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(84, 15);
			this.label15.TabIndex = 79;
			this.label15.Text = "Tipo de Poliza:";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label16.Location = new System.Drawing.Point(631, 1);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(99, 17);
			this.label16.TabIndex = 78;
			this.label16.Text = "Datos de Poliza";
			// 
			// ModificarYEliminarPoliza
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1238, 764);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Font = new System.Drawing.Font("Segoe UI", 8.5F);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "ModificarYEliminarPoliza";
			this.Text = "Modificar y Eliminar Polizas";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvPolizas)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAccountsAcreedor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvAccountsDeudor)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker FechaDesde;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvPolizas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNumberPoliza;
        private System.Windows.Forms.Button btnUpdatePoliza;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtLevel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtLevel2;
        private System.Windows.Forms.TextBox txtLevel3;
        private System.Windows.Forms.TextBox txtLevel4;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNameAccount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbTipoSaldo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker DateTimePoliza;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label Number;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtQuienAutoriza;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.ComboBox cmbTipoPoliza;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtTotalAcreedor;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtTotalDeudor;
        private System.Windows.Forms.DataGridView dgvAccountsAcreedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridView dgvAccountsDeudor;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DateTimePicker FechaHasta;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.Button btnEliminarPoliza;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label txtNumber;
    }
}