﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class LibroDiario : Form
	{
		BindingList<Accounting_Party> AccountingParty;

		private BindingList<Account> Accounts; //Lista de cuentas

		public LibroDiario()
		{
			InitializeComponent();

			var m = Program.getManager();
			//Cargar los datos del Archivo a la List AccountingParty
			AccountingParty = m.GetDepartureManager().GetDepartures();
			Accounts = m.GetNomenclatureManager().GetAccounts();
		}

		private void btnActualizarLibro_Click(object sender, EventArgs e)
		{
			dgvLibroDiario.Rows.Clear();//limpia la dgv antes de actualizar los datos
			int Filas = 0;

			for (int i = 0; i < AccountingParty.Count; i++) //para recorrer todas las partidas 
			{
				decimal SaldoDebe = 0;  //acumula saldo debe
				decimal SaldoHaber = 0; //acumula saldo haber

				if (DateTime.Compare(FechaDesde.Value, AccountingParty[i].Date) < 0 && //evalua si cumple la partida los requisitos
					DateTime.Compare(AccountingParty[i].Date, FechaHasta.Value) < 0)   //de fecha
				{
					dgvLibroDiario.Rows.Add();
					dgvLibroDiario.Rows[Filas].Cells[0].Value = "Pda " + AccountingParty[i].Number;
					dgvLibroDiario.Rows[Filas].Cells[1].Value = AccountingParty[i].Date;

					foreach (var value in AccountingParty[i].Debe)  //recorre todos los movimientos
					{                                                                           //de la partida
						dgvLibroDiario.Rows.Add();
						Filas++;

						foreach (var Account in Accounts)
						{
							if (Account.Code == value.Account.Code)
							{
								dgvLibroDiario.Rows[Filas].Cells[1].Value = Account.Description;
							}
						}
						dgvLibroDiario.Rows[Filas].Cells[2].Value = value.Debe;
						SaldoDebe += value.Debe;
					}
					foreach (var value in AccountingParty[i].Haber)  //recorre todos los movimientos
					{                                                                           //de la partida
						dgvLibroDiario.Rows.Add();
						Filas++;

						foreach (var Account in Accounts)
						{
							if (Account.Code == value.Account.Code)
							{
								dgvLibroDiario.Rows[Filas].Cells[1].Value = Account.Description;
							}
						}
						dgvLibroDiario.Rows[Filas].Cells[3].Value = value.Haber;
						SaldoHaber += value.Haber;
					}
					dgvLibroDiario.Rows.Add();
					Filas++;
					dgvLibroDiario.Rows[Filas].Cells[1].Value = "TOTALES";
					dgvLibroDiario.Rows[Filas].Cells[2].Value = SaldoDebe;
					dgvLibroDiario.Rows[Filas].Cells[3].Value = SaldoHaber;
					dgvLibroDiario.Rows.Add();
					Filas++;
				}
			}
		}
	}
}
