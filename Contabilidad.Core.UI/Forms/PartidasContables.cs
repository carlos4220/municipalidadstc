﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class PartidasContables : Form
	{
		BindingList<Accounting_Party> AccountingItems;//lista para partidas
		private BindingList<Account> Accounts; //Lista de cuentas
		private List<AccountingMovement> MovementsDebe; //movimientos de saldo Deudor
		private List<AccountingMovement> MovementsHaber;  //Movimiento de saldo acreedor

		private AccountingMovement Movement; //Para llenar los datos de movimiento
		private Accounting_Party Departure;  //Para llenar los datos de partida;

        private Usuario Usuario;

        private ErrorProvider Error = new ErrorProvider(); //para mostrar errores

		private bool ExistsAccount = false; //para evaluar si existe la cuenta
		private int IdAccount = 0; //Id de la cuenta que estamos trabajando

		public PartidasContables()
		{
			InitializeComponent();
			var m = Program.getManager();

			AccountingItems = m.GetDepartureManager().GetDepartures();
			//cargar partidas a la lista accountingItems
			Accounts = m.GetNomenclatureManager().GetAccounts();
            //cargar las cuentas a la lista Accounts

            this.Usuario = m.GetUserManager().GetCurrentUser();

            MovementsDebe = new List<AccountingMovement>();
			MovementsHaber = new List<AccountingMovement>();

			if (AccountingItems.Count == 0)
				Number.Text = "1";
			else
				Number.Text = (AccountingItems.Last().Number + 1).ToString();

			cmbTypeAccount.DataSource = Enum.GetValues(typeof(TypeBalance));
		}

		private void UpdateTotales(List<AccountingMovement> Debe, List<AccountingMovement> Haber)
		{
			decimal TotalDebe = 0;
			decimal TotalHaber = 0;

			foreach (AccountingMovement Value in Debe)
				if (Value.Debe != 0)
					TotalDebe += Value.Debe;

			foreach (AccountingMovement Value in Haber)
				if (Value.Haber != 0)
					TotalHaber += Value.Haber;

			txtTotalHaber.Text = TotalHaber.ToString();
			txtTotalDebe.Text = TotalDebe.ToString();
		}

		private string UpdateAccount()//actualiza el nombre de la cuenta con respecto al codigo
		{
			IdAccount = 0;
			ExistsAccount = false;

			string Level1, Level2, Level3, Level4;

			txtNameAccount.Text = "";

			Level1 = txtLevel1.Text;
			Level2 = txtLevel2.Text;
			Level3 = txtLevel3.Text;
			Level4 = txtLevel4.Text;

			string Code = Level1 + "." + Level2 + "." + Level3 + "." + Level4;
			for (int i = 0; i < Accounts.Count; i++)
				if (Code == Accounts[i].Code)
				{
					txtNameAccount.Text = Accounts[i].Description;
					IdAccount = i;
					ExistsAccount = true;
					break;
				}
			return Code;
		}

		private string SearchAccount()
		{
			string Code = UpdateAccount();
			ExistsAccount = false;

			foreach (AccountingMovement Movement in MovementsDebe)
				if (Code == Movement.Account.Code)
				{
					ExistsAccount = true;
					break;
				}
			foreach (AccountingMovement Movement in MovementsHaber)
			
				if (Code == Movement.Account.Code)
				{
					ExistsAccount = true;
					break;
				}
			return Code;
		}

		private void btnAdd_Click(object sender, EventArgs e)//agregar un movimiento a la partida
		{
			UpdateAccount();

			Error.Clear();

			if (!ExistsAccount)
			{
				Error.SetError(txtLevel4, "No Existe Ninguna Cuenta Con Este Codigo");
				return;
			}
			if (decimal.Parse(txtAmount.Text) <= 0)
			{
				Error.SetError(txtAmount, "No Puede Tener Un Monto Menor o Igual a 0");
				return;
			}
			if ((TypeBalance)cmbTypeAccount.SelectedIndex != TypeBalance.Debe && (TypeBalance)cmbTypeAccount.SelectedIndex != TypeBalance.Haber)
			{
				Error.SetError(cmbTypeAccount, "Seleccione El Tipo De movimiento a ejecutar");
				return;
			}
			SearchAccount();
			if (ExistsAccount)
			{
				Error.SetError(txtLevel4, "Esta Cuenta Ya Ha Sido Agregada a Esta Partida");
				return;
			}

			// SI NO HAY ERRORES AGREGAR LAS CUENTAS A LA LISTA CORRESPONDIENTE, DEBE O HABER


			if ((TypeBalance)cmbTypeAccount.SelectedIndex == TypeBalance.Debe)
			{
				Movement = new AccountingMovement
				{
					Date = DateTime.Now,
					Debe = decimal.Parse(txtAmount.Text),
					Haber = 0,
					Account = Accounts[IdAccount]
				};

				dgvAccountsDebe.Rows.Add();
				dgvAccountsDebe.Rows[MovementsDebe.Count].Cells[0].Value = Movement.Account.Code;
				dgvAccountsDebe.Rows[MovementsDebe.Count].Cells[1].Value = Movement.Account.Description;
				dgvAccountsDebe.Rows[MovementsDebe.Count].Cells[2].Value = Movement.Debe;

				MovementsDebe.Add(Movement);

				Error.Clear();
			}
			if ((TypeBalance)cmbTypeAccount.SelectedIndex == TypeBalance.Haber)
			{
				Movement = new AccountingMovement
				{
					Date = DateTime.Now,
					Debe = 0,
					Haber = decimal.Parse(txtAmount.Text),
					Account = Accounts[IdAccount]
				};

				dgvAccountsHaber.Rows.Add();
				dgvAccountsHaber.Rows[MovementsHaber.Count].Cells[0].Value = Movement.Account.Code;
				dgvAccountsHaber.Rows[MovementsHaber.Count].Cells[1].Value = Movement.Account.Description;
				dgvAccountsHaber.Rows[MovementsHaber.Count].Cells[2].Value = Movement.Haber;

				MovementsHaber.Add(Movement);

				Error.Clear();

			}

			txtLevel1.Text = "0";
			txtLevel2.Text = "0";
			txtLevel3.Text = "0";
			txtLevel4.Text = "0";
			txtNameAccount.Clear();
			txtAmount.Text = "0";
			UpdateTotales(MovementsDebe, MovementsHaber);
		}

		private void txtLevel1_TextChanged(object sender, EventArgs e) => UpdateAccount();

		private void btnRemove_Click(object sender, EventArgs e)
		{
			string Code = SearchAccount();
			if (!ExistsAccount)
			{
				Error.SetError(txtLevel4, "Esta Cuenta No ha sido utilizada en la partida");
				return;
			}

			foreach (AccountingMovement Movement in MovementsDebe)
			{
				if (Code == Movement.Account.Code)
				{
					int Rows = 0;

					MovementsDebe.Remove(Movement);
					dgvAccountsDebe.Rows.Clear();
					foreach (AccountingMovement value in MovementsDebe)//actualiza la dgv despues de que se elimino la cuenta
					{
						dgvAccountsDebe.Rows.Add();
						dgvAccountsDebe.Rows[Rows].Cells[0].Value = value.Account.Code;
						dgvAccountsDebe.Rows[Rows].Cells[1].Value = value.Account.Description;
						dgvAccountsDebe.Rows[Rows].Cells[2].Value = value.Debe;
						Rows++;
					}
					Error.Clear();
					return;
				}
			}
			foreach (AccountingMovement Movement in MovementsHaber)
			{
				if (Code == Movement.Account.Code)
				{
					int Rows = 0;

					MovementsDebe.Remove(Movement);
					dgvAccountsHaber.Rows.Clear();
					foreach (AccountingMovement value in MovementsHaber)//actualiza la dgv despues de que se elimino la cuenta
					{
						dgvAccountsHaber.Rows.Add();
						dgvAccountsHaber.Rows[Rows].Cells[0].Value = value.Account.Code;
						dgvAccountsHaber.Rows[Rows].Cells[1].Value = value.Account.Description;
						dgvAccountsHaber.Rows[Rows].Cells[2].Value = value.Haber;
						Rows++;
					}
					Error.Clear();
					return;
				}
			}
			UpdateTotales(MovementsDebe, MovementsHaber);
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			Error.Clear();

			if (MovementsDebe.Count == 0)
			{
				Error.SetError(btnSave, "Una Partida Debe Contener Movimientos DEBE");
				return;
			}

			if (MovementsHaber.Count == 0)
			{
				Error.SetError(btnSave, "Una Partida Debe Contener Movimientos Haber");
				return;
			}

			UpdateTotales(MovementsDebe, MovementsHaber);//actualiza totales antes de evaluar

			if (txtTotalDebe.Text != txtTotalHaber.Text || txtTotalDebe.Text == string.Empty || txtTotalHaber.Text == string.Empty)
			{
				Error.SetError(btnSave, "Los Totales Son distintos o son iguales a 0");
				return;
			}

			if (txtDescription.Text == string.Empty)
			{
				Error.SetError(txtDescription, "Debe Existir Una descripcion");
				return;
			}

			Departure = new Accounting_Party //agrega datos de identidad a la partida
			{
				Date = DateTimePartida.Value,
				Description = txtDescription.Text
			};

			if (AccountingItems.Count == 0)
				Departure.Number = 1;
			else
				Departure.Number = AccountingItems.Last().Number + 1;

			Departure.Debe.AddRange(MovementsDebe);
			Departure.Haber.AddRange(MovementsHaber);

			//DEBE GUARDAR LA PARTIDA EN EL ARCHIVO DE TEXTO

			AccountingItems.Add(Departure);

			dgvAccountsDebe.Rows.Clear();   //LIMPIA FORMULARIO
			dgvAccountsHaber.Rows.Clear();
			txtLevel1.Text = "0";
			txtLevel2.Text = "0";
			txtLevel3.Text = "0";
			txtLevel4.Text = "0";
			txtNameAccount.Clear();
			txtAmount.Text = "0";
			txtDescription.Clear();
			txtTotalDebe.Clear();
			txtTotalHaber.Clear();

			MovementsDebe.Clear();//Limpia los Movimientos de debe
			MovementsHaber.Clear();//Limpia los movimientos de haber

			MessageBox.Show("Partida Agregada Exitosamente ");

			Number.Text = (AccountingItems.Last().Number+1).ToString();
		}

		private void ValidateTxt(object sender, EventArgs e)
		{
			int b;
			if (!int.TryParse(((TextBox)sender).Text, out b))
				((TextBox)sender).Text = "0";
		}

        private void button1_Click(object sender, EventArgs e)
        {
            if (Usuario.Puesto == UserPosition.Contabilidad)
                new ModificaryEliminarPartidas().Show();
            else
                MessageBox.Show("Usted no Tiene Acceso a Modificacion Y eliminacion de Datos");
        }
    }
}
