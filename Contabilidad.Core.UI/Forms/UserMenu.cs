﻿using Contabilidad.Core.Classes;
using Contabilidad.Core.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class UserMenu : Form
	{
		private UserManager um;
		private Usuario us;

		public UserMenu()
		{
			InitializeComponent();
			um = Program.getManager().GetUserManager();

			if (um.GetCurrentUser().Puesto != UserPosition.Administrador)
				buttonRegister.Enabled = false;

			dgvUsers.DataSource = um.GetUsers();
		}

		private void buttonRegister_Click(object sender, EventArgs e)
		{
			new RegisterUser(null).ShowDialog();
		}

		private void buttonDelete_Click(object sender, EventArgs e)
		{
			if (us != null)
			{
				var window = MessageBox.Show($"Desea eliminar el usuario {us.User}?", "Eliminar Usuario", MessageBoxButtons.YesNo);
				if (window == DialogResult.Yes)
					um.RemoveUser(us);
			}
		}

		private void dgvUsers_SelectionChanged(object sender, EventArgs e)
		{
			DataGridViewRow row = dgvUsers.CurrentRow;
			us = (Usuario)row.DataBoundItem;
			buttonDelete.Enabled = !(us == um.GetCurrentUser()) && um.GetCurrentUser().Puesto == UserPosition.Administrador;
			buttonModify.Enabled = us == um.GetCurrentUser() || um.GetCurrentUser().Puesto == UserPosition.Administrador;
		}

		private void buttonModify_Click(object sender, EventArgs e)
		{
			if (us != null)
				new RegisterUser(us).ShowDialog();
			dgvUsers.Refresh();
		}
	}
}
