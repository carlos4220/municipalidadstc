﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class ReportesCuenta : Form
	{
		Account Account = new Account();
		BindingList<Accounting_Party> Accounting_Party;
		public ReportesCuenta(Account Account)
		{
			InitializeComponent();
            var m = Program.getManager();
            this.Account = Account;
			//cargar los datos de las partidas a la list Accounting_Party
            Accounting_Party = m.GetDepartureManager().GetDepartures();
        }

		public void ActualizarDgv()
		{
			NameAccount.Text = Account.Description;
			int Filas = 0;
            dgvMovement.Rows.Clear();

			foreach (var Departure in Accounting_Party)
			{
				if (DateTime.Compare(FechaDesde.Value,Departure.Date)<0 && DateTime.Compare(Departure.Date,FechaHasta.Value)<0)
				{
					foreach (var Movement in Departure.Debe)
					{
						if (Movement.Account.Code == this.Account.Code)
						{
							dgvMovement.Rows.Add();
							dgvMovement.Rows[Filas].Cells[0].Value = ("Pda " + Departure.Number);
							dgvMovement.Rows[Filas].Cells[1].Value = Departure.Date;
							dgvMovement.Rows[Filas].Cells[2].Value = Departure.Description;

							dgvMovement.Rows[Filas].Cells[3].Value = Movement.Debe;
                            Filas++;
						}
					}
					foreach (var Movement in Departure.Haber)
					{
						if (Movement.Account.Code == this.Account.Code)
						{
							dgvMovement.Rows.Add();
							dgvMovement.Rows[Filas].Cells[0].Value = ("Pda " + Departure.Number);
							dgvMovement.Rows[Filas].Cells[1].Value = Departure.Date;
							dgvMovement.Rows[Filas].Cells[2].Value = Departure.Description;

							dgvMovement.Rows[Filas].Cells[4].Value = Movement.Haber;
                            Filas++;
						}
					}
				}
			}
		}

        private void btnActualizarLibro_Click(object sender, EventArgs e)
        {
            ActualizarDgv();
        }
    }
}
