﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class ImprimirBalance : Form
	{
		public ImprimirBalance( BalanceSheet Balance)
		{
			InitializeComponent();
			UpdateBalance(Balance);
		}


		public void UpdateBalance( BalanceSheet Balance)
		{
			TextNumber.Text = Balance.Number.ToString();
			TextDate.Text = Balance.Date.ToString();
			UpdateDgv(Balance);
		}

		public void UpdateDgv(BalanceSheet Balance)
		{
			int RowsA = 0; //filas activo
			int RowsP = 0; //filas pasivo
			decimal SaldoCorriente = 0;
			decimal SaldoNoCorriente = 0;

			//mostrar activo corriente
			dgvActivo.Rows.Add();
			dgvActivo.Rows[RowsA].Cells[1].Value = "Activo Corriente";
			RowsA++;
			foreach (MovementBalance Value in Balance.Movement)
			{
				if (Value.Type == BalanceAccountType.ActivoCorriente)
				{
					dgvActivo.Rows.Add();
					dgvActivo.Rows[RowsA].Cells[0].Value = Value.Account.Code;
					dgvActivo.Rows[RowsA].Cells[1].Value = Value.Account.Description;
					dgvActivo.Rows[RowsA].Cells[2].Value = Value.Saldo;
					SaldoCorriente += Value.Saldo;
					RowsA++;
				}
			}
            if (SaldoCorriente > 0)
            {
                dgvActivo.Rows.Add();
                dgvActivo.Rows[RowsA].Cells[1].Value = "Sub Total";
                dgvActivo.Rows[RowsA].Cells[3].Value = SaldoCorriente;
                RowsA++;
            }
			//mostrar activo no corriente

			dgvActivo.Rows.Add();
			dgvActivo.Rows[RowsA].Cells[1].Value = "Activo No Corriente";
			RowsA++;
			foreach (MovementBalance Value in Balance.Movement)
			{
				if (Value.Type == BalanceAccountType.ActivoNoCorriente)
				{
					dgvActivo.Rows.Add();
					dgvActivo.Rows[RowsA].Cells[0].Value = Value.Account.Code;
					dgvActivo.Rows[RowsA].Cells[1].Value = Value.Account.Description;
					dgvActivo.Rows[RowsA].Cells[2].Value = Value.Saldo;
					SaldoNoCorriente += Value.Saldo;
					RowsA++;
				}
			}
            if (SaldoNoCorriente > 0)
            {
                dgvActivo.Rows.Add();
                dgvActivo.Rows[RowsA].Cells[1].Value = "Sub Total";
                dgvActivo.Rows[RowsA].Cells[3].Value = SaldoNoCorriente;
                RowsA++;
            }
			//mostrar total activo

			dgvActivo.Rows.Add();
			dgvActivo.Rows[RowsA].Cells[1].Value = "Total Activo";
			dgvActivo.Rows[RowsA].Cells[3].Value = (SaldoCorriente + SaldoNoCorriente);
			RowsA++;

			//limpiar los saldos
			SaldoCorriente = 0;
			SaldoNoCorriente = 0;

			//mostrar pasivo  corriente
			dgvPasivoYCapital.Rows.Add();
			dgvPasivoYCapital.Rows[RowsP].Cells[1].Value = "Pasivo Corriente";
			RowsP++;

			foreach (MovementBalance Value in Balance.Movement)
			{
				if (Value.Type == BalanceAccountType.PasivoCorriente)
				{
					dgvPasivoYCapital.Rows.Add();
					dgvPasivoYCapital.Rows[RowsP].Cells[0].Value = Value.Account.Code;
					dgvPasivoYCapital.Rows[RowsP].Cells[1].Value = Value.Account.Description;
					dgvPasivoYCapital.Rows[RowsP].Cells[2].Value = Value.Saldo;
					SaldoCorriente += Value.Saldo;
					RowsP++;
				}
			}
            if (SaldoCorriente > 0)
            {
                dgvPasivoYCapital.Rows.Add();
                dgvPasivoYCapital.Rows[RowsP].Cells[1].Value = "Sub Total";
                dgvPasivoYCapital.Rows[RowsP].Cells[3].Value = SaldoCorriente;
                RowsP++;
            }
			//mostrar pasivo No corriente
			dgvPasivoYCapital.Rows.Add();
			dgvPasivoYCapital.Rows[RowsP].Cells[1].Value = "Pasivo No Corriente";
			RowsP++;

			foreach (MovementBalance Value in Balance.Movement)
			{
				if (Value.Type == BalanceAccountType.PasivoNoCorriente)
				{
					dgvPasivoYCapital.Rows.Add();
					dgvPasivoYCapital.Rows[RowsP].Cells[0].Value = Value.Account.Code;
					dgvPasivoYCapital.Rows[RowsP].Cells[1].Value = Value.Account.Description;
					dgvPasivoYCapital.Rows[RowsP].Cells[2].Value = Value.Saldo;
					SaldoNoCorriente += Value.Saldo;
					RowsP++;
				}
			}
            if (SaldoNoCorriente > 0)
            {
                dgvPasivoYCapital.Rows.Add();
                dgvPasivoYCapital.Rows[RowsP].Cells[1].Value = "Sub Total";
                dgvPasivoYCapital.Rows[RowsP].Cells[3].Value = SaldoNoCorriente;
                RowsP++;
            }
			//mostrar total de pasivo 
			dgvPasivoYCapital.Rows.Add();
			dgvPasivoYCapital.Rows[RowsP].Cells[1].Value = "Total Pasivo";
			dgvPasivoYCapital.Rows[RowsP].Cells[3].Value = (SaldoCorriente + SaldoNoCorriente);
			RowsP++;

			//saldocorriente toma el valor acumulado del pasivo
			SaldoCorriente += SaldoNoCorriente;
			//saldoNoCorriente acuma el valor de patrimonio
			SaldoNoCorriente = 0;


			//mustra valor de patrimonio
			dgvPasivoYCapital.Rows.Add();
			dgvPasivoYCapital.Rows[RowsP].Cells[1].Value = "Patrimonio";
			RowsP++;

			foreach (MovementBalance Value in Balance.Movement)
			{
				if (Value.Type == BalanceAccountType.Patrimonio)
				{
					dgvPasivoYCapital.Rows.Add();
					dgvPasivoYCapital.Rows[RowsP].Cells[0].Value = Value.Account.Code;
					dgvPasivoYCapital.Rows[RowsP].Cells[1].Value = Value.Account.Description;
					dgvPasivoYCapital.Rows[RowsP].Cells[2].Value = Value.Saldo;
					SaldoNoCorriente += Value.Saldo;
					RowsP++;
				}
			}
            if (SaldoNoCorriente > 0)
            {
                dgvPasivoYCapital.Rows.Add();
                dgvPasivoYCapital.Rows[RowsP].Cells[1].Value = "Total Patrimonio";
                dgvPasivoYCapital.Rows[RowsP].Cells[3].Value = SaldoNoCorriente;
                RowsP++;
            }
			dgvPasivoYCapital.Rows.Add();
			dgvPasivoYCapital.Rows[RowsP].Cells[1].Value = "Total Pasivo y Patrimonio";
			dgvPasivoYCapital.Rows[RowsP].Cells[3].Value = (SaldoCorriente + SaldoNoCorriente);

		}
	}
}
