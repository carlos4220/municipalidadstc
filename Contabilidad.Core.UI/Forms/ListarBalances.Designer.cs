﻿namespace Contabilidad.Core.UI.Forms
{
    partial class ListarBalances
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListarBalances));
			this.dgvLista = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtNumber = new System.Windows.Forms.TextBox();
			this.btnIrBalance = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dgvLista)).BeginInit();
			this.SuspendLayout();
			// 
			// dgvLista
			// 
			this.dgvLista.AllowUserToAddRows = false;
			this.dgvLista.AllowUserToDeleteRows = false;
			this.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvLista.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
			this.dgvLista.Location = new System.Drawing.Point(157, 42);
			this.dgvLista.MultiSelect = false;
			this.dgvLista.Name = "dgvLista";
			this.dgvLista.ReadOnly = true;
			this.dgvLista.Size = new System.Drawing.Size(347, 150);
			this.dgvLista.TabIndex = 0;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "Numero";
			this.Column1.Name = "Column1";
			this.Column1.ReadOnly = true;
			this.Column1.Width = 150;
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Fecha";
			this.Column2.Name = "Column2";
			this.Column2.ReadOnly = true;
			this.Column2.Width = 150;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(301, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(171, 30);
			this.label1.TabIndex = 1;
			this.label1.Text = "Lista de Balances";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(111, 237);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(124, 17);
			this.label2.TabIndex = 2;
			this.label2.Text = "Numero De Balance";
			// 
			// txtNumber
			// 
			this.txtNumber.Location = new System.Drawing.Point(241, 237);
			this.txtNumber.Name = "txtNumber";
			this.txtNumber.Size = new System.Drawing.Size(145, 23);
			this.txtNumber.TabIndex = 3;
			this.txtNumber.Text = "0";
			this.txtNumber.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// btnIrBalance
			// 
			this.btnIrBalance.Location = new System.Drawing.Point(422, 237);
			this.btnIrBalance.Name = "btnIrBalance";
			this.btnIrBalance.Size = new System.Drawing.Size(82, 28);
			this.btnIrBalance.TabIndex = 4;
			this.btnIrBalance.Text = "Ir a Balance";
			this.btnIrBalance.UseVisualStyleBackColor = true;
			this.btnIrBalance.Click += new System.EventHandler(this.btnIrBalance_Click);
			// 
			// ListarBalances
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(736, 323);
			this.Controls.Add(this.btnIrBalance);
			this.Controls.Add(this.txtNumber);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.dgvLista);
			this.Font = new System.Drawing.Font("Segoe UI", 8.5F);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "ListarBalances";
			this.Text = "Balances";
			((System.ComponentModel.ISupportInitialize)(this.dgvLista)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvLista;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.Button btnIrBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    }
}