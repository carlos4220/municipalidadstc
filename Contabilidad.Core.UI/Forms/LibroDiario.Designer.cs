﻿namespace Contabilidad.Core.UI.Forms
{
    partial class LibroDiario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LibroDiario));
			this.label1 = new System.Windows.Forms.Label();
			this.FechaDesde = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnActualizarLibro = new System.Windows.Forms.Button();
			this.dgvLibroDiario = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Debe = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Haber = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FechaHasta = new System.Windows.Forms.DateTimePicker();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
			this.button1 = new System.Windows.Forms.Button();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvLibroDiario)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(367, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(149, 30);
			this.label1.TabIndex = 0;
			this.label1.Text = "Libro de Diario";
			// 
			// FechaDesde
			// 
			this.FechaDesde.CustomFormat = "dd/MM/yyy";
			this.FechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.FechaDesde.Location = new System.Drawing.Point(143, 93);
			this.FechaDesde.Name = "FechaDesde";
			this.FechaDesde.Size = new System.Drawing.Size(84, 23);
			this.FechaDesde.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(203, 49);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(106, 17);
			this.label2.TabIndex = 2;
			this.label2.Text = "Filtros De Fechas";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnActualizarLibro);
			this.panel1.Controls.Add(this.dgvLibroDiario);
			this.panel1.Controls.Add(this.FechaHasta);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.FechaDesde);
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Margin = new System.Windows.Forms.Padding(0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(883, 556);
			this.panel1.TabIndex = 3;
			// 
			// btnActualizarLibro
			// 
			this.btnActualizarLibro.Location = new System.Drawing.Point(517, 81);
			this.btnActualizarLibro.Name = "btnActualizarLibro";
			this.btnActualizarLibro.Size = new System.Drawing.Size(113, 32);
			this.btnActualizarLibro.TabIndex = 9;
			this.btnActualizarLibro.Text = "Actualizar Libro";
			this.btnActualizarLibro.UseVisualStyleBackColor = true;
			this.btnActualizarLibro.Click += new System.EventHandler(this.btnActualizarLibro_Click);
			// 
			// dgvLibroDiario
			// 
			this.dgvLibroDiario.AllowUserToAddRows = false;
			this.dgvLibroDiario.AllowUserToDeleteRows = false;
			this.dgvLibroDiario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvLibroDiario.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Debe,
            this.Haber});
			this.dgvLibroDiario.Location = new System.Drawing.Point(43, 131);
			this.dgvLibroDiario.MultiSelect = false;
			this.dgvLibroDiario.Name = "dgvLibroDiario";
			this.dgvLibroDiario.ReadOnly = true;
			this.dgvLibroDiario.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvLibroDiario.Size = new System.Drawing.Size(794, 395);
			this.dgvLibroDiario.TabIndex = 7;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "";
			this.Column1.Name = "Column1";
			this.Column1.ReadOnly = true;
			this.Column1.Width = 50;
			// 
			// Column2
			// 
			this.Column2.HeaderText = "";
			this.Column2.Name = "Column2";
			this.Column2.ReadOnly = true;
			this.Column2.Width = 400;
			// 
			// Debe
			// 
			this.Debe.HeaderText = "DEBE";
			this.Debe.Name = "Debe";
			this.Debe.ReadOnly = true;
			this.Debe.Width = 150;
			// 
			// Haber
			// 
			this.Haber.HeaderText = "HABER";
			this.Haber.Name = "Haber";
			this.Haber.ReadOnly = true;
			this.Haber.Width = 150;
			// 
			// FechaHasta
			// 
			this.FechaHasta.CustomFormat = "dd/MM/yyy";
			this.FechaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.FechaHasta.Location = new System.Drawing.Point(279, 93);
			this.FechaHasta.Name = "FechaHasta";
			this.FechaHasta.Size = new System.Drawing.Size(84, 23);
			this.FechaHasta.TabIndex = 5;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(164, 77);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(39, 15);
			this.label4.TabIndex = 4;
			this.label4.Text = "Desde";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(293, 73);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(41, 17);
			this.label3.TabIndex = 3;
			this.label3.Text = "Hasta";
			// 
			// dateTimePicker2
			// 
			this.dateTimePicker2.CustomFormat = "dd/MM/yyy";
			this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePicker2.Location = new System.Drawing.Point(279, 93);
			this.dateTimePicker2.Name = "dateTimePicker2";
			this.dateTimePicker2.Size = new System.Drawing.Size(84, 20);
			this.dateTimePicker2.TabIndex = 5;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(520, 77);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(113, 32);
			this.button1.TabIndex = 8;
			this.button1.Text = "Actualizar Libro";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// dataGridView1
			// 
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Location = new System.Drawing.Point(43, 131);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dataGridView1.Size = new System.Drawing.Size(794, 395);
			this.dataGridView1.TabIndex = 7;
			// 
			// LibroDiario
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(884, 558);
			this.Controls.Add(this.panel1);
			this.Font = new System.Drawing.Font("Segoe UI", 8.5F);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "LibroDiario";
			this.Text = "Libro Diario";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvLibroDiario)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker FechaDesde;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Debe;
        private System.Windows.Forms.DataGridViewTextBoxColumn Haber;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnActualizarLibro;
        private System.Windows.Forms.DataGridView dgvLibroDiario;
        private System.Windows.Forms.DateTimePicker FechaHasta;
    }
}