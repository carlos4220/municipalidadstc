﻿namespace Contabilidad.Core.UI.Forms
{
	partial class RegisterUser
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterUser));
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnRegistrarse = new System.Windows.Forms.Button();
			this.btnRegresar = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.cmbPuesto = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.txtPasswordConfirmation = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtUser = new System.Windows.Forms.TextBox();
			this.txtPassword = new System.Windows.Forms.TextBox();
			this.labelRegister = new System.Windows.Forms.Label();
			this.buttonModify = new System.Windows.Forms.Button();
			this.labelModify = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.disabledType = new System.Windows.Forms.TextBox();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.panel1.Controls.Add(this.disabledType);
			this.panel1.Controls.Add(this.labelModify);
			this.panel1.Controls.Add(this.buttonModify);
			this.panel1.Controls.Add(this.btnRegistrarse);
			this.panel1.Controls.Add(this.btnRegresar);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.cmbPuesto);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.txtPasswordConfirmation);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.txtUser);
			this.panel1.Controls.Add(this.txtPassword);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.labelRegister);
			this.panel1.Location = new System.Drawing.Point(12, 12);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(392, 332);
			this.panel1.TabIndex = 0;
			// 
			// btnRegistrarse
			// 
			this.btnRegistrarse.Location = new System.Drawing.Point(250, 289);
			this.btnRegistrarse.Margin = new System.Windows.Forms.Padding(3, 3, 20, 20);
			this.btnRegistrarse.Name = "btnRegistrarse";
			this.btnRegistrarse.Size = new System.Drawing.Size(122, 23);
			this.btnRegistrarse.TabIndex = 18;
			this.btnRegistrarse.Text = "Registrarse";
			this.btnRegistrarse.UseVisualStyleBackColor = true;
			this.btnRegistrarse.Click += new System.EventHandler(this.btnRegistrarse_Click);
			// 
			// btnRegresar
			// 
			this.btnRegresar.Location = new System.Drawing.Point(20, 289);
			this.btnRegresar.Margin = new System.Windows.Forms.Padding(20, 3, 3, 20);
			this.btnRegresar.Name = "btnRegresar";
			this.btnRegresar.Size = new System.Drawing.Size(81, 23);
			this.btnRegresar.TabIndex = 17;
			this.btnRegresar.Text = "Regresar";
			this.btnRegresar.UseVisualStyleBackColor = true;
			this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(32, 223);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(46, 15);
			this.label6.TabIndex = 16;
			this.label6.Text = "Puesto:";
			// 
			// cmbPuesto
			// 
			this.cmbPuesto.FormattingEnabled = true;
			this.cmbPuesto.Location = new System.Drawing.Point(84, 220);
			this.cmbPuesto.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
			this.cmbPuesto.Name = "cmbPuesto";
			this.cmbPuesto.Size = new System.Drawing.Size(238, 23);
			this.cmbPuesto.TabIndex = 15;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(31, 177);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(47, 15);
			this.label5.TabIndex = 14;
			this.label5.Text = "Repetir:";
			// 
			// txtPasswordConfirmation
			// 
			this.txtPasswordConfirmation.Location = new System.Drawing.Point(84, 174);
			this.txtPasswordConfirmation.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
			this.txtPasswordConfirmation.Name = "txtPasswordConfirmation";
			this.txtPasswordConfirmation.PasswordChar = '*';
			this.txtPasswordConfirmation.Size = new System.Drawing.Size(238, 23);
			this.txtPasswordConfirmation.TabIndex = 13;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(8, 131);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(70, 15);
			this.label3.TabIndex = 10;
			this.label3.Text = "Contraseña:";
			// 
			// txtUser
			// 
			this.txtUser.Location = new System.Drawing.Point(84, 82);
			this.txtUser.Name = "txtUser";
			this.txtUser.Size = new System.Drawing.Size(238, 23);
			this.txtUser.TabIndex = 6;
			// 
			// txtPassword
			// 
			this.txtPassword.Location = new System.Drawing.Point(84, 128);
			this.txtPassword.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.PasswordChar = '*';
			this.txtPassword.Size = new System.Drawing.Size(238, 23);
			this.txtPassword.TabIndex = 8;
			// 
			// labelRegister
			// 
			this.labelRegister.AutoSize = true;
			this.labelRegister.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelRegister.Location = new System.Drawing.Point(139, 23);
			this.labelRegister.Name = "labelRegister";
			this.labelRegister.Size = new System.Drawing.Size(114, 30);
			this.labelRegister.TabIndex = 7;
			this.labelRegister.Text = "Registrarse";
			this.labelRegister.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// buttonModify
			// 
			this.buttonModify.Location = new System.Drawing.Point(250, 289);
			this.buttonModify.Margin = new System.Windows.Forms.Padding(3, 3, 20, 20);
			this.buttonModify.Name = "buttonModify";
			this.buttonModify.Size = new System.Drawing.Size(122, 23);
			this.buttonModify.TabIndex = 19;
			this.buttonModify.Text = "Modificar";
			this.buttonModify.UseVisualStyleBackColor = true;
			this.buttonModify.Visible = false;
			this.buttonModify.Click += new System.EventHandler(this.buttonModify_Click);
			// 
			// labelModify
			// 
			this.labelModify.AutoSize = true;
			this.labelModify.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelModify.Location = new System.Drawing.Point(146, 23);
			this.labelModify.Name = "labelModify";
			this.labelModify.Size = new System.Drawing.Size(101, 30);
			this.labelModify.TabIndex = 20;
			this.labelModify.Text = "Modificar";
			this.labelModify.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.labelModify.Visible = false;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(24, 85);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(54, 15);
			this.label2.TabIndex = 9;
			this.label2.Text = "Nombre:";
			// 
			// disabledType
			// 
			this.disabledType.Location = new System.Drawing.Point(84, 220);
			this.disabledType.Name = "disabledType";
			this.disabledType.ReadOnly = true;
			this.disabledType.Size = new System.Drawing.Size(238, 23);
			this.disabledType.TabIndex = 21;
			this.disabledType.Visible = false;
			// 
			// RegisterUser
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(416, 356);
			this.Controls.Add(this.panel1);
			this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(432, 395);
			this.Name = "RegisterUser";
			this.Text = "Registrar Usuario";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormClose);
			this.Shown += new System.EventHandler(this.Loaded);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);

		}

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtPasswordConfirmation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label labelRegister;
        private System.Windows.Forms.Button btnRegresar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbPuesto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnRegistrarse;
		private System.Windows.Forms.Button buttonModify;
		private System.Windows.Forms.Label labelModify;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox disabledType;
	}
}