﻿namespace Contabilidad.Core.UI.Forms
{
	partial class Nomenclatura
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Nomenclatura));
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnMostraReportesCuenta = new System.Windows.Forms.Button();
			this.cmbTypeAccount = new System.Windows.Forms.ComboBox();
			this.label7 = new System.Windows.Forms.Label();
			this.txtLevel1 = new System.Windows.Forms.TextBox();
			this.dgvAccount = new System.Windows.Forms.DataGridView();
			this.btnRemove = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.txtAccountName = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtLevel2 = new System.Windows.Forms.TextBox();
			this.txtLevel3 = new System.Windows.Forms.TextBox();
			this.txtLevel4 = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAccount)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnMostraReportesCuenta);
			this.panel1.Controls.Add(this.cmbTypeAccount);
			this.panel1.Controls.Add(this.label7);
			this.panel1.Controls.Add(this.txtLevel1);
			this.panel1.Controls.Add(this.dgvAccount);
			this.panel1.Controls.Add(this.btnRemove);
			this.panel1.Controls.Add(this.btnAdd);
			this.panel1.Controls.Add(this.txtAccountName);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.txtLevel2);
			this.panel1.Controls.Add(this.txtLevel3);
			this.panel1.Controls.Add(this.txtLevel4);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Location = new System.Drawing.Point(12, 39);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(883, 494);
			this.panel1.TabIndex = 0;
			// 
			// btnMostraReportesCuenta
			// 
			this.btnMostraReportesCuenta.Location = new System.Drawing.Point(749, 102);
			this.btnMostraReportesCuenta.Name = "btnMostraReportesCuenta";
			this.btnMostraReportesCuenta.Size = new System.Drawing.Size(99, 38);
			this.btnMostraReportesCuenta.TabIndex = 17;
			this.btnMostraReportesCuenta.Text = "Reporte de Cuenta";
			this.btnMostraReportesCuenta.UseVisualStyleBackColor = true;
			this.btnMostraReportesCuenta.Click += new System.EventHandler(this.btnMostraReportesCuenta_Click);
			// 
			// cmbTypeAccount
			// 
			this.cmbTypeAccount.FormattingEnabled = true;
			this.cmbTypeAccount.Location = new System.Drawing.Point(2, 70);
			this.cmbTypeAccount.Name = "cmbTypeAccount";
			this.cmbTypeAccount.Size = new System.Drawing.Size(121, 21);
			this.cmbTypeAccount.TabIndex = 15;
			this.cmbTypeAccount.SelectedIndexChanged += new System.EventHandler(this.cmbTypeAccount_SelectedIndexChanged);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(42, 52);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(31, 15);
			this.label7.TabIndex = 14;
			this.label7.Text = "Tipo";
			// 
			// txtLevel1
			// 
			this.txtLevel1.Location = new System.Drawing.Point(129, 70);
			this.txtLevel1.Name = "txtLevel1";
			this.txtLevel1.Size = new System.Drawing.Size(29, 23);
			this.txtLevel1.TabIndex = 0;
			this.txtLevel1.Text = "0";
			this.txtLevel1.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// dgvAccount
			// 
			this.dgvAccount.AllowUserToAddRows = false;
			this.dgvAccount.AllowUserToDeleteRows = false;
			this.dgvAccount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvAccount.Location = new System.Drawing.Point(167, 136);
			this.dgvAccount.MultiSelect = false;
			this.dgvAccount.Name = "dgvAccount";
			this.dgvAccount.ReadOnly = true;
			this.dgvAccount.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvAccount.Size = new System.Drawing.Size(530, 355);
			this.dgvAccount.TabIndex = 13;
			this.dgvAccount.SelectionChanged += new System.EventHandler(this.dgvAccount_SelectionChanged);
			// 
			// btnRemove
			// 
			this.btnRemove.Location = new System.Drawing.Point(749, 18);
			this.btnRemove.Name = "btnRemove";
			this.btnRemove.Size = new System.Drawing.Size(99, 23);
			this.btnRemove.TabIndex = 12;
			this.btnRemove.Text = "Eliminar";
			this.btnRemove.UseVisualStyleBackColor = true;
			this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(749, 60);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(99, 23);
			this.btnAdd.TabIndex = 5;
			this.btnAdd.Text = "Agregar";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// txtAccountName
			// 
			this.txtAccountName.Location = new System.Drawing.Point(349, 70);
			this.txtAccountName.Name = "txtAccountName";
			this.txtAccountName.Size = new System.Drawing.Size(380, 23);
			this.txtAccountName.TabIndex = 4;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(513, 52);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(45, 15);
			this.label6.TabIndex = 9;
			this.label6.Text = "Cuenta";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(215, 73);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(12, 15);
			this.label5.TabIndex = 8;
			this.label5.Text = "-";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(266, 73);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(12, 15);
			this.label4.TabIndex = 7;
			this.label4.Text = "-";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(164, 73);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(12, 15);
			this.label3.TabIndex = 6;
			this.label3.Text = "-";
			// 
			// txtLevel2
			// 
			this.txtLevel2.Location = new System.Drawing.Point(180, 70);
			this.txtLevel2.Name = "txtLevel2";
			this.txtLevel2.Size = new System.Drawing.Size(29, 23);
			this.txtLevel2.TabIndex = 1;
			this.txtLevel2.Text = "0";
			this.txtLevel2.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// txtLevel3
			// 
			this.txtLevel3.Location = new System.Drawing.Point(231, 70);
			this.txtLevel3.Name = "txtLevel3";
			this.txtLevel3.Size = new System.Drawing.Size(29, 23);
			this.txtLevel3.TabIndex = 2;
			this.txtLevel3.Text = "0";
			this.txtLevel3.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// txtLevel4
			// 
			this.txtLevel4.Location = new System.Drawing.Point(282, 70);
			this.txtLevel4.Name = "txtLevel4";
			this.txtLevel4.Size = new System.Drawing.Size(29, 23);
			this.txtLevel4.TabIndex = 3;
			this.txtLevel4.Text = "0";
			this.txtLevel4.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(197, 52);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(46, 15);
			this.label2.TabIndex = 1;
			this.label2.Text = "Codigo";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(324, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(234, 30);
			this.label1.TabIndex = 0;
			this.label1.Text = "Nomenclatura Contable";
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(98, 73);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(29, 20);
			this.textBox3.TabIndex = 2;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(149, 73);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(29, 20);
			this.textBox2.TabIndex = 3;
			// 
			// Nomenclatura
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(907, 545);
			this.Controls.Add(this.panel1);
			this.Font = new System.Drawing.Font("Segoe UI", 8.5F);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "Nomenclatura";
			this.Text = "Nomenclatura";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAccount)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DataGridView dgvAccount;
		private System.Windows.Forms.Button btnRemove;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.TextBox txtAccountName;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtLevel2;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox txtLevel1;
		private System.Windows.Forms.TextBox txtLevel4;
		private System.Windows.Forms.TextBox txtLevel3;
		private System.Windows.Forms.ComboBox cmbTypeAccount;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Button btnMostraReportesCuenta;
	}
}