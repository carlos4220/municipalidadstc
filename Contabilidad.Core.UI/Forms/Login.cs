﻿using Contabilidad.Core.Classes;
using Contabilidad.Core.Managers;
using Contabilidad.Core.UI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class VentanaLogin : Form
	{

		private UserManager um = Program.getManager().GetUserManager();
		private ErrorProvider Error = new ErrorProvider();

		public VentanaLogin()
		{
			InitializeComponent();
			FormClosing += FormClose;
		}

		private void btnIniciarSecion_Click(object sender, EventArgs e)
		{
			Error.Clear();

			var re = um.Login(txtUser.Text, txtPassword.Text);

			switch (re)
			{
				case RegisterErrors.EMPTY_USER:
					Error.SetError(txtUser, "Ingrese un nombre.");
					break;
				case RegisterErrors.EMPTY_PASSWORD:
					Error.SetError(txtPassword, "Ingrese una contraseña.");
					break;
				case RegisterErrors.MISMATCH_PASSWORD:
					MessageBox.Show("La contraseña no es correcta, por favor intente de nuevo.");
					break;
				case RegisterErrors.MISMATCH_USER:
					MessageBox.Show("El usuario no es correcto, por favor intente de nuevo.");
					break;
				case RegisterErrors.OK:
					Close();
					break;
			}
		}

		private void FormClose(object sender, FormClosingEventArgs e)
		{
			if (!um.IsLoggedIn())
			{
				var window = MessageBox.Show("Se necesita iniciar sesion para utilizar la aplicacion, desea salir?", "Salir sin iniciar sesion", MessageBoxButtons.YesNo);
				e.Cancel = (window == DialogResult.No);
			}

		}

	}
}
