﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class ImpresionDePoliza : Form
	{
		AccountingPolicies Poliza;
        private BindingList<Account> Accounts;

        public ImpresionDePoliza(AccountingPolicies Poliza)
		{
			InitializeComponent();
            var m = Program.getManager();

            Accounts = m.GetNomenclatureManager().GetAccounts();

            this.Poliza = new AccountingPolicies();
			this.Poliza = Poliza;
			GenerarPoliza();
		}

		public void GenerarPoliza()
		{
			TextTipoPoliza.Text = Poliza.Type;
			TextFecha.Text = Poliza.Date.ToString("dd/MM/yyy");//esto No se si funciona  deberia mostrar en la fecha solo dia/mes/year
			txtAutorizadoPor.Text = Poliza.Authorizer;
			txtHechoPor.Text = Poliza.Developer;
			txtDescription.Text = Poliza.Description;
			txtNumero.Text = Poliza.Number.ToString();

			int Rows = 0;
			decimal TotalParcial = 0;
			decimal TotalDebe = 0;
			decimal TotalHaber = 0;

			foreach (AccountingMovement Value in Poliza.Debe)
			{
				dgvMovements.Rows.Add();
				dgvMovements.Rows[Rows].Cells[0].Value = Value.Account.Code;
                foreach(var Account in Accounts)
                {
                    if (Value.Account.Code == Account.Code)
                    {
                        dgvMovements.Rows[Rows].Cells[1].Value = Account.Description;
                    }
                }
				
				dgvMovements.Rows[Rows].Cells[2].Value = Value.Debe;
				dgvMovements.Rows[Rows].Cells[3].Value = Value.Debe;
				TotalParcial += Value.Debe;
				TotalDebe += Value.Debe;

				Rows++;
			}

			foreach (AccountingMovement Value in Poliza.Haber)
			{
				dgvMovements.Rows.Add();
				dgvMovements.Rows[Rows].Cells[0].Value = Value.Account.Code;
                foreach (var Account in Accounts)
                {
                    if (Value.Account.Code == Account.Code)
                    {
                        dgvMovements.Rows[Rows].Cells[1].Value = Account.Description;
                    }
                }
                dgvMovements.Rows[Rows].Cells[2].Value = Value.Haber;
				dgvMovements.Rows[Rows].Cells[4].Value = Value.Haber;
				TotalParcial += Value.Haber;
				TotalHaber += Value.Haber;

				Rows++;
			}
			txtSumaDebe.Text = TotalDebe.ToString();
			txtSumaHaber.Text = TotalHaber.ToString();
			txtSumaParcial.Text = TotalParcial.ToString();
		}
	}
}
