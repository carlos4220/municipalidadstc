﻿namespace Contabilidad.Core.UI.Forms
{
    partial class PolizasContables
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PolizasContables));
			this.panel1 = new System.Windows.Forms.Panel();
			this.DateTimePoliza = new System.Windows.Forms.DateTimePicker();
			this.label20 = new System.Windows.Forms.Label();
			this.Number = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.txtTotalAcreedor = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.txtTotalDeudor = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.dgvAccountsAcreedor = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.btnSave = new System.Windows.Forms.Button();
			this.label17 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.dgvAccountsDeudor = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.txtQuienAutoriza = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.txtDescription = new System.Windows.Forms.TextBox();
			this.cmbTipoPoliza = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.btnRemove = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.label11 = new System.Windows.Forms.Label();
			this.txtLevel1 = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.txtLevel2 = new System.Windows.Forms.TextBox();
			this.txtLevel3 = new System.Windows.Forms.TextBox();
			this.txtLevel4 = new System.Windows.Forms.TextBox();
			this.txtAmount = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtNameAccount = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.cmbTipoSaldo = new System.Windows.Forms.ComboBox();
			this.label12 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.btnModificarPolizas = new System.Windows.Forms.Button();
			this.label21 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAccountsAcreedor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvAccountsDeudor)).BeginInit();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.DateTimePoliza);
			this.panel1.Controls.Add(this.label20);
			this.panel1.Controls.Add(this.Number);
			this.panel1.Controls.Add(this.label19);
			this.panel1.Controls.Add(this.txtTotalAcreedor);
			this.panel1.Controls.Add(this.label15);
			this.panel1.Controls.Add(this.txtTotalDeudor);
			this.panel1.Controls.Add(this.label16);
			this.panel1.Controls.Add(this.dgvAccountsAcreedor);
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Controls.Add(this.label17);
			this.panel1.Controls.Add(this.label18);
			this.panel1.Controls.Add(this.dgvAccountsDeudor);
			this.panel1.Controls.Add(this.txtQuienAutoriza);
			this.panel1.Controls.Add(this.label14);
			this.panel1.Controls.Add(this.label13);
			this.panel1.Controls.Add(this.txtDescription);
			this.panel1.Controls.Add(this.cmbTipoPoliza);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.btnRemove);
			this.panel1.Controls.Add(this.btnAdd);
			this.panel1.Controls.Add(this.label11);
			this.panel1.Controls.Add(this.txtLevel1);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.label9);
			this.panel1.Controls.Add(this.label10);
			this.panel1.Controls.Add(this.txtLevel2);
			this.panel1.Controls.Add(this.txtLevel3);
			this.panel1.Controls.Add(this.txtLevel4);
			this.panel1.Controls.Add(this.txtAmount);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.txtNameAccount);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.cmbTipoSaldo);
			this.panel1.Controls.Add(this.label12);
			this.panel1.Controls.Add(this.label8);
			this.panel1.Controls.Add(this.label7);
			this.panel1.Location = new System.Drawing.Point(154, 13);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1088, 566);
			this.panel1.TabIndex = 0;
			// 
			// DateTimePoliza
			// 
			this.DateTimePoliza.CustomFormat = "dd/MM/yyy";
			this.DateTimePoliza.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.DateTimePoliza.Location = new System.Drawing.Point(677, 307);
			this.DateTimePoliza.Name = "DateTimePoliza";
			this.DateTimePoliza.Size = new System.Drawing.Size(200, 23);
			this.DateTimePoliza.TabIndex = 77;
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label20.Location = new System.Drawing.Point(623, 311);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(48, 17);
			this.label20.TabIndex = 76;
			this.label20.Text = "Fecha: ";
			// 
			// Number
			// 
			this.Number.AutoSize = true;
			this.Number.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Number.Location = new System.Drawing.Point(677, 83);
			this.Number.Name = "Number";
			this.Number.Size = new System.Drawing.Size(0, 17);
			this.Number.TabIndex = 75;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label19.Location = new System.Drawing.Point(558, 79);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(113, 17);
			this.label19.TabIndex = 74;
			this.label19.Text = "Numero de Poliza";
			// 
			// txtTotalAcreedor
			// 
			this.txtTotalAcreedor.Location = new System.Drawing.Point(658, 525);
			this.txtTotalAcreedor.Name = "txtTotalAcreedor";
			this.txtTotalAcreedor.ReadOnly = true;
			this.txtTotalAcreedor.Size = new System.Drawing.Size(173, 23);
			this.txtTotalAcreedor.TabIndex = 73;
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.Location = new System.Drawing.Point(616, 525);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(36, 17);
			this.label15.TabIndex = 72;
			this.label15.Text = "Total";
			// 
			// txtTotalDeudor
			// 
			this.txtTotalDeudor.Location = new System.Drawing.Point(102, 525);
			this.txtTotalDeudor.Name = "txtTotalDeudor";
			this.txtTotalDeudor.ReadOnly = true;
			this.txtTotalDeudor.Size = new System.Drawing.Size(171, 23);
			this.txtTotalDeudor.TabIndex = 71;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label16.Location = new System.Drawing.Point(60, 525);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(36, 17);
			this.label16.TabIndex = 70;
			this.label16.Text = "Total";
			// 
			// dgvAccountsAcreedor
			// 
			this.dgvAccountsAcreedor.AllowUserToAddRows = false;
			this.dgvAccountsAcreedor.AllowUserToDeleteRows = false;
			this.dgvAccountsAcreedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvAccountsAcreedor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
			this.dgvAccountsAcreedor.Location = new System.Drawing.Point(568, 368);
			this.dgvAccountsAcreedor.MultiSelect = false;
			this.dgvAccountsAcreedor.Name = "dgvAccountsAcreedor";
			this.dgvAccountsAcreedor.ReadOnly = true;
			this.dgvAccountsAcreedor.Size = new System.Drawing.Size(494, 150);
			this.dgvAccountsAcreedor.TabIndex = 69;
			// 
			// dataGridViewTextBoxColumn1
			// 
			this.dataGridViewTextBoxColumn1.HeaderText = "Codigo";
			this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
			this.dataGridViewTextBoxColumn1.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn2
			// 
			this.dataGridViewTextBoxColumn2.HeaderText = "Cuenta";
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			this.dataGridViewTextBoxColumn2.ReadOnly = true;
			this.dataGridViewTextBoxColumn2.Width = 200;
			// 
			// dataGridViewTextBoxColumn3
			// 
			this.dataGridViewTextBoxColumn3.HeaderText = "Monto";
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.ReadOnly = true;
			this.dataGridViewTextBoxColumn3.Width = 150;
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(962, 525);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(100, 33);
			this.btnSave.TabIndex = 68;
			this.btnSave.Text = "Guardar Poliza";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(716, 341);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(120, 15);
			this.label17.TabIndex = 67;
			this.label17.Text = "Cuentas (ACREEDOR)";
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(160, 341);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(107, 15);
			this.label18.TabIndex = 66;
			this.label18.Text = "Cuentas (DEUDOR)";
			// 
			// dgvAccountsDeudor
			// 
			this.dgvAccountsDeudor.AllowUserToAddRows = false;
			this.dgvAccountsDeudor.AllowUserToDeleteRows = false;
			this.dgvAccountsDeudor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvAccountsDeudor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
			this.dgvAccountsDeudor.Location = new System.Drawing.Point(26, 368);
			this.dgvAccountsDeudor.MultiSelect = false;
			this.dgvAccountsDeudor.Name = "dgvAccountsDeudor";
			this.dgvAccountsDeudor.ReadOnly = true;
			this.dgvAccountsDeudor.Size = new System.Drawing.Size(494, 150);
			this.dgvAccountsDeudor.TabIndex = 65;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "Codigo";
			this.Column1.Name = "Column1";
			this.Column1.ReadOnly = true;
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Cuenta";
			this.Column2.Name = "Column2";
			this.Column2.ReadOnly = true;
			this.Column2.Width = 200;
			// 
			// Column3
			// 
			this.Column3.HeaderText = "Monto";
			this.Column3.Name = "Column3";
			this.Column3.ReadOnly = true;
			this.Column3.Width = 150;
			// 
			// txtQuienAutoriza
			// 
			this.txtQuienAutoriza.Location = new System.Drawing.Point(678, 254);
			this.txtQuienAutoriza.Name = "txtQuienAutoriza";
			this.txtQuienAutoriza.Size = new System.Drawing.Size(292, 23);
			this.txtQuienAutoriza.TabIndex = 64;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label14.Location = new System.Drawing.Point(575, 254);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(97, 17);
			this.label14.TabIndex = 63;
			this.label14.Text = "Quien Autoriza:";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label13.Location = new System.Drawing.Point(592, 163);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(79, 17);
			this.label13.TabIndex = 62;
			this.label13.Text = "Descripcion:";
			// 
			// txtDescription
			// 
			this.txtDescription.Location = new System.Drawing.Point(677, 163);
			this.txtDescription.Multiline = true;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(293, 72);
			this.txtDescription.TabIndex = 61;
			// 
			// cmbTipoPoliza
			// 
			this.cmbTipoPoliza.FormattingEnabled = true;
			this.cmbTipoPoliza.Location = new System.Drawing.Point(677, 119);
			this.cmbTipoPoliza.Name = "cmbTipoPoliza";
			this.cmbTipoPoliza.Size = new System.Drawing.Size(139, 21);
			this.cmbTipoPoliza.TabIndex = 60;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(577, 119);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(94, 17);
			this.label5.TabIndex = 59;
			this.label5.Text = "Tipo de Poliza:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(685, 38);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(99, 17);
			this.label2.TabIndex = 58;
			this.label2.Text = "Datos de Poliza";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(129, 245);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(200, 17);
			this.label1.TabIndex = 57;
			this.label1.Text = "Operaciones Sobre Movimientos";
			// 
			// btnRemove
			// 
			this.btnRemove.Location = new System.Drawing.Point(225, 282);
			this.btnRemove.Name = "btnRemove";
			this.btnRemove.Size = new System.Drawing.Size(75, 23);
			this.btnRemove.TabIndex = 56;
			this.btnRemove.Text = "Eliminar";
			this.btnRemove.UseVisualStyleBackColor = true;
			this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(132, 282);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(75, 23);
			this.btnAdd.TabIndex = 55;
			this.btnAdd.Text = "Agregar";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.Location = new System.Drawing.Point(49, 127);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(54, 17);
			this.label11.TabIndex = 54;
			this.label11.Text = "Codigo:";
			// 
			// txtLevel1
			// 
			this.txtLevel1.Location = new System.Drawing.Point(109, 127);
			this.txtLevel1.Name = "txtLevel1";
			this.txtLevel1.Size = new System.Drawing.Size(29, 23);
			this.txtLevel1.TabIndex = 1;
			this.txtLevel1.Text = "0";
			this.txtLevel1.TextChanged += new System.EventHandler(this.txtLevel1_TextChanged);
			this.txtLevel1.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(195, 130);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(12, 15);
			this.label6.TabIndex = 53;
			this.label6.Text = "-";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(246, 130);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(12, 15);
			this.label9.TabIndex = 52;
			this.label9.Text = "-";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(144, 130);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(12, 15);
			this.label10.TabIndex = 51;
			this.label10.Text = "-";
			// 
			// txtLevel2
			// 
			this.txtLevel2.Location = new System.Drawing.Point(160, 127);
			this.txtLevel2.Name = "txtLevel2";
			this.txtLevel2.Size = new System.Drawing.Size(29, 23);
			this.txtLevel2.TabIndex = 2;
			this.txtLevel2.Text = "0";
			this.txtLevel2.TextChanged += new System.EventHandler(this.txtLevel1_TextChanged);
			this.txtLevel2.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// txtLevel3
			// 
			this.txtLevel3.Location = new System.Drawing.Point(211, 127);
			this.txtLevel3.Name = "txtLevel3";
			this.txtLevel3.Size = new System.Drawing.Size(29, 23);
			this.txtLevel3.TabIndex = 3;
			this.txtLevel3.Text = "0";
			this.txtLevel3.TextChanged += new System.EventHandler(this.txtLevel1_TextChanged);
			this.txtLevel3.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// txtLevel4
			// 
			this.txtLevel4.Location = new System.Drawing.Point(262, 127);
			this.txtLevel4.Name = "txtLevel4";
			this.txtLevel4.Size = new System.Drawing.Size(29, 23);
			this.txtLevel4.TabIndex = 4;
			this.txtLevel4.Text = "0";
			this.txtLevel4.TextChanged += new System.EventHandler(this.txtLevel1_TextChanged);
			this.txtLevel4.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// txtAmount
			// 
			this.txtAmount.Location = new System.Drawing.Point(109, 211);
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Size = new System.Drawing.Size(182, 23);
			this.txtAmount.TabIndex = 6;
			this.txtAmount.Text = "0";
			this.txtAmount.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(53, 211);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(50, 17);
			this.label4.TabIndex = 45;
			this.label4.Text = "Monto:";
			// 
			// txtNameAccount
			// 
			this.txtNameAccount.Location = new System.Drawing.Point(109, 171);
			this.txtNameAccount.Name = "txtNameAccount";
			this.txtNameAccount.Size = new System.Drawing.Size(339, 23);
			this.txtNameAccount.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(52, 171);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(51, 17);
			this.label3.TabIndex = 43;
			this.label3.Text = "Cuenta:";
			// 
			// cmbTipoSaldo
			// 
			this.cmbTipoSaldo.FormattingEnabled = true;
			this.cmbTipoSaldo.Location = new System.Drawing.Point(109, 79);
			this.cmbTipoSaldo.Name = "cmbTipoSaldo";
			this.cmbTipoSaldo.Size = new System.Drawing.Size(139, 21);
			this.cmbTipoSaldo.TabIndex = 0;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.Location = new System.Drawing.Point(10, 79);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(93, 17);
			this.label12.TabIndex = 6;
			this.label12.Text = "Tipo de Saldo:";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(129, 38);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(140, 17);
			this.label8.TabIndex = 5;
			this.label8.Text = "Datos de Movimientos";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(383, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(174, 30);
			this.label7.TabIndex = 4;
			this.label7.Text = "Polizas Contables";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.btnModificarPolizas);
			this.panel2.Controls.Add(this.label21);
			this.panel2.Location = new System.Drawing.Point(13, 51);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(135, 305);
			this.panel2.TabIndex = 1;
			// 
			// btnModificarPolizas
			// 
			this.btnModificarPolizas.Location = new System.Drawing.Point(8, 133);
			this.btnModificarPolizas.Name = "btnModificarPolizas";
			this.btnModificarPolizas.Size = new System.Drawing.Size(110, 38);
			this.btnModificarPolizas.TabIndex = 1;
			this.btnModificarPolizas.Text = "Modificar Y Eliminar Poliza";
			this.btnModificarPolizas.UseVisualStyleBackColor = true;
			this.btnModificarPolizas.Click += new System.EventHandler(this.btnModificarPolizas_Click);
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label21.Location = new System.Drawing.Point(5, 57);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(99, 17);
			this.label21.TabIndex = 0;
			this.label21.Text = "Otras Opciones";
			// 
			// PolizasContables
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1238, 583);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Font = new System.Drawing.Font("Segoe UI", 8.5F);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "PolizasContables";
			this.Text = "Polizas";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAccountsAcreedor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvAccountsDeudor)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmbTipoSaldo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtLevel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtLevel2;
        private System.Windows.Forms.TextBox txtLevel3;
        private System.Windows.Forms.TextBox txtLevel4;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNameAccount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.ComboBox cmbTipoPoliza;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtQuienAutoriza;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtTotalAcreedor;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtTotalDeudor;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridView dgvAccountsAcreedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DataGridView dgvAccountsDeudor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Label Number;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DateTimePicker DateTimePoliza;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnModificarPolizas;
        private System.Windows.Forms.Label label21;
    }
}