﻿using Contabilidad.Core;
using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class BalanceGeneral : Form
	{
		private BindingList<BalanceSheet> Balances;
		private BalanceSheet Balance;
		private BindingList<Account> Accounts;
		private List<MovementBalance> Movements;
		private MovementBalance Movement;
        private Usuario User;

		private int IdAccount = 0;
		private bool ExistsAccount = false;
        
		private ErrorProvider Error = new ErrorProvider();

		public BalanceGeneral()
		{
			InitializeComponent();

			var m = Program.getManager();

			Accounts = m.GetNomenclatureManager().GetAccounts();

			Balances = m.GetBalanceManager().GetBalances();
			Movements = new List<MovementBalance>();
            User = new Usuario();
            this.User = m.GetUserManager().GetCurrentUser();

            //cargar balances a Balances
            //cargar Cuentas a Accounts

            cmbTipoCuenta.DataSource = Enum.GetValues(typeof(BalanceAccountType));

			cmbTipoCuenta.SelectedIndex = 0;
		}

		private string UpdateAccount()//actualiza el nombre de la cuenta con respecto al codigo
		{
			IdAccount = 0;
			ExistsAccount = false;

			string Level1, Level2, Level3, Level4;

			txtNameAccount.Text = "";

			Level1 = txtLevel1.Text;
			Level2 = txtLevel2.Text;
			Level3 = txtLevel3.Text;
			Level4 = txtLevel4.Text;

			string Code = Level1 + "." + Level2 + "." + Level3 + "." + Level4;
			for (int i = 0; i < Accounts.Count; i++)
				if (Code == Accounts[i].Code)
				{
					txtNameAccount.Text = Accounts[i].Description;
					IdAccount = i;
					ExistsAccount = true;
					break;
				}

			return Code;
		}

		private string SearchAccount()
		{
			string Code = UpdateAccount();
			ExistsAccount = false;

			foreach (MovementBalance Movement in Movements)
				if (Code == Movement.Account.Code)
				{
					ExistsAccount = true;
					break;
				}

			return Code;
		}

		private void txtLevel1_TextChanged(object sender, EventArgs e) => UpdateAccount();

		private void UpdateDgv()
		{

			int RowsAC = 0; //Filas para activo corriente
			int RowsANC = 0; //Filas para activo no corriente
			int RowsPC = 0; //Filas para pasivo corriente
			int RowsPNC = 0; //Filas para pasico no corriente
			int RowsPA = 0; //Filas para patrimino

			decimal SaldoAC = 0; //saldo activo corriente
			decimal SaldoANC = 0; //saldo activo no corriente
			decimal SaldoPC = 0; //saldo pasivo corriente
			decimal SaldoPNC = 0; //Saldo Pasivo No corriente
			decimal SaldoPA = 0; //Saldo Patrimonio

			//Limpiar Data grid
			dgvActivoCorriente.Rows.Clear();
			dgvActivoNoCorriente.Rows.Clear();
			dgvPasivoCorriente.Rows.Clear();
			dgvPasivoNoCorriente.Rows.Clear();
			dgvPatrimonio.Rows.Clear();

			foreach (MovementBalance Value in Movements)
			{
				switch (Value.Type)
				{
					case BalanceAccountType.ActivoCorriente:
						dgvActivoCorriente.Rows.Add();
						dgvActivoCorriente.Rows[RowsAC].Cells[0].Value = Value.Account.Code;
						dgvActivoCorriente.Rows[RowsAC].Cells[1].Value = Value.Account.Description;
						dgvActivoCorriente.Rows[RowsAC].Cells[2].Value = Value.Saldo;
						SaldoAC += Value.Saldo;
						RowsAC++;
						break;
					case BalanceAccountType.ActivoNoCorriente:
						dgvActivoNoCorriente.Rows.Add();
						dgvActivoNoCorriente.Rows[RowsANC].Cells[0].Value = Value.Account.Code;
						dgvActivoNoCorriente.Rows[RowsANC].Cells[1].Value = Value.Account.Description;
						dgvActivoNoCorriente.Rows[RowsANC].Cells[2].Value = Value.Saldo;
						SaldoANC += Value.Saldo;
						RowsANC++;
						break;
					case BalanceAccountType.PasivoCorriente:
						dgvPasivoCorriente.Rows.Add();
						dgvPasivoCorriente.Rows[RowsPC].Cells[0].Value = Value.Account.Code;
						dgvPasivoCorriente.Rows[RowsPC].Cells[1].Value = Value.Account.Description;
						dgvPasivoCorriente.Rows[RowsPC].Cells[2].Value = Value.Saldo;
						SaldoPC += Value.Saldo;
						RowsPC++;
						break;
					case BalanceAccountType.PasivoNoCorriente:
						dgvPasivoNoCorriente.Rows.Add();
						dgvPasivoNoCorriente.Rows[RowsPNC].Cells[0].Value = Value.Account.Code;
						dgvPasivoNoCorriente.Rows[RowsPNC].Cells[1].Value = Value.Account.Description;
						dgvPasivoNoCorriente.Rows[RowsPNC].Cells[2].Value = Value.Saldo;
						SaldoPNC += Value.Saldo;
						RowsPNC++;
						break;
					case BalanceAccountType.Patrimonio:
						dgvPatrimonio.Rows.Add();
						dgvPatrimonio.Rows[RowsPA].Cells[0].Value = Value.Account.Code;
						dgvPatrimonio.Rows[RowsPA].Cells[1].Value = Value.Account.Description;
						dgvPatrimonio.Rows[RowsPA].Cells[2].Value = Value.Saldo;
						SaldoPA += Value.Saldo;
						RowsPA++;
						break;
				}
			}

			txtTotalActivoCorriente.Text = SaldoAC.ToString();
			txtTotalActivoNoCorriente.Text = SaldoANC.ToString();
			txtTotalPasivoCorriente.Text = SaldoPC.ToString();
			txtPasivoNoCorriente.Text = SaldoPNC.ToString();
			txtTotalPatrimonio.Text = SaldoPA.ToString();
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{

			Error.Clear();
			UpdateAccount();

			if (!ExistsAccount)
			{
				Error.SetError(txtLevel4, "No se Encontro Ninguna Cuenta Con ese Codigo");
				return;
			}
			if (int.Parse(txtAmount.Text) <= 0)
			{
				Error.SetError(txtAmount, "No se Pueden colocar Montos Menores o Iguales a 0");
				return;
			}
			if (cmbTipoCuenta.Text == string.Empty)
			{
				Error.SetError(cmbTipoCuenta, "La Cuenta Necesita Un tipo");
				return;
			}

			SearchAccount();
			if (ExistsAccount)
			{
				Error.SetError(btnAdd, "Esta Cuenta YA se Utilizo En Un Movimiento");
				return;
			}

			Movement = new MovementBalance
			{
				Date = DateTime.Now,
				Account = Accounts[IdAccount],
				Saldo = int.Parse(txtAmount.Text),
				Type = (BalanceAccountType)cmbTipoCuenta.SelectedIndex,
			};

			Movements.Add(Movement);

			UpdateDgv();
		}

		private void btnRemove_Click(object sender, EventArgs e)
		{
			Error.Clear();
			string Code = SearchAccount();

			if (!ExistsAccount)
			{
				Error.SetError(btnRemove, "No hay Ningun Movimiento Con Esta Cuenta");
				return;
			}

			foreach (MovementBalance Value in Movements)
			{
				if (Code == Value.Account.Code)
				{
					Movements.Remove(Value);
					MessageBox.Show($"Se ha Eliminado el Movimiento Con cuenta {Value.Account.Description}");
					UpdateDgv();
					return;
				}
			}
		}

		private void btnGuardarPartida_Click(object sender, EventArgs e)
		{
			UpdateDgv();
			Error.Clear();

			if (Movements.Count == 0)
			{
				Error.SetError(btnGuardarBalance, "EL Balance Necesita Movimientos");
				return;
			}

			if ((int.Parse(txtTotalActivoCorriente.Text) + int.Parse(txtTotalActivoNoCorriente.Text)) !=
				(int.Parse(txtTotalPasivoCorriente.Text) + int.Parse(txtPasivoNoCorriente.Text) + int.Parse(txtTotalPatrimonio.Text)))
			{
				Error.SetError(btnGuardarBalance, "Los Totales No Son Iguales");
				return;
			}

			//si no hay errores agregar los movimientos al balance
			Balance = new BalanceSheet
			{
				Date = DateTime.Now,
				Description = "Balance General, Cifra en Quetzales",
			};

			if (Balances.Count == 0)
			{
				Balance.Number = 1;
			}
			else
			{
				Balance.Number = (Balances.Last().Number + 1);
			}

			Balance.AgregarMovimientos(Movements);

			Balances.Add(Balance);

			MessageBox.Show("Balance Guardado");

			//limpiar datos
			Movements.Clear();
			UpdateDgv();
            txtAmount.Clear();
            txtNameAccount.Clear();

		}

		private void ValidateTxt(object sender, EventArgs e)
		{
			int b;
			if (!int.TryParse(((TextBox)sender).Text, out b))
				((TextBox)sender).Text = "0";
		}

        private void btnModificarBalance_Click(object sender, EventArgs e)
        {
            if (User.Puesto == UserPosition.Contabilidad)
            {
                new ListarBalances().Show();
            }
            else
            {
                MessageBox.Show("Usted No Tiene Acceso a la Eliminacion y Modificacion De Datos");
            }
            
        }
    }
}
