﻿using Contabilidad.Core.Classes;
using Contabilidad.Core.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class RegisterUser : Form
	{

		private UserManager um = Program.getManager().GetUserManager();
		private ErrorProvider Error = new ErrorProvider();

		private Usuario us;

		public RegisterUser(Usuario us)
		{
			InitializeComponent();
			this.us = us;
		}

		private void Loaded(object sender, EventArgs e)
		{
			cmbPuesto.DataSource = Enum.GetValues(typeof(UserPosition));
			if (um.GetUsers().Count == 0)
			{
				btnRegresar.Visible = false;
				cmbPuesto.SelectedIndex = (int)UserPosition.Administrador;
				cmbPuesto.Visible = false;
				disabledType.Text = UserPosition.Administrador.ToString();
				disabledType.Visible = true;
			}
			if (us != null)
			{
				txtUser.ReadOnly = true;
				if (Program.getManager().GetUserManager().GetCurrentUser().Puesto != UserPosition.Administrador)
				{
					cmbPuesto.Visible = false;
					disabledType.Visible = true;
					disabledType.Text = us.Puesto.ToString();
				}
				txtUser.Text = us.User;
				txtPassword.Text = us.Password;
				txtPasswordConfirmation.Text = us.Password;
				cmbPuesto.SelectedIndex = (int)us.Puesto;
				btnRegistrarse.Visible = false;
				labelRegister.Visible = false;
				buttonModify.Visible = true;
				labelModify.Visible = true;
			}
		}

		private void btnRegistrarse_Click(object sender, EventArgs e)
		{
			Error.Clear();

			UserPosition pos;

			Enum.TryParse<UserPosition>(cmbPuesto.SelectedValue.ToString(), out pos);

			var re = um.Register(txtUser.Text, txtPassword.Text, txtPasswordConfirmation.Text, pos);

			switch (re)
			{
				case RegisterErrors.EMPTY_USER:
					Error.SetError(txtUser, "Ingrese un nombre.");
					break;
				case RegisterErrors.EMPTY_PASSWORD:
					Error.SetError(txtPassword, "Ingrese una contraseña.");
					break;
				case RegisterErrors.MISMATCH_PASSWORD:
					Error.SetError(txtPasswordConfirmation, "No coinciden las contraseñas.");
					break;
				case RegisterErrors.OK:
					if (um.GetUsers().Count == 1)
						um.Login(txtUser.Text, txtPassword.Text); // Auto-iniciar
					Close();
					break;
				case RegisterErrors.EXIST:
					Error.SetError(txtUser, "Ya existe un usuario con ese nombre.");
					break;
				case RegisterErrors.MISMATCH_USER:
					// No se usa aqui
					break;
			}
		}

		private void FormClose(object sender, FormClosingEventArgs e)
		{
			if (um.GetUsers().Count == 0)
			{
				var window = MessageBox.Show("Se necesita un usuario para utilizar la aplicacion, desea salir?", "Salir sin registrar usuario", MessageBoxButtons.YesNo);
				e.Cancel = (window == DialogResult.No);
			}
		}

		private void btnRegresar_Click(object sender, EventArgs e) => Close();

		private void buttonModify_Click(object sender, EventArgs e)
		{
			UserPosition pos;
			Enum.TryParse<UserPosition>(cmbPuesto.SelectedValue.ToString(), out pos);
			var re = um.Modify(txtUser.Text, txtPassword.Text, txtPasswordConfirmation.Text, pos);
			switch (re)
			{
				case RegisterErrors.EMPTY_USER:
					Error.SetError(txtUser, "Ingrese un nombre.");
					break;
				case RegisterErrors.EMPTY_PASSWORD:
					Error.SetError(txtPassword, "Ingrese una contraseña.");
					break;
				case RegisterErrors.MISMATCH_PASSWORD:
					Error.SetError(txtPasswordConfirmation, "No coinciden las contraseñas.");
					break;
				case RegisterErrors.OK:
					Close();
					break;
				case RegisterErrors.EXIST:
					// No se usa aqui
					break;
				case RegisterErrors.MISMATCH_USER:
					// No se usa aqui
					break;
			}
		}
	}
}
