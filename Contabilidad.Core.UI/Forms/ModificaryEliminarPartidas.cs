﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class ModificaryEliminarPartidas : Form
	{
		BindingList<Accounting_Party> AccountingParty;
		private BindingList<Account> Accounts;

        private Accounting_Party current;

		private List<AccountingMovement> MovementsDebe;
		private List<AccountingMovement> MovementsHaber;

		private AccountingMovement Movement;

		ErrorProvider Error = new ErrorProvider();
		int idAccount = 0;
		int idPartida = 0;
		bool ExistAccount = false;

		public ModificaryEliminarPartidas()
		{
			InitializeComponent();
			AccountingParty = new BindingList<Accounting_Party>();
			Accounts = new BindingList<Account>();
			MovementsDebe = new List<AccountingMovement>();
			MovementsHaber = new List<AccountingMovement>();
            current = new Accounting_Party();

            var m = Program.getManager();

            AccountingParty = m.GetDepartureManager().GetDepartures();
            //cargar partidas a la lista accountingItems
            Accounts = m.GetNomenclatureManager().GetAccounts();
            //cargar las cuentas a la lista Accounts

            cmbTypeAccount.DataSource = Enum.GetValues(typeof(TypeBalance));
        }

        private void btnFiltrar_Click_1(object sender, EventArgs e)
		{
			int Rows = 0;
            dgvPartidas.Rows.Clear();
			foreach (Accounting_Party value in AccountingParty)
			{
				dgvPartidas.Rows.Add();
				dgvPartidas.Rows[Rows].Cells[0].Value = value.Number;
				dgvPartidas.Rows[Rows].Cells[1].Value = value.Date;
				dgvPartidas.Rows[Rows].Cells[2].Value = value.Description;
				Rows++;
			}
		}
		private void UpdateTotales(List<AccountingMovement> Debe, List<AccountingMovement> Haber)
		{
			decimal TotalDebe = 0;
			decimal TotalHaber = 0;

			foreach (AccountingMovement value in Haber)
			{
				if (value.Haber != 0)
				{
					TotalHaber += value.Haber;
				}
			}
			foreach (AccountingMovement value in Debe)
			{
				if (value.Debe != 0)
				{
					TotalDebe += value.Debe;
				}
			}

			txtTotalDebe.Text = Convert.ToString(TotalDebe);
			txtTotalHaber.Text = Convert.ToString(TotalHaber);

			return;
		}

		private string UpdateAccount()//actualiza el nombre de la cuenta con respecto al codigo
		{
			idAccount = 0;
			ExistAccount = false;

			string Level1, Level2, Level3, Level4;

			txtNameAccount.Text = "";

			if (txtLevel1.Text == "") { Level1 = "0"; } else { Level1 = txtLevel1.Text; }
			if (txtLevel2.Text == "") { Level2 = "0"; } else { Level2 = txtLevel2.Text; }
			if (txtLevel3.Text == "") { Level3 = "0"; } else { Level3 = txtLevel3.Text; }
			if (txtLevel4.Text == "") { Level4 = "0"; } else { Level4 = txtLevel4.Text; }

			string Code = (Level1 + "." + Level2 + "." + Level3 + "." + Level4);
			for (int i = 0; i < Accounts.Count; i++)
			{
				if (Code == Accounts[i].Code)
				{
					txtNameAccount.Text = Accounts[i].Description;
					idAccount = i;
					ExistAccount = true;
				}
			}
			return Code;
		}
		private string SearchAccount()
		{
			string Code = UpdateAccount();
			ExistAccount = false;

			foreach (AccountingMovement Movement in current.Debe)
			{
				if (Code == Movement.Account.Code)
				{
					ExistAccount = true;
					return Code;
				}
			}
			foreach (AccountingMovement Movement in current.Haber)
			{
				if (Code == Movement.Account.Code)
				{
					ExistAccount = true;
					return Code;
				}
			}
			return Code;
		}

		private void UpdateMovements(Accounting_Party PartidasContables)
		{
			int RowsDebe = 0;
			int RowsHaber = 0;
            dgvAccountsDebe.Rows.Clear();
            dgvAccountsHaber.Rows.Clear();
            foreach (AccountingMovement Movement in PartidasContables.Debe)
            {
                if (Movement.Debe != 0)
                {
                    dgvAccountsDebe.Rows.Add();
                    dgvAccountsDebe.Rows[RowsDebe].Cells[0].Value = Movement.Account.Code;
                    dgvAccountsDebe.Rows[RowsDebe].Cells[1].Value = Movement.Account.Description;
                    dgvAccountsDebe.Rows[RowsDebe].Cells[2].Value = Movement.Debe;
                    RowsDebe++;
                }
                
            }
            foreach(var Movement in PartidasContables.Haber)
            {
                if (Movement.Haber != 0)
                {
                    dgvAccountsHaber.Rows.Add();
                    dgvAccountsHaber.Rows[RowsHaber].Cells[0].Value = Movement.Account.Code;
                    dgvAccountsHaber.Rows[RowsHaber].Cells[1].Value = Movement.Account.Description;
                    dgvAccountsHaber.Rows[RowsHaber].Cells[2].Value = Movement.Haber;
                    RowsHaber++;
                }
            }
            UpdateTotales(current.Debe,current.Haber);
			return;
		}

		private void btnUpdatePartida_Click(object sender, EventArgs e)
		{
			Error.Clear();
			idPartida = 0;

			try
			{
				foreach (var value in AccountingParty)
				{
					if (value.Number == (int.Parse(txtNumberPartida.Text)))
					{
                        current = value;
                        UpdateMovements(value);
                        Number.Text = Convert.ToString(value.Number);                       
						txtDescription.Text = value.Description;
						cmbTypeAccount.Text = value.Type;
						DateTimePartida.Value = value.Date;
                        return;
					}
					idPartida++;
				}
				Error.SetError(btnUpdatePartida, "No existe ninguna partida con ese numero.");
				return;
			}
			catch (Exception Ex)
			{
				MessageBox.Show(Ex.Message);
			}

		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			UpdateAccount();
			Error.Clear();
			try
			{
				if (ExistAccount == false)
				{
					Error.SetError(txtLevel4, "La cuenta no ha sido encontrada en la Nomenclatura Contable");
					return;
				}
				if (cmbTypeAccount.Text == String.Empty)
				{
					Error.SetError(cmbTypeAccount, "Debe seleccionar un tipo de cuenta");
					return;
				}
				if (txtAmount.Text == String.Empty)
				{
					Error.SetError(txtAmount, "Para realizar el movimiento debe ingresar un monto");
					return;
				}
				SearchAccount();

				if (ExistAccount == true)
				{
					Error.SetError(txtLevel4, "La cuenta que desea usar, ya ha sido usada para otro movimiento");
					return;
				}

				Movement = new AccountingMovement
				{
					Date = DateTime.Now,
					Account = Accounts[idAccount]
				};

				if (cmbTypeAccount.Text == "Debe")
				{
					Movement.Debe = (decimal.Parse(txtAmount.Text));

					dgvAccountsDebe.Rows.Add();
					dgvAccountsDebe.Rows[current.Debe.Count].Cells[0].Value = Movement.Account.Code;
					dgvAccountsDebe.Rows[current.Debe.Count].Cells[1].Value = Movement.Account.Description;
					dgvAccountsDebe.Rows[current.Debe.Count].Cells[2].Value = Movement.Debe;
                    current.Debe.Add(Movement);
				}
				if (cmbTypeAccount.Text == "Haber")
				{
					Movement.Haber = (decimal.Parse(txtAmount.Text));

					dgvAccountsHaber.Rows.Add();
					dgvAccountsHaber.Rows[current.Haber.Count].Cells[0].Value = Movement.Account.Code;
					dgvAccountsHaber.Rows[current.Haber.Count].Cells[1].Value = Movement.Account.Description;
					dgvAccountsHaber.Rows[current.Haber.Count].Cells[2].Value = Movement.Haber;
                    current.Haber.Add(Movement);
				}

				UpdateTotales(current.Debe, current.Haber);
				txtLevel1.Text = "";
				txtLevel2.Text = "";
				txtLevel3.Text = "";
				txtLevel4.Text = "";
				txtAmount.Text = "";
				btnAdd.Focus();
			}
			catch (Exception Ex)
			{
				MessageBox.Show(Ex.Message);
			}

		}

		private void btnRemove_Click(object sender, EventArgs e)
		{
			Error.Clear();

			string Code = SearchAccount();
			if (ExistAccount == false)
			{
				Error.SetError(btnRemove, "Esta Cuenta No ha sido Utilizada");
				return;
			}
			for(int i=0;i<current.Debe.Count;i++)
			{
				if (current.Debe[i].Account.Code == Code)
				{
					int Rows = 0;

					current.Debe.Remove(current.Debe[i]);
					dgvAccountsDebe.Rows.Clear();

					foreach (AccountingMovement Value in current.Debe)
					{
						dgvAccountsDebe.Rows.Add();
						dgvAccountsDebe.Rows[Rows].Cells[0].Value = Value.Account.Code;
						dgvAccountsDebe.Rows[Rows].Cells[1].Value = Value.Account.Description;
						dgvAccountsDebe.Rows[Rows].Cells[2].Value = Value.Debe;
						Rows++;
					}

				}
			}
			for (int i=0;i<current.Haber.Count;i++)
			{
				if (current.Haber[i].Account.Code == Code)
				{
					int Rows = 0;

					current.Haber.Remove(current.Haber[i]);
					dgvAccountsHaber.Rows.Clear();

					foreach (AccountingMovement Value in current.Haber)
					{
						dgvAccountsHaber.Rows.Add();
						dgvAccountsHaber.Rows[Rows].Cells[0].Value = Value.Account.Code;
						dgvAccountsHaber.Rows[Rows].Cells[1].Value = Value.Account.Description;
						dgvAccountsHaber.Rows[Rows].Cells[2].Value = Value.Haber;
						Rows++;
					}

				}
			}
			UpdateTotales(current.Debe,current.Haber);
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			Error.Clear();

			if (Number.Text == string.Empty)
			{
				Error.SetError(btnSave, "No hay ninguna Partida Seleccionada");
				return;
			}

			if (current.Debe.Count == 0)
			{
				Error.SetError(btnSave, "Una partida Debe Contener Movimientos DEBE");
				return;
			}

			if (current.Haber.Count == 0)
			{
				Error.SetError(btnSave, "Una Cuenta Debe Contener Movimientos HABER");
				return;
			}

			if (txtDescription.Text == string.Empty)
			{
				Error.SetError(txtDescription, "Una Cuenta debe Contener Una descripcion");
				return;
			}

			UpdateTotales(current.Debe,current.Haber);

			if (txtTotalDebe.Text != txtTotalHaber.Text || txtTotalDebe.Text == string.Empty || txtTotalHaber.Text == string.Empty)
			{
				Error.SetError(btnSave, "Los Saldos son diferentes o son iguales a 0");
				return;
			}

            // AccountingParty[idPartida].AccountingMovements.Clear();

            //foreach (AccountingMovement Value in MovementsDebe)
            //{
            //    AccountingParty[idPartida].AccountingMovements.Add(Value);
            //}
            //foreach (AccountingMovement Value in MovementsHaber)
            //{
            //    AccountingParty[idPartida].AccountingMovements.Add(Value);
            //}
            //AccountingParty[idPartida].Date = Convert.ToDateTime(DateTimePartida.Text);
            //AccountingParty[idPartida].Description = txtDescription.Text;
            //AccountingParty[idPartida].Type = cmbTypeAccount.Text;
            current.Description = txtDescription.Text;
            current.Date = DateTimePartida.Value;

			txtLevel1.Clear();
			txtLevel2.Clear();
			txtLevel3.Clear();
			txtLevel4.Clear();
			txtNameAccount.Clear();
			txtAmount.Clear();
			txtDescription.Clear();
			txtTotalDebe.Clear();
			txtTotalHaber.Clear();
			DateTimePartida.Text = Convert.ToString(DateTime.Now);
			cmbTypeAccount.SelectedIndex = 0;
            dgvAccountsDebe.Rows.Clear();
            dgvAccountsHaber.Rows.Clear();
			MessageBox.Show("Partida Modificada Con Exito");
            Number.Text = "";
			txtNumberPartida.Focus();
			return;
		}

		private void btnEliminarPartida_Click(object sender, EventArgs e)
		{
			Error.Clear();
			try
			{
				foreach (Accounting_Party Value in AccountingParty)
				{
					if (txtNumberPartida.Text == Convert.ToString(Value.Number))
					{
						AccountingParty.Remove(Value);
						MessageBox.Show("SE HA ELIMINADO LA PARTIDA");
						btnFiltrar.PerformClick();
						return;
					}
				}

				Error.SetError(btnEliminarPartida, "NO SE HA ENCONTRADO NINGUNA PARTIDA CON ESE CODIGO");
			}
			catch (Exception Ex)
			{
				MessageBox.Show(Ex.Message);
			}
		}

        private void txtLevel1_TextChanged(object sender, EventArgs e) => UpdateAccount();

        private void txtLevel2_TextChanged(object sender, EventArgs e) => UpdateAccount();

        private void txtLevel3_TextChanged(object sender, EventArgs e) => UpdateAccount();

        private void txtLevel4_TextChanged(object sender, EventArgs e) => UpdateAccount();
    }
}
