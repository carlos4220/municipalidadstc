﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class LibroMayor : Form
	{
		BindingList<Accounting_Party> AccountingParty;
		private BindingList<Account> Accounts;

		public LibroMayor()
		{
			InitializeComponent();
            var m = Program.getManager();
            
            //cargar los datos a la lista AccountingParty
            AccountingParty = m.GetDepartureManager().GetDepartures();
            //cargar las cuentas de momenclatura a la lista Accounts
            Accounts= m.GetNomenclatureManager().GetAccounts();

        }

		private void btnActualizarLibro_Click(object sender, EventArgs e)
		{
			int FilasDebe = 0;
			int FilasHaber = 0;
			int FilaCuenta = 0;
			decimal SaldoDebe = 0;
			decimal SaldoHaber = 0;

			dgvLibroMayor.Rows.Clear();
			foreach (Account Account in Accounts) // recorre todas las cuentas
			{
				foreach (Accounting_Party Departure in AccountingParty)   // rrecorre todas las partidas 
				{

					if (DateTime.Compare(FechaDesde.Value,Departure.Date)<0 && //evalua si la partida cumple con la fecha
						DateTime.Compare(Departure.Date,FechaHasta.Value)<0)
					{
						foreach (AccountingMovement Movement in Departure.Debe) //rrecorre los movim. de cada partida
						{
							if (Movement.Account.Code == Account.Code)  //evalua si la cuenta esta incluida en ese movimiento
							{
								dgvLibroMayor.Rows.Add();

								//Mostrar saldo debe a la Izquierda
								dgvLibroMayor.Rows[FilasDebe].Cells[0].Value = ("Pda " + Departure.Number);
								dgvLibroMayor.Rows[FilasDebe].Cells[1].Value = Departure.Date;
								dgvLibroMayor.Rows[FilasDebe].Cells[2].Value = Movement.Debe;
								SaldoDebe += Movement.Debe;
								FilasDebe++;
							}
						}
						foreach (AccountingMovement Movement in Departure.Haber) //rrecorre los movim. de cada partida
						{
							if (Movement.Account.Code == Account.Code)  //evalua si la cuenta esta incluida en ese movimiento
							{
								dgvLibroMayor.Rows.Add();

								//Mostrar Saldo Haber a la Derecha
								dgvLibroMayor.Rows[FilasHaber].Cells[4].Value = ("Pda " + Departure.Number);
								dgvLibroMayor.Rows[FilasHaber].Cells[5].Value = Departure.Date;
								dgvLibroMayor.Rows[FilasHaber].Cells[6].Value = Movement.Haber;
								SaldoHaber += Movement.Haber;
								FilasHaber++;
							}
						}
					}
				}
                if (SaldoDebe>0 || SaldoHaber > 0)
                {
                    dgvLibroMayor.Rows[FilaCuenta].Cells[3].Value = Account.Description;
                }
                if (SaldoDebe > 0)
                {
                    dgvLibroMayor.Rows.Add();
                    dgvLibroMayor.Rows[FilasDebe].Cells[1].Value = "TOTAL"; //total Debe de la cuenta
                    dgvLibroMayor.Rows[FilasDebe].Cells[2].Value = SaldoDebe;
                    FilasDebe++;
                }

                if (SaldoHaber > 0)
                {
                    dgvLibroMayor.Rows.Add();
                    dgvLibroMayor.Rows[FilasHaber].Cells[5].Value = "TOTAL"; //total Haber de la cuenta
                    dgvLibroMayor.Rows[FilasHaber].Cells[6].Value = SaldoHaber;
                    FilasHaber++;
                }
				if (FilasDebe > FilasHaber) { FilasHaber = FilasDebe; FilaCuenta = FilasDebe; }
				else { FilasDebe = FilasHaber; FilaCuenta = FilasHaber; }

                SaldoHaber = 0;
                SaldoDebe = 0;
			}
		}
	}
}
