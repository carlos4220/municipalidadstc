﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class ListarBalances : Form
	{
		private BindingList<BalanceSheet> Balances;
		public ListarBalances()
		{
			InitializeComponent();
			var m = Program.getManager();
			Balances = m.GetBalanceManager().GetBalances();
			UpdateDgv();
		}

		public void UpdateDgv()
		{
			int Rows = 0;
			dgvLista.Rows.Clear();

			foreach (var Value in Balances)
			{
				dgvLista.Rows.Add();
				dgvLista.Rows[Rows].Cells[0].Value = Value.Number;
				dgvLista.Rows[Rows].Cells[1].Value = Value.Date;
                Rows++;
			}
		}

		private void btnIrBalance_Click(object sender, EventArgs e)
		{

			foreach (var Value in Balances)
			{
				if (Value.Number == int.Parse(txtNumber.Text))
				{
					new ModificarYEliminarBalance(Value).Show();
					return;
				}
			}
			MessageBox.Show("No hay Ningun Balance con ese Numero");
		}

		private void ValidateTxt(object sender, EventArgs e)
		{
			int b;
			if (!int.TryParse(((TextBox)sender).Text, out b))
				((TextBox)sender).Text = "0";
		}
	}
}
