﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class PolizasContables : Form
	{
		private BindingList<AccountingPolicies> AccountingPolicies;
		private AccountingPolicies AccountingPolicy;

		private BindingList<Account> Accounts;

		private List<AccountingMovement> MovementsDebe;
		private List<AccountingMovement> MovementsHaber;
		private AccountingMovement Movement;

		private Usuario Usuario;

		private int IdAccount = 0;
		private bool ExistsAccount = false;

		private ErrorProvider Error = new ErrorProvider();

		public PolizasContables()
		{
			InitializeComponent();

			var m = Program.getManager();

			AccountingPolicies = m.GetPolicyManager().GetAccountingPolicies();
			Accounts = m.GetNomenclatureManager().GetAccounts();

			MovementsDebe = new List<AccountingMovement>();
			MovementsHaber = new List<AccountingMovement>();

			this.Usuario = m.GetUserManager().GetCurrentUser();

			cmbTipoSaldo.DataSource = Enum.GetValues(typeof(BalanceType));

			cmbTipoPoliza.DataSource = Enum.GetValues(typeof(PolicyType));

			if (AccountingPolicies.Count == 0)
				Number.Text = "1";
			else
				Number.Text = (AccountingPolicies.Last().Number + 1).ToString();

			txtLevel1.Focus();
		}

		private void UpdateTotales(List<AccountingMovement> Debe, List<AccountingMovement> Haber)
		{
			decimal TotalDebe = 0;
			decimal TotalHaber = 0;

			foreach (var Value in Debe)
					TotalDebe += Value.Debe;

			foreach (var Value in Haber)
					TotalHaber += Value.Haber;

			txtTotalAcreedor.Text = Convert.ToString(TotalHaber);
			txtTotalDeudor.Text = Convert.ToString(TotalDebe);
		}

		private string UpdateAccount()//actualiza el nombre de la cuenta con respecto al codigo
		{
			IdAccount = 0;
			ExistsAccount = false;

			string Level1, Level2, Level3, Level4;

			txtNameAccount.Text = "";

			Level1 = txtLevel1.Text;
			Level2 = txtLevel2.Text;
			Level3 = txtLevel3.Text;
			Level4 = txtLevel4.Text;

			string Code = Level1 + "." + Level2 + "." + Level3 + "." + Level4;
			for (int i = 0; i < Accounts.Count; i++)
				if (Code == Accounts[i].Code)
				{
					txtNameAccount.Text = Accounts[i].Description;
					IdAccount = i;
					ExistsAccount = true;
					break;
				}

			return Code;
		}

		private string SearchAccount()
		{
			string Code = UpdateAccount();
			ExistsAccount = false;

			foreach (AccountingMovement Movement in MovementsDebe)
				if (Code == Movement.Account.Code)
				{
					ExistsAccount = true;
					break;
				}
			foreach (AccountingMovement Movement in MovementsHaber)
				if (Code == Movement.Account.Code)
				{
					ExistsAccount = true;
					break;
				}
			return Code;
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			UpdateAccount();
			Error.Clear();

			if (ExistsAccount == false)
			{
				Error.SetError(txtLevel4, "Cuenta No encontrada En Nomenclatura Contable");
				return;
			}

			if (cmbTipoSaldo.Text == string.Empty)
			{
				Error.SetError(cmbTipoSaldo, "Debe seleccionar Un tipo De saldo");
				return;
			}

			if (txtAmount.Text == string.Empty)
			{
				Error.SetError(txtAmount, "Para Agregar Un Movimiento Se Debe Ingresar Un Monto");
				return;
			}

			if (decimal.Parse(txtAmount.Text) <= 0)
			{
				Error.SetError(txtAmount, "No Puede agregar un monto igual o menor a 0");
				return;
			}

			SearchAccount();

			if (ExistsAccount == true)
			{
				Error.SetError(txtLevel4, "Esta Cuenta Ya Ha sido Utilizada En Algun Movimiento");
				return;
			}

			//si no hay error agregar El Movimiento al lugar correspondiente

			Movement = new AccountingMovement
			{
				Date = DateTime.Now,
				Account = Accounts[IdAccount]
			};

			if ((BalanceType)cmbTipoSaldo.SelectedIndex == BalanceType.Deudor)
			{
				Movement.Debe = decimal.Parse(txtAmount.Text);

				dgvAccountsDeudor.Rows.Add();
				dgvAccountsDeudor.Rows[MovementsDebe.Count].Cells[0].Value = Movement.Account.Code;
				dgvAccountsDeudor.Rows[MovementsDebe.Count].Cells[1].Value = Movement.Account.Description;
				dgvAccountsDeudor.Rows[MovementsDebe.Count].Cells[2].Value = Movement.Debe;

				MovementsDebe.Add(Movement);

			}

			if ((BalanceType)cmbTipoSaldo.SelectedIndex == BalanceType.Acreedor)
			{
				Movement.Haber = decimal.Parse(txtAmount.Text);

				dgvAccountsAcreedor.Rows.Add();
				dgvAccountsAcreedor.Rows[MovementsHaber.Count].Cells[0].Value = Movement.Account.Code;
				dgvAccountsAcreedor.Rows[MovementsHaber.Count].Cells[1].Value = Movement.Account.Description;
				dgvAccountsAcreedor.Rows[MovementsHaber.Count].Cells[2].Value = Movement.Haber;

				MovementsHaber.Add(Movement);
			}

			UpdateTotales(MovementsDebe, MovementsHaber);
			txtLevel1.Text = "0";
			txtLevel2.Text = "0";
			txtLevel3.Text = "0";
			txtLevel4.Text = "0";
			txtAmount.Text = "0";
			btnAdd.Focus();
		}

		private void txtLevel1_TextChanged(object sender, EventArgs e) => UpdateAccount();

		private void btnRemove_Click(object sender, EventArgs e)
		{
			Error.Clear();

			string Code = SearchAccount();
			if (!ExistsAccount) //La cuenta se Utilizo?
			{
				Error.SetError(btnRemove, "Esta Cuenta No ha sido Utilizada");
				return;
			}

            //si se Utilizo
            
			for (int i=0;i<MovementsDebe.Count;i++)
			{
                AccountingMovement Movement = new AccountingMovement();
                Movement = MovementsDebe[i];

				if (Movement.Account.Code == Code)
				{
					int Rows = 0;
                    MovementsDebe.Remove(Movement);
					dgvAccountsDeudor.Rows.Clear();

					foreach (AccountingMovement Value in MovementsDebe)//actualiza dgvaccountsDeudor
					{
						dgvAccountsDeudor.Rows.Add();
						dgvAccountsDeudor.Rows[Rows].Cells[0].Value = Value.Account.Code;
						dgvAccountsDeudor.Rows[Rows].Cells[1].Value = Value.Account.Description;
						dgvAccountsDeudor.Rows[Rows].Cells[2].Value = Value.Debe;
						Rows++;
					}

				}
			}

            for(int i=0;i<MovementsHaber.Count;i++)
			{
                AccountingMovement Movement = new AccountingMovement();
                Movement = MovementsHaber[i];
				if (Movement.Account.Code == Code)
				{
					int Rows = 0;

                    MovementsHaber.Remove(Movement);
					dgvAccountsAcreedor.Rows.Clear();

					foreach (AccountingMovement Value in MovementsHaber) //Actualiza DgvAccountsAcreedor
					{
						dgvAccountsAcreedor.Rows.Add();
						dgvAccountsAcreedor.Rows[Rows].Cells[0].Value = Value.Account.Code;
						dgvAccountsAcreedor.Rows[Rows].Cells[1].Value = Value.Account.Description;
						dgvAccountsAcreedor.Rows[Rows].Cells[2].Value = Value.Haber;
						Rows++;
					}

				}
			}
			UpdateTotales(MovementsDebe, MovementsHaber);
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			Error.Clear();

			if (cmbTipoPoliza.Text == string.Empty)
			{
				Error.SetError(cmbTipoPoliza, "Se debe selccionar Un Tipo de poliza");
				return;
			}

			if (MovementsDebe.Count == 0)
			{
				Error.SetError(btnSave, "Una Poliza Debe Contener Movimientos DEBE");
				return;
			}

			if (MovementsHaber.Count == 0)
			{
				Error.SetError(btnSave, "Una Poliza Debe Contener Movimientos Haber");
				return;
			}

			if (txtDescription.Text == string.Empty)
			{
				Error.SetError(txtDescription, "Una Poliza debe Contener Una descripcion");
				return;
			}

			UpdateTotales(MovementsDebe, MovementsHaber);//para actualizar los totales antes de evaluar

			if (txtTotalAcreedor.Text != txtTotalDeudor.Text || txtTotalAcreedor.Text == string.Empty || txtTotalDeudor.Text == string.Empty)
			{
				Error.SetError(btnSave, "Los Saldos son diferentes o son iguales a 0");
				return;
			}

			if (txtQuienAutoriza.Text == string.Empty)
			{
				Error.SetError(txtQuienAutoriza, "Se Necesita Saber el Nombre de Quien Autoriza");
				return;
			}


			AccountingPolicy = new AccountingPolicies
			{
				Date = DateTimePoliza.Value,
				Description = txtDescription.Text,
				Type = cmbTipoPoliza.Text,
				Authorizer = txtQuienAutoriza.Text,
				Developer = Usuario.User
			};

			if (AccountingPolicies.Count == 0)
				AccountingPolicy.Number = 1;
			else
				AccountingPolicy.Number = AccountingPolicies.Last().Number + 1;


			AccountingPolicy.Debe.AddRange(MovementsDebe);
			AccountingPolicy.Haber.AddRange(MovementsHaber);

			//guardar la partida en el archivo de texto

			AccountingPolicies.Add(AccountingPolicy);

			//Limpia Formulario
			dgvAccountsAcreedor.Rows.Clear();   //LIMPIA FORMULARIO
			dgvAccountsDeudor.Rows.Clear();
			txtLevel1.Text = "0";
			txtLevel2.Text = "0";
			txtLevel3.Text = "0";
			txtLevel4.Text = "0";
			txtNameAccount.Clear();
			txtAmount.Text = "0";
			txtDescription.Clear();
			txtQuienAutoriza.Clear();
			txtTotalAcreedor.Clear();
			txtTotalDeudor.Clear();

			//limpiar los movimientos
			MovementsDebe.Clear();
			MovementsHaber.Clear();

			//actuliza los combo box y datatime
			DateTimePoliza.Value = DateTime.Now;

			cmbTipoPoliza.SelectedIndex = 0;
			cmbTipoSaldo.SelectedIndex = 0;

			MessageBox.Show("Poliza Agregada con Exito");
			Number.Text = (AccountingPolicies.Last().Number + 1).ToString();
			txtLevel1.Focus();
		}

		private void btnModificarPolizas_Click(object sender, EventArgs e)
		{
			if (Usuario.Puesto == UserPosition.Contabilidad)
				new ModificarYEliminarPoliza().Show();
			else
				MessageBox.Show("Usted no Tiene Acceso a Modificacion Y eliminacion de Datos");

		}

		private void ValidateTxt(object sender, EventArgs e)
		{
			int b;
			if (!int.TryParse(((TextBox)sender).Text, out b))
				((TextBox)sender).Text = "0";
		}
	}
}
