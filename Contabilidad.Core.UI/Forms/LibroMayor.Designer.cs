﻿namespace Contabilidad.Core.UI.Forms
{
    partial class LibroMayor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LibroMayor));
			this.panel1 = new System.Windows.Forms.Panel();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.dgvLibroMayor = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.btnActualizarLibro = new System.Windows.Forms.Button();
			this.FechaHasta = new System.Windows.Forms.DateTimePicker();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.FechaDesde = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvLibroMayor)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.dgvLibroMayor);
			this.panel1.Controls.Add(this.btnActualizarLibro);
			this.panel1.Controls.Add(this.FechaHasta);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.FechaDesde);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Location = new System.Drawing.Point(29, 12);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(985, 553);
			this.panel1.TabIndex = 0;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(742, 136);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(55, 16);
			this.label6.TabIndex = 12;
			this.label6.Text = "HABER";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(178, 136);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(45, 16);
			this.label5.TabIndex = 11;
			this.label5.Text = "DEBE";
			// 
			// dgvLibroMayor
			// 
			this.dgvLibroMayor.AllowUserToAddRows = false;
			this.dgvLibroMayor.AllowUserToDeleteRows = false;
			this.dgvLibroMayor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvLibroMayor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7});
			this.dgvLibroMayor.Location = new System.Drawing.Point(21, 165);
			this.dgvLibroMayor.MultiSelect = false;
			this.dgvLibroMayor.Name = "dgvLibroMayor";
			this.dgvLibroMayor.ReadOnly = true;
			this.dgvLibroMayor.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dgvLibroMayor.Size = new System.Drawing.Size(945, 367);
			this.dgvLibroMayor.TabIndex = 10;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "N.pda";
			this.Column1.Name = "Column1";
			this.Column1.ReadOnly = true;
			this.Column1.Width = 50;
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Descripcion";
			this.Column2.Name = "Column2";
			this.Column2.ReadOnly = true;
			this.Column2.Width = 200;
			// 
			// Column3
			// 
			this.Column3.HeaderText = "Monto";
			this.Column3.Name = "Column3";
			this.Column3.ReadOnly = true;
			// 
			// Column4
			// 
			this.Column4.HeaderText = "Cuenta";
			this.Column4.Name = "Column4";
			this.Column4.ReadOnly = true;
			this.Column4.Width = 200;
			// 
			// Column5
			// 
			this.Column5.HeaderText = "N.pda";
			this.Column5.Name = "Column5";
			this.Column5.ReadOnly = true;
			this.Column5.Width = 50;
			// 
			// Column6
			// 
			this.Column6.HeaderText = "Descripcion";
			this.Column6.Name = "Column6";
			this.Column6.ReadOnly = true;
			this.Column6.Width = 200;
			// 
			// Column7
			// 
			this.Column7.HeaderText = "Monto";
			this.Column7.Name = "Column7";
			this.Column7.ReadOnly = true;
			// 
			// btnActualizarLibro
			// 
			this.btnActualizarLibro.Location = new System.Drawing.Point(617, 72);
			this.btnActualizarLibro.Name = "btnActualizarLibro";
			this.btnActualizarLibro.Size = new System.Drawing.Size(113, 32);
			this.btnActualizarLibro.TabIndex = 0;
			this.btnActualizarLibro.Text = "Actualizar Libro";
			this.btnActualizarLibro.UseVisualStyleBackColor = true;
			this.btnActualizarLibro.Click += new System.EventHandler(this.btnActualizarLibro_Click);
			// 
			// FechaHasta
			// 
			this.FechaHasta.CustomFormat = "dd/MM/yyy";
			this.FechaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.FechaHasta.Location = new System.Drawing.Point(317, 84);
			this.FechaHasta.Name = "FechaHasta";
			this.FechaHasta.Size = new System.Drawing.Size(84, 23);
			this.FechaHasta.TabIndex = 9;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(202, 68);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(39, 15);
			this.label4.TabIndex = 8;
			this.label4.Text = "Desde";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(331, 64);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(41, 17);
			this.label3.TabIndex = 7;
			this.label3.Text = "Hasta";
			// 
			// FechaDesde
			// 
			this.FechaDesde.CustomFormat = "dd/MM/yyy";
			this.FechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.FechaDesde.Location = new System.Drawing.Point(181, 84);
			this.FechaDesde.Name = "FechaDesde";
			this.FechaDesde.Size = new System.Drawing.Size(84, 23);
			this.FechaDesde.TabIndex = 6;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(235, 33);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(106, 17);
			this.label2.TabIndex = 1;
			this.label2.Text = "Filtros De Fechas";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(430, 3);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(124, 30);
			this.label1.TabIndex = 0;
			this.label1.Text = "Libro Mayor";
			// 
			// LibroMayor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1043, 569);
			this.Controls.Add(this.panel1);
			this.Font = new System.Drawing.Font("Segoe UI", 8.5F);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "LibroMayor";
			this.Text = "Libro Mayor";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvLibroMayor)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker FechaHasta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker FechaDesde;
        private System.Windows.Forms.DataGridView dgvLibroMayor;
        private System.Windows.Forms.Button btnActualizarLibro;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
    }
}