﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class BalanceDeSaldos : Form
	{
		BindingList<Account> Accounts;
		BindingList<Accounting_Party> AccountingParty;


		public BalanceDeSaldos()
		{
			Accounts = new BindingList<Account>();
			AccountingParty = new BindingList<Accounting_Party>();
            var m = Program.getManager();
            //Cargar Datos de las Cuentas a lista Accounts
            Accounts = m.GetNomenclatureManager().GetAccounts();
            //Cargar Datos de las Partidas a lista AccountingParty
            AccountingParty = m.GetDepartureManager().GetDepartures();

            InitializeComponent();

		}

		private void btnActualizarLibro_Click(object sender, EventArgs e)
		{
			decimal TotalDebe = 0;
			decimal TotalHaber = 0;
			decimal TotalDeudor = 0;
			decimal TotalAcreedor = 0;
			int FilaCuenta = 0;
			bool ExistsTotal = false; //para ver si debo mostrar el total
			dgvBalanceDeSaldos.Rows.Clear();

			foreach (var Account in Accounts)
			{
				decimal SumaDebe = 0;   //suma del debe de cada cuenta
				decimal SumaHaber = 0;  //suma de haber de cada cuenta
				decimal SaldoAcreedor = 0;  //saldo acreedor de cada cuenta
				decimal SaldoDeudor = 0;    //saldo deudor de cada cuenta
				bool ExistsMovement = false; //para saber si hay movimientos en la cuenta

				foreach (var Departure in AccountingParty)
				{
					
					if (DateTime.Compare(FechaDesde.Value,Departure.Date)<0 && DateTime.Compare(Departure.Date,FechaHasta.Value)<0)
					{
						foreach (var Movement in Departure.Debe)
                        {
                            if (Movement.Account.Code == Account.Code)
                            {
                                SumaDebe += Movement.Debe;
                                ExistsMovement = true;
                            }
                        }

                        foreach (var Movement in Departure.Haber)
                        {
                            if (Movement.Account.Code == Account.Code)
                            {
								SumaHaber += Movement.Haber;
                                ExistsMovement = true;
                            }
                        }
					}
				}

				if (SumaDebe > SumaHaber)
				{
					SaldoDeudor = (SumaDebe - SumaHaber);
                    ExistsTotal = true;
				}

				if (SumaDebe < SumaHaber)
				{
					SaldoAcreedor = (SumaHaber - SumaDebe);
                    ExistsTotal = true;
				}

				TotalAcreedor += SaldoAcreedor;
				TotalDeudor += SaldoDeudor;
				TotalDebe += SumaDebe;
				TotalHaber += SumaHaber;

				if (ExistsMovement) //hay movimientos en la cuenta?
				{
					dgvBalanceDeSaldos.Rows.Add();

					dgvBalanceDeSaldos.Rows[FilaCuenta].Cells[0].Value = Account.Code;
					dgvBalanceDeSaldos.Rows[FilaCuenta].Cells[1].Value = Account.Description;
					if (SumaDebe != 0)
					{
						dgvBalanceDeSaldos.Rows[FilaCuenta].Cells[2].Value = SumaDebe;
					}
					if (SumaHaber != 0)
					{
						dgvBalanceDeSaldos.Rows[FilaCuenta].Cells[3].Value = SumaHaber;
					}
					if (SaldoDeudor != 0)
					{
						dgvBalanceDeSaldos.Rows[FilaCuenta].Cells[4].Value = SaldoDeudor;
					}
					if (SaldoAcreedor != 0)
					{
						dgvBalanceDeSaldos.Rows[FilaCuenta].Cells[5].Value = SaldoAcreedor;
					}

					FilaCuenta++;
				}
			}
            FilaCuenta++;
            dgvBalanceDeSaldos.Rows.Add();
			if (ExistsTotal == true)
			{
				dgvBalanceDeSaldos.Rows.Add();
				dgvBalanceDeSaldos.Rows[FilaCuenta].Cells[1].Value = "TOTALES";
				dgvBalanceDeSaldos.Rows[FilaCuenta].Cells[2].Value = TotalDebe;
				dgvBalanceDeSaldos.Rows[FilaCuenta].Cells[3].Value = TotalHaber;
				dgvBalanceDeSaldos.Rows[FilaCuenta].Cells[4].Value = TotalDeudor;
				dgvBalanceDeSaldos.Rows[FilaCuenta].Cells[5].Value = TotalAcreedor;
			}
		}
	}
}
