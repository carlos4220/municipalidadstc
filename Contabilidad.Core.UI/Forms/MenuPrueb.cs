﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class MenuPrincipal : Form
	{
		private LibroDiario ld;
		private BalanceDeSaldos bds;
		private BalanceGeneral bg;
		private LibroMayor lm;
		private Nomenclatura nm;
		private PartidasContables pac;
		private PolizasContables poc;
		private UserMenu um;

		public MenuPrincipal()
		{
			InitializeComponent();
		}

		private void pictureBox2_Click(object sender, EventArgs e)
		{
			if (LateralMenu.Width == 275)
			{
				LateralMenu.Width = 125;
			}
			else
			{
				LateralMenu.Width = 275;
			}
		}

		private void buttonLibroDiario_Click(object sender, EventArgs e)
		{
			if (ld != null)
			{
				if (ld.IsDisposed)
				{
					ld = new LibroDiario();
					ld.Show();
				}
				ld.BringToFront();
				return;
			}
			ld = new LibroDiario();
			ld.Show();
		}

		private void buttonBalancesSaldos_Click(object sender, EventArgs e)
		{
			if (bds != null)
			{
				if (bds.IsDisposed)
				{
					bds = new BalanceDeSaldos();
					bds.Show();
				}
				bds.BringToFront();
				return;
			}
			bds = new BalanceDeSaldos();
			bds.Show();
		}

		private void buttonBalanceGeneral_Click(object sender, EventArgs e)
		{
			if (bg != null)
			{
				if (bg.IsDisposed)
				{
					bg = new BalanceGeneral();
					bg.Show();
				}
				bg.BringToFront();
				return;
			}
			bg = new BalanceGeneral();
			bg.Show();
		}

		private void buttonLibroMayor_Click(object sender, EventArgs e)
		{
			if (lm != null)
			{
				if (lm.IsDisposed)
				{
					lm = new LibroMayor();
					lm.Show();
				}
				lm.BringToFront();
				return;
			}
			lm = new LibroMayor();
			lm.Show();
		}

		private void ButtonNomenclatura_Click(object sender, EventArgs e)
		{
			if (nm != null)
			{
				if (nm.IsDisposed)
				{
					nm = new Nomenclatura();
					nm.Show();
				}
				nm.BringToFront();
				return;
			}
			nm = new Nomenclatura();
			nm.Show();
		}

		private void buttonpartidas_Click(object sender, EventArgs e)
		{
			if (pac != null)
			{
				if (pac.IsDisposed)
				{
					pac = new PartidasContables();
					pac.Show();
				}
				pac.BringToFront();
				return;
			}
			pac = new PartidasContables();
			pac.Show();
		}

		private void buttonPolizas_Click(object sender, EventArgs e)
		{
			if (poc != null)
			{
				if (poc.IsDisposed)
				{
					poc = new PolizasContables();
					poc.Show();
				}
				poc.BringToFront();
				return;
			}
			poc = new PolizasContables();
			poc.Show();
		}

		private void buttonSettings_Click(object sender, EventArgs e)
		{
			if (um != null)
			{
				if (um.IsDisposed)
				{
					um = new UserMenu();
					um.Show();
				}
				um.BringToFront();
				return;
			}
			um = new UserMenu();
			um.Show();
		}

		private async void MenuPrincipal_Shown(object sender, EventArgs e)
		{
			await Init();
		}

		private void MenuPrincipal_FormClosed(object sender, FormClosedEventArgs e)
		{
			Dispo();
		}

		private async Task Init()
		{
			await Task.Delay(250);

			var m = Program.getManager();
			await Task.Run(() => m.Init());
			var um = m.GetUserManager();

			if (um.GetUsers().Count == 0)
				new RegisterUser(null).ShowDialog();
			else
				new VentanaLogin().ShowDialog();

			if (!um.IsLoggedIn())
				Close();

		}

		private void Dispo()
		{
			var m = Program.getManager();
			m.Dispose();
		}

	}
}

