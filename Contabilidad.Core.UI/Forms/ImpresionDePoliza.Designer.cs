﻿namespace Contabilidad.Core.UI.Forms
{
    partial class ImpresionDePoliza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImpresionDePoliza));
			this.label1 = new System.Windows.Forms.Label();
			this.TextTipoPoliza = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.TextFecha = new System.Windows.Forms.Label();
			this.dgvMovements = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label3 = new System.Windows.Forms.Label();
			this.txtDescription = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtSumaParcial = new System.Windows.Forms.TextBox();
			this.txtSumaDebe = new System.Windows.Forms.TextBox();
			this.txtSumaHaber = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.txtHechoPor = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.txtAutorizadoPor = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.txtNumero = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.dgvMovements)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(34, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(54, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "Poliza de";
			// 
			// TextTipoPoliza
			// 
			this.TextTipoPoliza.AutoSize = true;
			this.TextTipoPoliza.Location = new System.Drawing.Point(90, 13);
			this.TextTipoPoliza.Name = "TextTipoPoliza";
			this.TextTipoPoliza.Size = new System.Drawing.Size(0, 15);
			this.TextTipoPoliza.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(586, 13);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(41, 15);
			this.label2.TabIndex = 2;
			this.label2.Text = "Fecha:";
			// 
			// TextFecha
			// 
			this.TextFecha.AutoSize = true;
			this.TextFecha.Location = new System.Drawing.Point(632, 13);
			this.TextFecha.Name = "TextFecha";
			this.TextFecha.Size = new System.Drawing.Size(0, 15);
			this.TextFecha.TabIndex = 3;
			// 
			// dgvMovements
			// 
			this.dgvMovements.AllowUserToAddRows = false;
			this.dgvMovements.AllowUserToDeleteRows = false;
			this.dgvMovements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvMovements.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
			this.dgvMovements.Location = new System.Drawing.Point(12, 64);
			this.dgvMovements.MultiSelect = false;
			this.dgvMovements.Name = "dgvMovements";
			this.dgvMovements.ReadOnly = true;
			this.dgvMovements.Size = new System.Drawing.Size(943, 268);
			this.dgvMovements.TabIndex = 4;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "Cuentas";
			this.Column1.Name = "Column1";
			this.Column1.ReadOnly = true;
			this.Column1.Width = 150;
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Descripcion";
			this.Column2.Name = "Column2";
			this.Column2.ReadOnly = true;
			this.Column2.Width = 300;
			// 
			// Column3
			// 
			this.Column3.HeaderText = "Parcial";
			this.Column3.Name = "Column3";
			this.Column3.ReadOnly = true;
			this.Column3.Width = 150;
			// 
			// Column4
			// 
			this.Column4.HeaderText = "Debe";
			this.Column4.Name = "Column4";
			this.Column4.ReadOnly = true;
			this.Column4.Width = 150;
			// 
			// Column5
			// 
			this.Column5.HeaderText = "Haber";
			this.Column5.Name = "Column5";
			this.Column5.ReadOnly = true;
			this.Column5.Width = 150;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(21, 351);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(62, 15);
			this.label3.TabIndex = 5;
			this.label3.Text = "Concepto:";
			// 
			// txtDescription
			// 
			this.txtDescription.Location = new System.Drawing.Point(89, 348);
			this.txtDescription.Multiline = true;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.ReadOnly = true;
			this.txtDescription.Size = new System.Drawing.Size(260, 46);
			this.txtDescription.TabIndex = 6;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(461, 351);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(45, 15);
			this.label4.TabIndex = 7;
			this.label4.Text = "Sumas:";
			// 
			// txtSumaParcial
			// 
			this.txtSumaParcial.Location = new System.Drawing.Point(512, 348);
			this.txtSumaParcial.Name = "txtSumaParcial";
			this.txtSumaParcial.ReadOnly = true;
			this.txtSumaParcial.Size = new System.Drawing.Size(138, 23);
			this.txtSumaParcial.TabIndex = 8;
			// 
			// txtSumaDebe
			// 
			this.txtSumaDebe.Location = new System.Drawing.Point(656, 348);
			this.txtSumaDebe.Name = "txtSumaDebe";
			this.txtSumaDebe.ReadOnly = true;
			this.txtSumaDebe.Size = new System.Drawing.Size(147, 23);
			this.txtSumaDebe.TabIndex = 9;
			// 
			// txtSumaHaber
			// 
			this.txtSumaHaber.Location = new System.Drawing.Point(809, 348);
			this.txtSumaHaber.Name = "txtSumaHaber";
			this.txtSumaHaber.ReadOnly = true;
			this.txtSumaHaber.Size = new System.Drawing.Size(145, 23);
			this.txtSumaHaber.TabIndex = 10;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(90, 444);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(63, 15);
			this.label5.TabIndex = 11;
			this.label5.Text = "Hecho por";
			// 
			// txtHechoPor
			// 
			this.txtHechoPor.Location = new System.Drawing.Point(26, 462);
			this.txtHechoPor.Name = "txtHechoPor";
			this.txtHechoPor.ReadOnly = true;
			this.txtHechoPor.Size = new System.Drawing.Size(220, 23);
			this.txtHechoPor.TabIndex = 12;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(377, 444);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(86, 15);
			this.label6.TabIndex = 13;
			this.label6.Text = "Autorizado por";
			// 
			// txtAutorizadoPor
			// 
			this.txtAutorizadoPor.Location = new System.Drawing.Point(313, 462);
			this.txtAutorizadoPor.Name = "txtAutorizadoPor";
			this.txtAutorizadoPor.ReadOnly = true;
			this.txtAutorizadoPor.Size = new System.Drawing.Size(218, 23);
			this.txtAutorizadoPor.TabIndex = 14;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(702, 444);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(60, 15);
			this.label7.TabIndex = 15;
			this.label7.Text = "Poliza No.";
			// 
			// txtNumero
			// 
			this.txtNumero.Location = new System.Drawing.Point(667, 462);
			this.txtNumero.Name = "txtNumero";
			this.txtNumero.ReadOnly = true;
			this.txtNumero.Size = new System.Drawing.Size(126, 23);
			this.txtNumero.TabIndex = 16;
			// 
			// ImpresionDePoliza
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(980, 529);
			this.Controls.Add(this.txtNumero);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.txtAutorizadoPor);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.txtHechoPor);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.txtSumaHaber);
			this.Controls.Add(this.txtSumaDebe);
			this.Controls.Add(this.txtSumaParcial);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.txtDescription);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.dgvMovements);
			this.Controls.Add(this.TextFecha);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.TextTipoPoliza);
			this.Controls.Add(this.label1);
			this.Font = new System.Drawing.Font("Segoe UI", 8.5F);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "ImpresionDePoliza";
			this.Text = "Impresion de Poliza";
			((System.ComponentModel.ISupportInitialize)(this.dgvMovements)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label TextTipoPoliza;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label TextFecha;
        private System.Windows.Forms.DataGridView dgvMovements;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSumaParcial;
        private System.Windows.Forms.TextBox txtSumaDebe;
        private System.Windows.Forms.TextBox txtSumaHaber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtHechoPor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAutorizadoPor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNumero;
    }
}