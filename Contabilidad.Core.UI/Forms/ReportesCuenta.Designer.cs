﻿namespace Contabilidad.Core.UI.Forms
{
    partial class ReportesCuenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportesCuenta));
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnActualizarLibro = new System.Windows.Forms.Button();
			this.FechaHasta = new System.Windows.Forms.DateTimePicker();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.FechaDesde = new System.Windows.Forms.DateTimePicker();
			this.dgvMovement = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.NameAccount = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvMovement)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnActualizarLibro);
			this.panel1.Controls.Add(this.FechaHasta);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.FechaDesde);
			this.panel1.Controls.Add(this.dgvMovement);
			this.panel1.Controls.Add(this.NameAccount);
			this.panel1.Location = new System.Drawing.Point(62, 21);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(838, 526);
			this.panel1.TabIndex = 0;
			// 
			// btnActualizarLibro
			// 
			this.btnActualizarLibro.Location = new System.Drawing.Point(515, 112);
			this.btnActualizarLibro.Name = "btnActualizarLibro";
			this.btnActualizarLibro.Size = new System.Drawing.Size(113, 32);
			this.btnActualizarLibro.TabIndex = 15;
			this.btnActualizarLibro.Text = "Actualizar Datos";
			this.btnActualizarLibro.UseVisualStyleBackColor = true;
			this.btnActualizarLibro.Click += new System.EventHandler(this.btnActualizarLibro_Click);
			// 
			// FechaHasta
			// 
			this.FechaHasta.CustomFormat = "dd/MM/yyy";
			this.FechaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.FechaHasta.Location = new System.Drawing.Point(277, 124);
			this.FechaHasta.Name = "FechaHasta";
			this.FechaHasta.Size = new System.Drawing.Size(84, 23);
			this.FechaHasta.TabIndex = 14;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(162, 108);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(39, 15);
			this.label4.TabIndex = 13;
			this.label4.Text = "Desde";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(291, 104);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(41, 17);
			this.label3.TabIndex = 12;
			this.label3.Text = "Hasta";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(190, 79);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(106, 17);
			this.label2.TabIndex = 11;
			this.label2.Text = "Filtros De Fechas";
			// 
			// FechaDesde
			// 
			this.FechaDesde.CustomFormat = "dd/MM/yyy";
			this.FechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.FechaDesde.Location = new System.Drawing.Point(141, 124);
			this.FechaDesde.Name = "FechaDesde";
			this.FechaDesde.Size = new System.Drawing.Size(84, 23);
			this.FechaDesde.TabIndex = 10;
			// 
			// dgvMovement
			// 
			this.dgvMovement.AllowUserToAddRows = false;
			this.dgvMovement.AllowUserToDeleteRows = false;
			this.dgvMovement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvMovement.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
			this.dgvMovement.Location = new System.Drawing.Point(25, 202);
			this.dgvMovement.MultiSelect = false;
			this.dgvMovement.Name = "dgvMovement";
			this.dgvMovement.ReadOnly = true;
			this.dgvMovement.Size = new System.Drawing.Size(783, 274);
			this.dgvMovement.TabIndex = 3;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "Pda";
			this.Column1.Name = "Column1";
			this.Column1.ReadOnly = true;
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Fecha ";
			this.Column2.Name = "Column2";
			this.Column2.ReadOnly = true;
			// 
			// Column3
			// 
			this.Column3.HeaderText = "Descripcion";
			this.Column3.Name = "Column3";
			this.Column3.ReadOnly = true;
			this.Column3.Width = 300;
			// 
			// Column4
			// 
			this.Column4.HeaderText = "Debe";
			this.Column4.Name = "Column4";
			this.Column4.ReadOnly = true;
			this.Column4.Width = 120;
			// 
			// Column5
			// 
			this.Column5.HeaderText = "Haber";
			this.Column5.Name = "Column5";
			this.Column5.ReadOnly = true;
			this.Column5.Width = 120;
			// 
			// NameAccount
			// 
			this.NameAccount.AutoSize = true;
			this.NameAccount.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.NameAccount.Location = new System.Drawing.Point(337, 10);
			this.NameAccount.Name = "NameAccount";
			this.NameAccount.Size = new System.Drawing.Size(0, 30);
			this.NameAccount.TabIndex = 2;
			// 
			// ReportesCuenta
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(912, 595);
			this.Controls.Add(this.panel1);
			this.Font = new System.Drawing.Font("Segoe UI", 8.5F);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "ReportesCuenta";
			this.Text = "Reportes de Cuentas";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvMovement)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvMovement;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.Label NameAccount;
        private System.Windows.Forms.Button btnActualizarLibro;
        private System.Windows.Forms.DateTimePicker FechaHasta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker FechaDesde;
    }
}