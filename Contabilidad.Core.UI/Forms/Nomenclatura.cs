﻿using Contabilidad.Core.Classes;
using Contabilidad.Core.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class Nomenclatura : Form
	{

		private ErrorProvider Error = new ErrorProvider();

		private NomenclatureManager nm = Program.getManager().GetNomenclatureManager();
		private Usuario user;

		private Account selectedAccount;

		public Nomenclatura()
		{
			InitializeComponent();

			user = Program.getManager().GetUserManager().GetCurrentUser();

			cmbTypeAccount.DataSource = Enum.GetValues(typeof(AccountType));
			dgvAccount.DataSource = nm.GetAccounts();
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			int type;
			int.TryParse(txtLevel1.Text, out type);

			String code = txtLevel1.Text + "." + txtLevel2.Text + "." + txtLevel3.Text + "." + txtLevel4.Text;

			var ae = nm.AddAccount((AccountType)type, code, txtAccountName.Text);
			if (ae == AccountError.EXIST)
				MessageBox.Show("Ya existe una cuenta con este codigo.");
		}

		private void cmbTypeAccount_SelectedIndexChanged(object sender, EventArgs e)
		{
			txtLevel1.Text = cmbTypeAccount.SelectedIndex.ToString();
		}

		private void btnRemove_Click(object sender, EventArgs e)
		{
			Error.Clear();
			if (user.Puesto == UserPosition.Contabilidad)
			{
				if (selectedAccount != null)
				{
					var window = MessageBox.Show("Desea borrar esta cuenta?", "Confirmar borrado", MessageBoxButtons.YesNo);
					if (window == DialogResult.No)
						return;

					nm.RemoveAccount(selectedAccount);
					txtLevel1.Text = "0";
					txtLevel2.Text = "0";
					txtLevel3.Text = "0";
					txtLevel4.Text = "0";
					txtAccountName.Clear();
					selectedAccount = null;
				}
				else
				{
					Error.SetError(btnRemove, "Cuenta no encontrada.");
				}
			}
			else
			{
				Error.SetError(btnRemove, "Su puesto no tiene acceso a eliminacion de datos.");
			}
		}

		private void btnMostraReportesCuenta_Click(object sender, EventArgs e)
		{
			Error.Clear();
			if (selectedAccount != null)
			{
				ReportesCuenta ReporteDeCuenta = new ReportesCuenta(selectedAccount);
				ReporteDeCuenta.ActualizarDgv();
				ReporteDeCuenta.Show();
			}
			else
			{
				Error.SetError(btnMostraReportesCuenta, "Cuenta no encontrada.");
			}
		}

		private void ValidateTxt(object sender, EventArgs e)
		{
			int b;
			if (!int.TryParse(((TextBox)sender).Text, out b))
				((TextBox)sender).Text = "0";
		}

		private void dgvAccount_SelectionChanged(object sender, EventArgs e)
		{
			DataGridViewRow row = dgvAccount.CurrentRow;
			selectedAccount = (Account)row.DataBoundItem;
			updateTextData(selectedAccount);
		}

		private void updateTextData(Account acc)
		{
			String[] code = acc.Code.Split('.');
			txtLevel1.Text = code[0];
			txtLevel2.Text = code[1];
			txtLevel3.Text = code[2];
			txtLevel4.Text = code[3];
			txtAccountName.Text = acc.Description;
		}
	}
}
