﻿namespace Contabilidad.Core.UI.Forms
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuPrincipal));
			this.LateralMenu = new System.Windows.Forms.Panel();
			this.pictureBox6 = new System.Windows.Forms.PictureBox();
			this.pictureBox5 = new System.Windows.Forms.PictureBox();
			this.pictureBox4 = new System.Windows.Forms.PictureBox();
			this.buttonSettings = new System.Windows.Forms.Panel();
			this.pictureBox3 = new System.Windows.Forms.PictureBox();
			this.label4 = new System.Windows.Forms.Label();
			this.buttonPolizas = new System.Windows.Forms.Button();
			this.buttonpartidas = new System.Windows.Forms.Button();
			this.ButtonNomenclatura = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.Title = new System.Windows.Forms.Panel();
			this.label3 = new System.Windows.Forms.Label();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.buttonLibroDiario = new System.Windows.Forms.Panel();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.pictureBox7 = new System.Windows.Forms.PictureBox();
			this.buttonBalancesSaldos = new System.Windows.Forms.Panel();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.pictureBox8 = new System.Windows.Forms.PictureBox();
			this.buttonBalanceGeneral = new System.Windows.Forms.Panel();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.pictureBox9 = new System.Windows.Forms.PictureBox();
			this.buttonLibroMayor = new System.Windows.Forms.Panel();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.pictureBox10 = new System.Windows.Forms.PictureBox();
			this.LateralMenu.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
			this.buttonSettings.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.Title.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			this.buttonLibroDiario.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
			this.buttonBalancesSaldos.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
			this.buttonBalanceGeneral.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
			this.buttonLibroMayor.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
			this.SuspendLayout();
			// 
			// LateralMenu
			// 
			this.LateralMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(126)))), ((int)(((byte)(139)))));
			this.LateralMenu.Controls.Add(this.pictureBox6);
			this.LateralMenu.Controls.Add(this.pictureBox5);
			this.LateralMenu.Controls.Add(this.pictureBox4);
			this.LateralMenu.Controls.Add(this.buttonSettings);
			this.LateralMenu.Controls.Add(this.buttonPolizas);
			this.LateralMenu.Controls.Add(this.buttonpartidas);
			this.LateralMenu.Controls.Add(this.ButtonNomenclatura);
			this.LateralMenu.Controls.Add(this.label2);
			this.LateralMenu.Controls.Add(this.label1);
			this.LateralMenu.Controls.Add(this.pictureBox1);
			this.LateralMenu.Dock = System.Windows.Forms.DockStyle.Left;
			this.LateralMenu.Location = new System.Drawing.Point(0, 0);
			this.LateralMenu.Name = "LateralMenu";
			this.LateralMenu.Size = new System.Drawing.Size(275, 650);
			this.LateralMenu.TabIndex = 0;
			// 
			// pictureBox6
			// 
			this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox6.Cursor = System.Windows.Forms.Cursors.Hand;
			this.pictureBox6.Image = global::Contabilidad.Core.UI.Properties.Resources.polizaicon;
			this.pictureBox6.Location = new System.Drawing.Point(12, 375);
			this.pictureBox6.Name = "pictureBox6";
			this.pictureBox6.Size = new System.Drawing.Size(50, 50);
			this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox6.TabIndex = 7;
			this.pictureBox6.TabStop = false;
			this.pictureBox6.Click += new System.EventHandler(this.buttonPolizas_Click);
			// 
			// pictureBox5
			// 
			this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox5.Cursor = System.Windows.Forms.Cursors.Hand;
			this.pictureBox5.Image = global::Contabilidad.Core.UI.Properties.Resources.partidasicon;
			this.pictureBox5.Location = new System.Drawing.Point(12, 300);
			this.pictureBox5.Name = "pictureBox5";
			this.pictureBox5.Size = new System.Drawing.Size(50, 50);
			this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox5.TabIndex = 6;
			this.pictureBox5.TabStop = false;
			this.pictureBox5.Click += new System.EventHandler(this.buttonpartidas_Click);
			// 
			// pictureBox4
			// 
			this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
			this.pictureBox4.Image = global::Contabilidad.Core.UI.Properties.Resources.nomenicon;
			this.pictureBox4.Location = new System.Drawing.Point(12, 225);
			this.pictureBox4.Name = "pictureBox4";
			this.pictureBox4.Size = new System.Drawing.Size(50, 50);
			this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox4.TabIndex = 2;
			this.pictureBox4.TabStop = false;
			this.pictureBox4.Click += new System.EventHandler(this.ButtonNomenclatura_Click);
			// 
			// buttonSettings
			// 
			this.buttonSettings.Controls.Add(this.pictureBox3);
			this.buttonSettings.Controls.Add(this.label4);
			this.buttonSettings.Cursor = System.Windows.Forms.Cursors.Hand;
			this.buttonSettings.Location = new System.Drawing.Point(0, 600);
			this.buttonSettings.Name = "buttonSettings";
			this.buttonSettings.Size = new System.Drawing.Size(275, 50);
			this.buttonSettings.TabIndex = 2;
			this.buttonSettings.Click += new System.EventHandler(this.buttonSettings_Click);
			// 
			// pictureBox3
			// 
			this.pictureBox3.Image = global::Contabilidad.Core.UI.Properties.Resources.settings_white;
			this.pictureBox3.Location = new System.Drawing.Point(51, 0);
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.Size = new System.Drawing.Size(50, 50);
			this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox3.TabIndex = 2;
			this.pictureBox3.TabStop = false;
			this.pictureBox3.Click += new System.EventHandler(this.buttonSettings_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.Color.White;
			this.label4.Location = new System.Drawing.Point(97, 11);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(90, 25);
			this.label4.TabIndex = 0;
			this.label4.Text = "Settings";
			this.label4.Click += new System.EventHandler(this.buttonSettings_Click);
			// 
			// buttonPolizas
			// 
			this.buttonPolizas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(126)))), ((int)(((byte)(139)))));
			this.buttonPolizas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.buttonPolizas.Cursor = System.Windows.Forms.Cursors.Hand;
			this.buttonPolizas.FlatAppearance.BorderSize = 0;
			this.buttonPolizas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonPolizas.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonPolizas.ForeColor = System.Drawing.Color.White;
			this.buttonPolizas.Location = new System.Drawing.Point(0, 375);
			this.buttonPolizas.Name = "buttonPolizas";
			this.buttonPolizas.Size = new System.Drawing.Size(275, 50);
			this.buttonPolizas.TabIndex = 5;
			this.buttonPolizas.Text = "Pólizas";
			this.buttonPolizas.UseVisualStyleBackColor = false;
			this.buttonPolizas.Click += new System.EventHandler(this.buttonPolizas_Click);
			// 
			// buttonpartidas
			// 
			this.buttonpartidas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(126)))), ((int)(((byte)(139)))));
			this.buttonpartidas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.buttonpartidas.Cursor = System.Windows.Forms.Cursors.Hand;
			this.buttonpartidas.FlatAppearance.BorderSize = 0;
			this.buttonpartidas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonpartidas.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonpartidas.ForeColor = System.Drawing.Color.White;
			this.buttonpartidas.Location = new System.Drawing.Point(0, 300);
			this.buttonpartidas.Name = "buttonpartidas";
			this.buttonpartidas.Size = new System.Drawing.Size(275, 50);
			this.buttonpartidas.TabIndex = 4;
			this.buttonpartidas.Text = "Partidas";
			this.buttonpartidas.UseVisualStyleBackColor = false;
			this.buttonpartidas.Click += new System.EventHandler(this.buttonpartidas_Click);
			// 
			// ButtonNomenclatura
			// 
			this.ButtonNomenclatura.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(126)))), ((int)(((byte)(139)))));
			this.ButtonNomenclatura.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.ButtonNomenclatura.Cursor = System.Windows.Forms.Cursors.Hand;
			this.ButtonNomenclatura.FlatAppearance.BorderSize = 0;
			this.ButtonNomenclatura.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.ButtonNomenclatura.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ButtonNomenclatura.ForeColor = System.Drawing.Color.White;
			this.ButtonNomenclatura.Location = new System.Drawing.Point(0, 225);
			this.ButtonNomenclatura.Name = "ButtonNomenclatura";
			this.ButtonNomenclatura.Size = new System.Drawing.Size(275, 50);
			this.ButtonNomenclatura.TabIndex = 2;
			this.ButtonNomenclatura.Text = "Nomenclatura";
			this.ButtonNomenclatura.UseVisualStyleBackColor = false;
			this.ButtonNomenclatura.Click += new System.EventHandler(this.ButtonNomenclatura_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.Color.White;
			this.label2.Location = new System.Drawing.Point(128, 69);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(143, 33);
			this.label2.TabIndex = 3;
			this.label2.Text = "Accouting";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.White;
			this.label1.Location = new System.Drawing.Point(174, 33);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(87, 33);
			this.label1.TabIndex = 2;
			this.label1.Text = "Menu";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::Contabilidad.Core.UI.Properties.Resources.log;
			this.pictureBox1.Location = new System.Drawing.Point(12, 12);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(100, 100);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 2;
			this.pictureBox1.TabStop = false;
			// 
			// Title
			// 
			this.Title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(167)))));
			this.Title.Controls.Add(this.label3);
			this.Title.Controls.Add(this.pictureBox2);
			this.Title.Dock = System.Windows.Forms.DockStyle.Top;
			this.Title.Location = new System.Drawing.Point(275, 0);
			this.Title.Name = "Title";
			this.Title.Size = new System.Drawing.Size(1025, 120);
			this.Title.TabIndex = 1;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.Color.White;
			this.label3.Location = new System.Drawing.Point(117, 33);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(408, 55);
			this.label3.TabIndex = 4;
			this.label3.Text = "System Accouting";
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = global::Contabilidad.Core.UI.Properties.Resources.menu;
			this.pictureBox2.Location = new System.Drawing.Point(12, 12);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(90, 90);
			this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox2.TabIndex = 2;
			this.pictureBox2.TabStop = false;
			this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
			// 
			// buttonLibroDiario
			// 
			this.buttonLibroDiario.Controls.Add(this.label6);
			this.buttonLibroDiario.Controls.Add(this.label5);
			this.buttonLibroDiario.Controls.Add(this.pictureBox7);
			this.buttonLibroDiario.Cursor = System.Windows.Forms.Cursors.Hand;
			this.buttonLibroDiario.Location = new System.Drawing.Point(313, 155);
			this.buttonLibroDiario.Name = "buttonLibroDiario";
			this.buttonLibroDiario.Size = new System.Drawing.Size(250, 120);
			this.buttonLibroDiario.TabIndex = 2;
			this.buttonLibroDiario.Click += new System.EventHandler(this.buttonLibroDiario_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(126)))), ((int)(((byte)(139)))));
			this.label6.Location = new System.Drawing.Point(150, 60);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(92, 33);
			this.label6.TabIndex = 9;
			this.label6.Text = "Diario";
			this.label6.Click += new System.EventHandler(this.buttonLibroDiario_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(126)))), ((int)(((byte)(139)))));
			this.label5.Location = new System.Drawing.Point(163, 24);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(80, 33);
			this.label5.TabIndex = 8;
			this.label5.Text = "Libro";
			this.label5.Click += new System.EventHandler(this.buttonLibroDiario_Click);
			// 
			// pictureBox7
			// 
			this.pictureBox7.Image = global::Contabilidad.Core.UI.Properties.Resources.diaryicon;
			this.pictureBox7.Location = new System.Drawing.Point(10, 10);
			this.pictureBox7.Name = "pictureBox7";
			this.pictureBox7.Size = new System.Drawing.Size(100, 100);
			this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox7.TabIndex = 0;
			this.pictureBox7.TabStop = false;
			this.pictureBox7.Click += new System.EventHandler(this.buttonLibroDiario_Click);
			// 
			// buttonBalancesSaldos
			// 
			this.buttonBalancesSaldos.Controls.Add(this.label7);
			this.buttonBalancesSaldos.Controls.Add(this.label8);
			this.buttonBalancesSaldos.Controls.Add(this.pictureBox8);
			this.buttonBalancesSaldos.Cursor = System.Windows.Forms.Cursors.Hand;
			this.buttonBalancesSaldos.Location = new System.Drawing.Point(402, 305);
			this.buttonBalancesSaldos.Name = "buttonBalancesSaldos";
			this.buttonBalancesSaldos.Size = new System.Drawing.Size(250, 120);
			this.buttonBalancesSaldos.TabIndex = 10;
			this.buttonBalancesSaldos.Click += new System.EventHandler(this.buttonBalancesSaldos_Click);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(126)))), ((int)(((byte)(139)))));
			this.label7.Location = new System.Drawing.Point(112, 60);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(144, 33);
			this.label7.TabIndex = 9;
			this.label7.Text = "de Saldos";
			this.label7.Click += new System.EventHandler(this.buttonBalancesSaldos_Click);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(126)))), ((int)(((byte)(139)))));
			this.label8.Location = new System.Drawing.Point(138, 24);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(120, 33);
			this.label8.TabIndex = 8;
			this.label8.Text = "Balance";
			this.label8.Click += new System.EventHandler(this.buttonBalancesSaldos_Click);
			// 
			// pictureBox8
			// 
			this.pictureBox8.Image = global::Contabilidad.Core.UI.Properties.Resources.balancesumassaldosicon;
			this.pictureBox8.Location = new System.Drawing.Point(10, 10);
			this.pictureBox8.Name = "pictureBox8";
			this.pictureBox8.Size = new System.Drawing.Size(100, 100);
			this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox8.TabIndex = 0;
			this.pictureBox8.TabStop = false;
			this.pictureBox8.Click += new System.EventHandler(this.buttonBalancesSaldos_Click);
			// 
			// buttonBalanceGeneral
			// 
			this.buttonBalanceGeneral.Controls.Add(this.label9);
			this.buttonBalanceGeneral.Controls.Add(this.label10);
			this.buttonBalanceGeneral.Controls.Add(this.pictureBox9);
			this.buttonBalanceGeneral.Cursor = System.Windows.Forms.Cursors.Hand;
			this.buttonBalanceGeneral.Location = new System.Drawing.Point(561, 449);
			this.buttonBalanceGeneral.Name = "buttonBalanceGeneral";
			this.buttonBalanceGeneral.Size = new System.Drawing.Size(250, 120);
			this.buttonBalanceGeneral.TabIndex = 11;
			this.buttonBalanceGeneral.Click += new System.EventHandler(this.buttonBalanceGeneral_Click);
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(126)))), ((int)(((byte)(139)))));
			this.label9.Location = new System.Drawing.Point(139, 60);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(119, 33);
			this.label9.TabIndex = 9;
			this.label9.Text = "General";
			this.label9.Click += new System.EventHandler(this.buttonBalanceGeneral_Click);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(126)))), ((int)(((byte)(139)))));
			this.label10.Location = new System.Drawing.Point(138, 24);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(120, 33);
			this.label10.TabIndex = 8;
			this.label10.Text = "Balance";
			this.label10.Click += new System.EventHandler(this.buttonBalanceGeneral_Click);
			// 
			// pictureBox9
			// 
			this.pictureBox9.Image = global::Contabilidad.Core.UI.Properties.Resources.balances;
			this.pictureBox9.Location = new System.Drawing.Point(10, 10);
			this.pictureBox9.Name = "pictureBox9";
			this.pictureBox9.Size = new System.Drawing.Size(100, 100);
			this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox9.TabIndex = 0;
			this.pictureBox9.TabStop = false;
			this.pictureBox9.Click += new System.EventHandler(this.buttonBalanceGeneral_Click);
			// 
			// buttonLibroMayor
			// 
			this.buttonLibroMayor.Controls.Add(this.label11);
			this.buttonLibroMayor.Controls.Add(this.label12);
			this.buttonLibroMayor.Controls.Add(this.pictureBox10);
			this.buttonLibroMayor.Cursor = System.Windows.Forms.Cursors.Hand;
			this.buttonLibroMayor.Location = new System.Drawing.Point(875, 509);
			this.buttonLibroMayor.Name = "buttonLibroMayor";
			this.buttonLibroMayor.Size = new System.Drawing.Size(250, 120);
			this.buttonLibroMayor.TabIndex = 10;
			this.buttonLibroMayor.Click += new System.EventHandler(this.buttonLibroMayor_Click);
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(126)))), ((int)(((byte)(139)))));
			this.label11.Location = new System.Drawing.Point(147, 57);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(96, 33);
			this.label11.TabIndex = 9;
			this.label11.Text = "Mayor";
			this.label11.Click += new System.EventHandler(this.buttonLibroMayor_Click);
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(126)))), ((int)(((byte)(139)))));
			this.label12.Location = new System.Drawing.Point(163, 24);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(80, 33);
			this.label12.TabIndex = 8;
			this.label12.Text = "Libro";
			this.label12.Click += new System.EventHandler(this.buttonLibroMayor_Click);
			// 
			// pictureBox10
			// 
			this.pictureBox10.Image = global::Contabilidad.Core.UI.Properties.Resources.mayoricon;
			this.pictureBox10.Location = new System.Drawing.Point(10, 10);
			this.pictureBox10.Name = "pictureBox10";
			this.pictureBox10.Size = new System.Drawing.Size(100, 100);
			this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox10.TabIndex = 0;
			this.pictureBox10.TabStop = false;
			this.pictureBox10.Click += new System.EventHandler(this.buttonLibroMayor_Click);
			// 
			// MenuPrincipal
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1300, 650);
			this.Controls.Add(this.buttonLibroMayor);
			this.Controls.Add(this.buttonBalanceGeneral);
			this.Controls.Add(this.buttonBalancesSaldos);
			this.Controls.Add(this.buttonLibroDiario);
			this.Controls.Add(this.Title);
			this.Controls.Add(this.LateralMenu);
			this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "MenuPrincipal";
			this.Text = "Contabilidad";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MenuPrincipal_FormClosed);
			this.Shown += new System.EventHandler(this.MenuPrincipal_Shown);
			this.LateralMenu.ResumeLayout(false);
			this.LateralMenu.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
			this.buttonSettings.ResumeLayout(false);
			this.buttonSettings.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.Title.ResumeLayout(false);
			this.Title.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			this.buttonLibroDiario.ResumeLayout(false);
			this.buttonLibroDiario.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
			this.buttonBalancesSaldos.ResumeLayout(false);
			this.buttonBalancesSaldos.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
			this.buttonBalanceGeneral.ResumeLayout(false);
			this.buttonBalanceGeneral.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
			this.buttonLibroMayor.ResumeLayout(false);
			this.buttonLibroMayor.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LateralMenu;
        private System.Windows.Forms.Panel Title;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonpartidas;
        private System.Windows.Forms.Button ButtonNomenclatura;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel buttonSettings;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonPolizas;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel buttonLibroDiario;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel buttonBalancesSaldos;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel buttonBalanceGeneral;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Panel buttonLibroMayor;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox10;
    }
}