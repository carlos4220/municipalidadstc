﻿namespace Contabilidad.Core.UI.Forms
{
	partial class UserMenu
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.Panel panel1;
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserMenu));
			this.buttonDelete = new System.Windows.Forms.Button();
			this.buttonRegister = new System.Windows.Forms.Button();
			this.dgvUsers = new System.Windows.Forms.DataGridView();
			this.userDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.puestoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.usuarioBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.buttonModify = new System.Windows.Forms.Button();
			panel1 = new System.Windows.Forms.Panel();
			panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvUsers)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.usuarioBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			panel1.Controls.Add(this.buttonModify);
			panel1.Controls.Add(this.buttonDelete);
			panel1.Controls.Add(this.buttonRegister);
			panel1.Location = new System.Drawing.Point(463, 13);
			panel1.Name = "panel1";
			panel1.Size = new System.Drawing.Size(158, 425);
			panel1.TabIndex = 1;
			// 
			// buttonDelete
			// 
			this.buttonDelete.Location = new System.Drawing.Point(5, 71);
			this.buttonDelete.Margin = new System.Windows.Forms.Padding(5);
			this.buttonDelete.Name = "buttonDelete";
			this.buttonDelete.Size = new System.Drawing.Size(148, 23);
			this.buttonDelete.TabIndex = 1;
			this.buttonDelete.Text = "Eliminar Usuario";
			this.buttonDelete.UseVisualStyleBackColor = true;
			this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
			// 
			// buttonRegister
			// 
			this.buttonRegister.Location = new System.Drawing.Point(5, 5);
			this.buttonRegister.Margin = new System.Windows.Forms.Padding(5);
			this.buttonRegister.Name = "buttonRegister";
			this.buttonRegister.Size = new System.Drawing.Size(148, 23);
			this.buttonRegister.TabIndex = 0;
			this.buttonRegister.Text = "Registrar Usuario";
			this.buttonRegister.UseVisualStyleBackColor = true;
			this.buttonRegister.Click += new System.EventHandler(this.buttonRegister_Click);
			// 
			// dgvUsers
			// 
			this.dgvUsers.AllowUserToAddRows = false;
			this.dgvUsers.AllowUserToDeleteRows = false;
			this.dgvUsers.AutoGenerateColumns = false;
			this.dgvUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvUsers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.userDataGridViewTextBoxColumn,
            this.puestoDataGridViewTextBoxColumn});
			this.dgvUsers.DataSource = this.usuarioBindingSource;
			this.dgvUsers.Location = new System.Drawing.Point(12, 12);
			this.dgvUsers.MultiSelect = false;
			this.dgvUsers.Name = "dgvUsers";
			this.dgvUsers.ReadOnly = true;
			this.dgvUsers.Size = new System.Drawing.Size(444, 426);
			this.dgvUsers.TabIndex = 0;
			this.dgvUsers.SelectionChanged += new System.EventHandler(this.dgvUsers_SelectionChanged);
			// 
			// userDataGridViewTextBoxColumn
			// 
			this.userDataGridViewTextBoxColumn.DataPropertyName = "User";
			this.userDataGridViewTextBoxColumn.HeaderText = "User";
			this.userDataGridViewTextBoxColumn.Name = "userDataGridViewTextBoxColumn";
			this.userDataGridViewTextBoxColumn.ReadOnly = true;
			this.userDataGridViewTextBoxColumn.Width = 200;
			// 
			// puestoDataGridViewTextBoxColumn
			// 
			this.puestoDataGridViewTextBoxColumn.DataPropertyName = "Puesto";
			this.puestoDataGridViewTextBoxColumn.HeaderText = "Puesto";
			this.puestoDataGridViewTextBoxColumn.Name = "puestoDataGridViewTextBoxColumn";
			this.puestoDataGridViewTextBoxColumn.ReadOnly = true;
			this.puestoDataGridViewTextBoxColumn.Width = 200;
			// 
			// usuarioBindingSource
			// 
			this.usuarioBindingSource.DataSource = typeof(Contabilidad.Core.Classes.Usuario);
			// 
			// buttonModify
			// 
			this.buttonModify.Location = new System.Drawing.Point(5, 38);
			this.buttonModify.Margin = new System.Windows.Forms.Padding(5);
			this.buttonModify.Name = "buttonModify";
			this.buttonModify.Size = new System.Drawing.Size(148, 23);
			this.buttonModify.TabIndex = 2;
			this.buttonModify.Text = "Modificar Usuario";
			this.buttonModify.UseVisualStyleBackColor = true;
			this.buttonModify.Click += new System.EventHandler(this.buttonModify_Click);
			// 
			// UserMenu
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(633, 450);
			this.Controls.Add(panel1);
			this.Controls.Add(this.dgvUsers);
			this.Font = new System.Drawing.Font("Segoe UI", 8.5F);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "UserMenu";
			this.Text = "Menu de Usuarios";
			panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvUsers)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.usuarioBindingSource)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dgvUsers;
		private System.Windows.Forms.DataGridViewTextBoxColumn userDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn puestoDataGridViewTextBoxColumn;
		private System.Windows.Forms.BindingSource usuarioBindingSource;
		private System.Windows.Forms.Button buttonRegister;
		private System.Windows.Forms.Button buttonDelete;
		private System.Windows.Forms.Button buttonModify;
	}
}