﻿namespace Contabilidad.Core.UI.Forms
{
    partial class BalanceGeneral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BalanceGeneral));
			this.label1 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label21 = new System.Windows.Forms.Label();
			this.txtTotalActivoCorriente = new System.Windows.Forms.TextBox();
			this.label20 = new System.Windows.Forms.Label();
			this.txtTotalActivoNoCorriente = new System.Windows.Forms.TextBox();
			this.label19 = new System.Windows.Forms.Label();
			this.txtTotalPatrimonio = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			this.txtPasivoNoCorriente = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.txtTotalPasivoCorriente = new System.Windows.Forms.TextBox();
			this.dgvPatrimonio = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.dgvPasivoNoCorriente = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label14 = new System.Windows.Forms.Label();
			this.dgvPasivoCorriente = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label13 = new System.Windows.Forms.Label();
			this.dgvActivoNoCorriente = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label12 = new System.Windows.Forms.Label();
			this.dgvActivoCorriente = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.btnRemove = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.txtAmount = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtNameAccount = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.txtLevel1 = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.txtLevel2 = new System.Windows.Forms.TextBox();
			this.txtLevel3 = new System.Windows.Forms.TextBox();
			this.txtLevel4 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.cmbTipoCuenta = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.btnGuardarBalance = new System.Windows.Forms.Button();
			this.btnModificarBalance = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvPatrimonio)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvPasivoNoCorriente)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvPasivoCorriente)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvActivoNoCorriente)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvActivoCorriente)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(487, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(162, 30);
			this.label1.TabIndex = 0;
			this.label1.Text = "Balance General";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.label21);
			this.panel1.Controls.Add(this.txtTotalActivoCorriente);
			this.panel1.Controls.Add(this.label20);
			this.panel1.Controls.Add(this.txtTotalActivoNoCorriente);
			this.panel1.Controls.Add(this.label19);
			this.panel1.Controls.Add(this.txtTotalPatrimonio);
			this.panel1.Controls.Add(this.label18);
			this.panel1.Controls.Add(this.txtPasivoNoCorriente);
			this.panel1.Controls.Add(this.label17);
			this.panel1.Controls.Add(this.txtTotalPasivoCorriente);
			this.panel1.Controls.Add(this.dgvPatrimonio);
			this.panel1.Controls.Add(this.label16);
			this.panel1.Controls.Add(this.label15);
			this.panel1.Controls.Add(this.dgvPasivoNoCorriente);
			this.panel1.Controls.Add(this.label14);
			this.panel1.Controls.Add(this.dgvPasivoCorriente);
			this.panel1.Controls.Add(this.label13);
			this.panel1.Controls.Add(this.dgvActivoNoCorriente);
			this.panel1.Controls.Add(this.label12);
			this.panel1.Controls.Add(this.dgvActivoCorriente);
			this.panel1.Controls.Add(this.label8);
			this.panel1.Controls.Add(this.label7);
			this.panel1.Controls.Add(this.btnRemove);
			this.panel1.Controls.Add(this.btnAdd);
			this.panel1.Controls.Add(this.txtAmount);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.txtNameAccount);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.label11);
			this.panel1.Controls.Add(this.txtLevel1);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.label9);
			this.panel1.Controls.Add(this.label10);
			this.panel1.Controls.Add(this.txtLevel2);
			this.panel1.Controls.Add(this.txtLevel3);
			this.panel1.Controls.Add(this.txtLevel4);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.cmbTipoCuenta);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Location = new System.Drawing.Point(22, 12);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1136, 796);
			this.panel1.TabIndex = 1;
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label21.Location = new System.Drawing.Point(364, 411);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(36, 17);
			this.label21.TabIndex = 90;
			this.label21.Text = "Total";
			// 
			// txtTotalActivoCorriente
			// 
			this.txtTotalActivoCorriente.Location = new System.Drawing.Point(413, 408);
			this.txtTotalActivoCorriente.Name = "txtTotalActivoCorriente";
			this.txtTotalActivoCorriente.Size = new System.Drawing.Size(145, 23);
			this.txtTotalActivoCorriente.TabIndex = 89;
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label20.Location = new System.Drawing.Point(364, 677);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(36, 17);
			this.label20.TabIndex = 88;
			this.label20.Text = "Total";
			// 
			// txtTotalActivoNoCorriente
			// 
			this.txtTotalActivoNoCorriente.Location = new System.Drawing.Point(413, 674);
			this.txtTotalActivoNoCorriente.Name = "txtTotalActivoNoCorriente";
			this.txtTotalActivoNoCorriente.Size = new System.Drawing.Size(145, 23);
			this.txtTotalActivoNoCorriente.TabIndex = 87;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label19.Location = new System.Drawing.Point(928, 758);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(36, 17);
			this.label19.TabIndex = 86;
			this.label19.Text = "Total";
			// 
			// txtTotalPatrimonio
			// 
			this.txtTotalPatrimonio.Location = new System.Drawing.Point(977, 755);
			this.txtTotalPatrimonio.Name = "txtTotalPatrimonio";
			this.txtTotalPatrimonio.Size = new System.Drawing.Size(145, 23);
			this.txtTotalPatrimonio.TabIndex = 85;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label18.Location = new System.Drawing.Point(928, 567);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(36, 17);
			this.label18.TabIndex = 84;
			this.label18.Text = "Total";
			// 
			// txtPasivoNoCorriente
			// 
			this.txtPasivoNoCorriente.Location = new System.Drawing.Point(977, 564);
			this.txtPasivoNoCorriente.Name = "txtPasivoNoCorriente";
			this.txtPasivoNoCorriente.Size = new System.Drawing.Size(145, 23);
			this.txtPasivoNoCorriente.TabIndex = 83;
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label17.Location = new System.Drawing.Point(928, 365);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(36, 17);
			this.label17.TabIndex = 82;
			this.label17.Text = "Total";
			// 
			// txtTotalPasivoCorriente
			// 
			this.txtTotalPasivoCorriente.Location = new System.Drawing.Point(977, 362);
			this.txtTotalPasivoCorriente.Name = "txtTotalPasivoCorriente";
			this.txtTotalPasivoCorriente.Size = new System.Drawing.Size(145, 23);
			this.txtTotalPasivoCorriente.TabIndex = 81;
			// 
			// dgvPatrimonio
			// 
			this.dgvPatrimonio.AllowUserToAddRows = false;
			this.dgvPatrimonio.AllowUserToDeleteRows = false;
			this.dgvPatrimonio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvPatrimonio.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12});
			this.dgvPatrimonio.Location = new System.Drawing.Point(578, 599);
			this.dgvPatrimonio.MultiSelect = false;
			this.dgvPatrimonio.Name = "dgvPatrimonio";
			this.dgvPatrimonio.ReadOnly = true;
			this.dgvPatrimonio.Size = new System.Drawing.Size(544, 150);
			this.dgvPatrimonio.TabIndex = 80;
			// 
			// dataGridViewTextBoxColumn10
			// 
			this.dataGridViewTextBoxColumn10.HeaderText = "Codigo";
			this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
			this.dataGridViewTextBoxColumn10.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn11
			// 
			this.dataGridViewTextBoxColumn11.HeaderText = "Cuenta";
			this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
			this.dataGridViewTextBoxColumn11.ReadOnly = true;
			this.dataGridViewTextBoxColumn11.Width = 250;
			// 
			// dataGridViewTextBoxColumn12
			// 
			this.dataGridViewTextBoxColumn12.HeaderText = "Monto";
			this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
			this.dataGridViewTextBoxColumn12.ReadOnly = true;
			this.dataGridViewTextBoxColumn12.Width = 150;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label16.Location = new System.Drawing.Point(638, 579);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(71, 17);
			this.label16.TabIndex = 79;
			this.label16.Text = "Patrimonio";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.Location = new System.Drawing.Point(832, 160);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(45, 17);
			this.label15.TabIndex = 78;
			this.label15.Text = "Pasivo";
			// 
			// dgvPasivoNoCorriente
			// 
			this.dgvPasivoNoCorriente.AllowUserToAddRows = false;
			this.dgvPasivoNoCorriente.AllowUserToDeleteRows = false;
			this.dgvPasivoNoCorriente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvPasivoNoCorriente.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
			this.dgvPasivoNoCorriente.Location = new System.Drawing.Point(578, 408);
			this.dgvPasivoNoCorriente.MultiSelect = false;
			this.dgvPasivoNoCorriente.Name = "dgvPasivoNoCorriente";
			this.dgvPasivoNoCorriente.ReadOnly = true;
			this.dgvPasivoNoCorriente.Size = new System.Drawing.Size(544, 150);
			this.dgvPasivoNoCorriente.TabIndex = 77;
			// 
			// dataGridViewTextBoxColumn7
			// 
			this.dataGridViewTextBoxColumn7.HeaderText = "Codigo";
			this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
			this.dataGridViewTextBoxColumn7.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn8
			// 
			this.dataGridViewTextBoxColumn8.HeaderText = "Cuenta";
			this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
			this.dataGridViewTextBoxColumn8.ReadOnly = true;
			this.dataGridViewTextBoxColumn8.Width = 250;
			// 
			// dataGridViewTextBoxColumn9
			// 
			this.dataGridViewTextBoxColumn9.HeaderText = "Monto";
			this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
			this.dataGridViewTextBoxColumn9.ReadOnly = true;
			this.dataGridViewTextBoxColumn9.Width = 150;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label14.Location = new System.Drawing.Point(638, 388);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(125, 17);
			this.label14.TabIndex = 76;
			this.label14.Text = "Pasivo No Corriente";
			// 
			// dgvPasivoCorriente
			// 
			this.dgvPasivoCorriente.AllowUserToAddRows = false;
			this.dgvPasivoCorriente.AllowUserToDeleteRows = false;
			this.dgvPasivoCorriente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvPasivoCorriente.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
			this.dgvPasivoCorriente.Location = new System.Drawing.Point(578, 205);
			this.dgvPasivoCorriente.MultiSelect = false;
			this.dgvPasivoCorriente.Name = "dgvPasivoCorriente";
			this.dgvPasivoCorriente.ReadOnly = true;
			this.dgvPasivoCorriente.Size = new System.Drawing.Size(544, 150);
			this.dgvPasivoCorriente.TabIndex = 75;
			// 
			// dataGridViewTextBoxColumn4
			// 
			this.dataGridViewTextBoxColumn4.HeaderText = "Codigo";
			this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
			this.dataGridViewTextBoxColumn4.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn5
			// 
			this.dataGridViewTextBoxColumn5.HeaderText = "Cuenta";
			this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
			this.dataGridViewTextBoxColumn5.ReadOnly = true;
			this.dataGridViewTextBoxColumn5.Width = 250;
			// 
			// dataGridViewTextBoxColumn6
			// 
			this.dataGridViewTextBoxColumn6.HeaderText = "Monto";
			this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
			this.dataGridViewTextBoxColumn6.ReadOnly = true;
			this.dataGridViewTextBoxColumn6.Width = 150;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label13.Location = new System.Drawing.Point(638, 185);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(103, 17);
			this.label13.TabIndex = 74;
			this.label13.Text = "Pasivo Corriente";
			// 
			// dgvActivoNoCorriente
			// 
			this.dgvActivoNoCorriente.AllowUserToAddRows = false;
			this.dgvActivoNoCorriente.AllowUserToDeleteRows = false;
			this.dgvActivoNoCorriente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvActivoNoCorriente.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
			this.dgvActivoNoCorriente.Location = new System.Drawing.Point(14, 468);
			this.dgvActivoNoCorriente.MultiSelect = false;
			this.dgvActivoNoCorriente.Name = "dgvActivoNoCorriente";
			this.dgvActivoNoCorriente.ReadOnly = true;
			this.dgvActivoNoCorriente.Size = new System.Drawing.Size(544, 200);
			this.dgvActivoNoCorriente.TabIndex = 73;
			// 
			// dataGridViewTextBoxColumn1
			// 
			this.dataGridViewTextBoxColumn1.HeaderText = "Codigo";
			this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
			this.dataGridViewTextBoxColumn1.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn2
			// 
			this.dataGridViewTextBoxColumn2.HeaderText = "Cuenta";
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			this.dataGridViewTextBoxColumn2.ReadOnly = true;
			this.dataGridViewTextBoxColumn2.Width = 250;
			// 
			// dataGridViewTextBoxColumn3
			// 
			this.dataGridViewTextBoxColumn3.HeaderText = "Monto";
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.ReadOnly = true;
			this.dataGridViewTextBoxColumn3.Width = 150;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.Location = new System.Drawing.Point(74, 448);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(123, 17);
			this.label12.TabIndex = 72;
			this.label12.Text = "Activo No Corriente";
			// 
			// dgvActivoCorriente
			// 
			this.dgvActivoCorriente.AllowUserToAddRows = false;
			this.dgvActivoCorriente.AllowUserToDeleteRows = false;
			this.dgvActivoCorriente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvActivoCorriente.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
			this.dgvActivoCorriente.Location = new System.Drawing.Point(14, 205);
			this.dgvActivoCorriente.MultiSelect = false;
			this.dgvActivoCorriente.Name = "dgvActivoCorriente";
			this.dgvActivoCorriente.ReadOnly = true;
			this.dgvActivoCorriente.Size = new System.Drawing.Size(544, 190);
			this.dgvActivoCorriente.TabIndex = 71;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "Codigo";
			this.Column1.Name = "Column1";
			this.Column1.ReadOnly = true;
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Cuenta";
			this.Column2.Name = "Column2";
			this.Column2.ReadOnly = true;
			this.Column2.Width = 250;
			// 
			// Column3
			// 
			this.Column3.HeaderText = "Monto";
			this.Column3.Name = "Column3";
			this.Column3.ReadOnly = true;
			this.Column3.Width = 150;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(74, 185);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(101, 17);
			this.label8.TabIndex = 70;
			this.label8.Text = "Activo Corriente";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(246, 160);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(43, 17);
			this.label7.TabIndex = 69;
			this.label7.Text = "Activo";
			// 
			// btnRemove
			// 
			this.btnRemove.Font = new System.Drawing.Font("Segoe UI", 8.25F);
			this.btnRemove.Location = new System.Drawing.Point(1010, 38);
			this.btnRemove.Name = "btnRemove";
			this.btnRemove.Size = new System.Drawing.Size(75, 23);
			this.btnRemove.TabIndex = 68;
			this.btnRemove.Text = "Eliminar";
			this.btnRemove.UseVisualStyleBackColor = true;
			this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAdd.Location = new System.Drawing.Point(1010, 101);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(75, 23);
			this.btnAdd.TabIndex = 67;
			this.btnAdd.Text = "Agregar";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// txtAmount
			// 
			this.txtAmount.Location = new System.Drawing.Point(782, 107);
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Size = new System.Drawing.Size(182, 23);
			this.txtAmount.TabIndex = 64;
			this.txtAmount.Text = "0";
			this.txtAmount.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(849, 77);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(47, 17);
			this.label4.TabIndex = 66;
			this.label4.Text = "Monto";
			// 
			// txtNameAccount
			// 
			this.txtNameAccount.Location = new System.Drawing.Point(402, 107);
			this.txtNameAccount.Name = "txtNameAccount";
			this.txtNameAccount.Size = new System.Drawing.Size(339, 23);
			this.txtNameAccount.TabIndex = 63;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(530, 77);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(48, 17);
			this.label5.TabIndex = 65;
			this.label5.Text = "Cuenta";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.Location = new System.Drawing.Point(246, 77);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(51, 17);
			this.label11.TabIndex = 62;
			this.label11.Text = "Codigo";
			// 
			// txtLevel1
			// 
			this.txtLevel1.Location = new System.Drawing.Point(193, 107);
			this.txtLevel1.Name = "txtLevel1";
			this.txtLevel1.Size = new System.Drawing.Size(29, 23);
			this.txtLevel1.TabIndex = 55;
			this.txtLevel1.Text = "0";
			this.txtLevel1.TextChanged += new System.EventHandler(this.txtLevel1_TextChanged);
			this.txtLevel1.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(279, 110);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(12, 15);
			this.label6.TabIndex = 61;
			this.label6.Text = "-";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(330, 110);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(12, 15);
			this.label9.TabIndex = 60;
			this.label9.Text = "-";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(228, 110);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(12, 15);
			this.label10.TabIndex = 59;
			this.label10.Text = "-";
			// 
			// txtLevel2
			// 
			this.txtLevel2.Location = new System.Drawing.Point(244, 107);
			this.txtLevel2.Name = "txtLevel2";
			this.txtLevel2.Size = new System.Drawing.Size(29, 23);
			this.txtLevel2.TabIndex = 56;
			this.txtLevel2.Text = "0";
			this.txtLevel2.TextChanged += new System.EventHandler(this.txtLevel1_TextChanged);
			this.txtLevel2.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// txtLevel3
			// 
			this.txtLevel3.Location = new System.Drawing.Point(295, 107);
			this.txtLevel3.Name = "txtLevel3";
			this.txtLevel3.Size = new System.Drawing.Size(29, 23);
			this.txtLevel3.TabIndex = 57;
			this.txtLevel3.Text = "0";
			this.txtLevel3.TextChanged += new System.EventHandler(this.txtLevel1_TextChanged);
			this.txtLevel3.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// txtLevel4
			// 
			this.txtLevel4.Location = new System.Drawing.Point(346, 107);
			this.txtLevel4.Name = "txtLevel4";
			this.txtLevel4.Size = new System.Drawing.Size(29, 23);
			this.txtLevel4.TabIndex = 58;
			this.txtLevel4.Text = "0";
			this.txtLevel4.TextChanged += new System.EventHandler(this.txtLevel1_TextChanged);
			this.txtLevel4.Validated += new System.EventHandler(this.ValidateTxt);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(45, 78);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(98, 17);
			this.label3.TabIndex = 3;
			this.label3.Text = "Tipo De Cuenta";
			// 
			// cmbTipoCuenta
			// 
			this.cmbTipoCuenta.FormattingEnabled = true;
			this.cmbTipoCuenta.Location = new System.Drawing.Point(37, 107);
			this.cmbTipoCuenta.Name = "cmbTipoCuenta";
			this.cmbTipoCuenta.Size = new System.Drawing.Size(121, 21);
			this.cmbTipoCuenta.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(150, 44);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(135, 17);
			this.label2.TabIndex = 1;
			this.label2.Text = "Datos De Movimiento";
			// 
			// btnGuardarBalance
			// 
			this.btnGuardarBalance.Location = new System.Drawing.Point(1164, 37);
			this.btnGuardarBalance.Name = "btnGuardarBalance";
			this.btnGuardarBalance.Size = new System.Drawing.Size(107, 36);
			this.btnGuardarBalance.TabIndex = 2;
			this.btnGuardarBalance.Text = "Guardar Balance";
			this.btnGuardarBalance.UseVisualStyleBackColor = true;
			this.btnGuardarBalance.Click += new System.EventHandler(this.btnGuardarPartida_Click);
			// 
			// btnModificarBalance
			// 
			this.btnModificarBalance.Location = new System.Drawing.Point(1164, 155);
			this.btnModificarBalance.Name = "btnModificarBalance";
			this.btnModificarBalance.Size = new System.Drawing.Size(107, 53);
			this.btnModificarBalance.TabIndex = 3;
			this.btnModificarBalance.Text = "Eliminar, Modificar e Imprimir";
			this.btnModificarBalance.UseVisualStyleBackColor = true;
			this.btnModificarBalance.Click += new System.EventHandler(this.btnModificarBalance_Click);
			// 
			// BalanceGeneral
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1299, 809);
			this.Controls.Add(this.btnModificarBalance);
			this.Controls.Add(this.btnGuardarBalance);
			this.Controls.Add(this.panel1);
			this.Font = new System.Drawing.Font("Segoe UI", 8.5F);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "BalanceGeneral";
			this.Text = "Balance General";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvPatrimonio)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvPasivoNoCorriente)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvPasivoCorriente)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvActivoNoCorriente)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvActivoCorriente)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbTipoCuenta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtLevel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtLevel2;
        private System.Windows.Forms.TextBox txtLevel3;
        private System.Windows.Forms.TextBox txtLevel4;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNameAccount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgvPatrimonio;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView dgvPasivoNoCorriente;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView dgvPasivoCorriente;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView dgvActivoNoCorriente;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView dgvActivoCorriente;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtTotalActivoCorriente;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtTotalActivoNoCorriente;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtTotalPatrimonio;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPasivoNoCorriente;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtTotalPasivoCorriente;
        private System.Windows.Forms.Button btnGuardarBalance;
        private System.Windows.Forms.Button btnModificarBalance;
    }
}