﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad.Core.UI.Forms
{
	public partial class MainMenu : Form
	{

		public MainMenu()
		{
			InitializeComponent();
			Shown += Loaded;
			FormClosed += Unloaded;
		}

		private async void Loaded(object sender, EventArgs e)
		{
			await Init();
		}

		private void Unloaded(object sender, EventArgs e)
		{
			Dispo();
		}

		private async Task Init()
		{
			await Task.Delay(250);

			var m = Program.getManager();
			await Task.Run(() => m.Init());
			var um = m.GetUserManager();

			waitingText.Text = "Esperando al usuario...";

			if (um.GetUsers().Count == 0)
				new RegisterUser(null).ShowDialog();
			else
				new VentanaLogin().ShowDialog();

			if (!um.IsLoggedIn())
				Close();

			mainPanel.Visible = true;
			loadPanel.Visible = false;
		}

		private void Dispo()
		{
			var m = Program.getManager();
			m.Dispose();
		}

		private void buttonDiario_Click(object sender, EventArgs e)
		{
			new LibroDiario().Show();
		}

		private void buttonMayor_Click(object sender, EventArgs e)
		{
			new LibroMayor().Show();
		}

		private void buttonSaldos_Click(object sender, EventArgs e)
		{
			new BalanceDeSaldos().Show();
		}

		private void buttonNomenclatura_Click(object sender, EventArgs e)
		{
			new Nomenclatura().Show();
		}

        private void buttonPartidas_Click(object sender, EventArgs e)
        {
            new PartidasContables().Show();
        }

		private void buttonPolizas_Click(object sender, EventArgs e)
		{
			new PolizasContables().Show();
		}

		private void buttonBalanceGeneral_Click(object sender, EventArgs e)
		{
			new BalanceGeneral().Show();
		}
	}
}
