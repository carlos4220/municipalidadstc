﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Managers
{
	public class DepartureManager : IManager
	{

		private BindingList<Accounting_Party> departures;

		private Manager m;

		public DepartureManager(Manager m)
		{
			this.m = m;
			departures = new BindingList<Accounting_Party>();
		}

		public void Load(string path)
		{
			if (File.Exists(path + "Departures.txt"))
				using (StreamReader file = new StreamReader(path + "Departures.txt"))
				{
					Accounting_Party p = null;
					AccountingMovement m = null;
					String line;
					int mode = 0;
					while ((line = file.ReadLine()) != null)
					{
						if (mode == 0 && line == "departure:{")
						{
							p = new Accounting_Party();
							mode = 1;
						}
						{
							if (mode == 1 && line == "\tdebe:{")
								mode = 2;
							if (mode == 2 && line == "\t\tmovimiento:{")
							{
								m = new AccountingMovement();
								mode = 3;
							}
							if (mode == 3 && line == "\t\t}")
							{
								mode = 2;
								p.Debe.Add(m);
							}
							if (mode == 2 && line == "\t}")
								mode = 1;
						}
						{
							if (mode == 1 && line == "\thaber:{")
								mode = 4;
							if (mode == 4 && line == "\t\tmovimiento:{")
							{
								m = new AccountingMovement();
								mode = 5;
							}
							if (mode == 5 && line == "\t\t}")
							{
								mode = 4;
								p.Haber.Add(m);
							}
							if (mode == 4 && line == "\t}")
								mode = 1;
						}
						if (mode == 1 && line == "}")
						{
							departures.Add(p);
							mode = 0;
						}

						switch (mode)
						{
							case 1: // Estamos en modo de leer la partida
								if (line.StartsWith("\tdate:"))
									p.Date = DateTime.Parse(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\tnumber:"))
									p.Number = int.Parse(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\tdescription:"))
									p.Description = line.Substring(line.IndexOf(":") + 1);
								break;
							case 3:
								if (line.StartsWith("\t\t\tdate:"))
									m.Date = DateTime.Parse(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\t\t\tcode:"))
									m.Account = this.m.GetNomenclatureManager().GetAccount(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\t\t\tdebe:"))
									m.Debe = decimal.Parse(line.Substring(line.IndexOf(":") + 1));
								break;
							case 5:
								if (line.StartsWith("\t\t\tdate:"))
									m.Date = DateTime.Parse(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\t\t\tcode:"))
									m.Account = this.m.GetNomenclatureManager().GetAccount(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\t\t\thaber:"))
									m.Haber = decimal.Parse(line.Substring(line.IndexOf(":") + 1));
								break;
						}
					}
				}
		}

		public void Save(string path)
		{
			using (StreamWriter file = new StreamWriter(path + "Departures.txt"))
				foreach (var d in departures)
				{
					file.WriteLine("departure:{");
					file.WriteLine($"\tdate:{d.Date}");
					file.WriteLine($"\tnumber:{d.Number}");
					file.WriteLine($"\tdescription:{d.Description}");
					{
						file.WriteLine("\tdebe:{");
						foreach (var m in d.Debe)
						{
							file.WriteLine("\t\tmovimiento:{");
							file.WriteLine($"\t\t\tdate:{m.Date}");
							file.WriteLine($"\t\t\tcode:{m.Account.Code}");
							file.WriteLine($"\t\t\tdebe:{m.Debe}");
							file.WriteLine("\t\t}");
						}
						file.WriteLine("\t}");
					}
					{
						file.WriteLine("\thaber:{");
						foreach (var m in d.Haber)
						{
							file.WriteLine("\t\tmovimiento:{");
							file.WriteLine($"\t\t\tdate:{m.Date}");
							file.WriteLine($"\t\t\tcode:{m.Account.Code}");
							file.WriteLine($"\t\t\thaber:{m.Haber}");
							file.WriteLine("\t\t}");
						}
						file.WriteLine("\t}");
					}
					file.WriteLine("}");
				}
		}

		public ref BindingList<Accounting_Party> GetDepartures() => ref departures;
	}
}
