﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Managers
{
	public class Manager
	{

		private List<IManager> managers;

		private String path;

		private UserManager userManager;
		private NomenclatureManager nomenclatureManager;
		private DepartureManager departureManager;
		private PolicyManager policyManager;
		private BalanceManager balanceManager;

		public Manager(String path)
		{
			this.path = path;
			managers = new List<IManager>();
		}

		public void Init()
		{
			if (!Directory.Exists(path + "\\Data"))
				Directory.CreateDirectory(path + "\\Data");

			// Construir managers
			userManager = new UserManager(this);
			nomenclatureManager = new NomenclatureManager(this);
			departureManager = new DepartureManager(this);
			policyManager = new PolicyManager(this);
			balanceManager = new BalanceManager(this);

			// Agregar Managers
			managers.Add(userManager);
			managers.Add(nomenclatureManager);
			managers.Add(departureManager);
			managers.Add(policyManager);
			managers.Add(balanceManager);

			// Cargar managers
			foreach (var m in managers)
				m.Load(path + "\\Data\\");
		}

		public void Dispose()
		{
			// Guardar managers
			foreach (var m in managers)
				m.Save(path + "\\Data\\");
		}

		public ref UserManager GetUserManager() => ref userManager;

		public ref NomenclatureManager GetNomenclatureManager() => ref nomenclatureManager;

		public ref DepartureManager GetDepartureManager() => ref departureManager;

		public ref PolicyManager GetPolicyManager() => ref policyManager;

		public ref BalanceManager GetBalanceManager() => ref balanceManager;
	}
}
