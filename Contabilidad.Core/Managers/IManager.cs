﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Managers
{
	interface IManager
	{

		void Load(String path);

		void Save(String path);
	}
}
