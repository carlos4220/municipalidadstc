﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Managers
{
	public class BalanceManager : IManager
	{
		private BindingList<BalanceSheet> balances;

		private Manager m;

		public BalanceManager(Manager m)
		{
			this.m = m;
			balances = new BindingList<BalanceSheet>();
		}

		public void Load(string path)
		{
			if (File.Exists(path + "Balances.txt"))
				using (StreamReader file = new StreamReader(path + "Balances.txt"))
				{
					BalanceSheet b = null;
					MovementBalance m = null;
					String line;
					int mode = 0;
					while ((line = file.ReadLine()) != null)
					{
						if (mode == 0 && line == "balance:{")
						{
							b = new BalanceSheet();
							mode = 1;
						}
						{
							if (mode == 1 && line == "\tmovimientos:{")
								mode = 2;
							if (mode == 2 && line == "\t\tmovimiento:{")
							{
								m = new MovementBalance();
								mode = 3;
							}
							if (mode == 3 && line == "\t\t}")
							{
								mode = 2;
								b.Movement.Add(m);
							}
							if (mode == 2 && line == "\t}")
								mode = 1;
						}
						if (mode == 1 && line == "}")
						{
							balances.Add(b);
							mode = 0;
						}

						switch (mode)
						{
							case 1: // Estamos en modo de leer la poliza
								if (line.StartsWith("\tdate:"))
									b.Date = DateTime.Parse(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\tnumber:"))
									b.Number = int.Parse(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\tdescription:"))
									b.Description = line.Substring(line.IndexOf(":") + 1);
								break;
							case 3:
								if (line.StartsWith("\t\t\tdate:"))
									m.Date = DateTime.Parse(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\t\t\tcode:"))
									m.Account = this.m.GetNomenclatureManager().GetAccount(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\t\t\tsaldo:"))
									m.Saldo = decimal.Parse(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\t\t\ttipo:"))
									m.Type = (BalanceAccountType)Enum.Parse(typeof(BalanceAccountType), line.Substring(line.IndexOf(":") + 1));
								break;
						}
					}
				}
		}

		public void Save(string path)
		{
			using (StreamWriter file = new StreamWriter(path + "Balances.txt"))
				foreach (var d in balances)
				{
					file.WriteLine("balance:{");
					file.WriteLine($"\tdate:{d.Date}");
					file.WriteLine($"\tnumber:{d.Number}");
					file.WriteLine($"\tdescription:{d.Description}");
					{
						file.WriteLine("\tmovimientos:{");
						foreach (var m in d.Movement)
						{
							file.WriteLine("\t\tmovimiento:{");
							file.WriteLine($"\t\t\tdate:{m.Date}");
							file.WriteLine($"\t\t\tcode:{m.Account.Code}");
							file.WriteLine($"\t\t\tsaldo:{m.Saldo}");
							file.WriteLine($"\t\t\ttipo:{m.Type}");
							file.WriteLine("\t\t}");
						}
						file.WriteLine("\t}");
					}
					file.WriteLine("}");
				}
		}

		public ref BindingList<BalanceSheet> GetBalances() => ref balances;
	}
}
