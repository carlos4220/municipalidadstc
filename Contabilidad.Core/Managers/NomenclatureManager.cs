﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Managers
{
	public class NomenclatureManager : IManager
	{

		private BindingList<Account> accounts;

		public NomenclatureManager(Manager m)
		{
			accounts = new BindingList<Account>();
		}

		public void Load(string path)
		{
			if (File.Exists(path + "Nomenclature.txt"))
				using (StreamReader file = new StreamReader(path + "Nomenclature.txt"))
				{
					String line;
					while ((line = file.ReadLine()) != null)
					{
						String[] lines = line.Split('|');

						AccountType type;
						Enum.TryParse<AccountType>(lines[0], out type);

						accounts.Add(new Account()
						{
							Type = type,
							Code = lines[1],
							Description = lines[2]
						});
					}
				}
		}

		public void Save(string path)
		{
			using (StreamWriter file = new StreamWriter(path + "Nomenclature.txt"))
				foreach (var u in accounts)
					file.WriteLine($"{u.Type}|{u.Code}|{u.Description}");
		}

		public AccountError AddAccount(AccountType type, String code, String description)
		{

			// Check if exist
			foreach (var a in accounts)
				if (a.Code == code)
					return AccountError.EXIST;

			accounts.Add(new Account
			{
				Code = code,
				Type = type,
				Description = description
			});

			return AccountError.OK;
		}

		public AccountError RemoveAccount(Account acc)
		{
			accounts.Remove(acc);
			return AccountError.OK;
		}

		public Account GetAccount(String code)
		{
			foreach (var a in accounts)
				if (a.Code == code)
					return a;
			return null;
		}

		public ref BindingList<Account> GetAccounts() => ref accounts;
	}

	public enum AccountError
	{
		OK, EXIST, NOT_FOUND
	}
}
