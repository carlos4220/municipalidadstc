﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Managers
{
	public class PolicyManager : IManager
	{
		private BindingList<AccountingPolicies> accountingPolicies;

		private Manager m;

		public PolicyManager(Manager m)
		{
			this.m = m;
			accountingPolicies = new BindingList<AccountingPolicies>();
		}

		public void Load(string path)
		{
			if (File.Exists(path + "Policy.txt"))
				using (StreamReader file = new StreamReader(path + "Policy.txt"))
				{
					AccountingPolicies p = null;
					AccountingMovement m = null;
					String line;
					int mode = 0;
					while ((line = file.ReadLine()) != null)
					{
						if (mode == 0 && line == "policy:{")
						{
							p = new AccountingPolicies();
							mode = 1;
						}
						{
							if (mode == 1 && line == "\tdebe:{")
								mode = 2;
							if (mode == 2 && line == "\t\tmovimiento:{")
							{
								m = new AccountingMovement();
								mode = 3;
							}
							if (mode == 3 && line == "\t\t}")
							{
								mode = 2;
								p.Debe.Add(m);
							}
							if (mode == 2 && line == "\t}")
								mode = 1;
						}
						{
							if (mode == 1 && line == "\thaber:{")
								mode = 4;
							if (mode == 4 && line == "\t\tmovimiento:{")
							{
								m = new AccountingMovement();
								mode = 5;
							}
							if (mode == 5 && line == "\t\t}")
							{
								mode = 4;
								p.Haber.Add(m);
							}
							if (mode == 4 && line == "\t}")
								mode = 1;
						}
						if (mode == 1 && line == "}")
						{
							accountingPolicies.Add(p);
							mode = 0;
						}

						switch (mode)
						{
							case 1: // Estamos en modo de leer la poliza
								if (line.StartsWith("\tdate:"))
									p.Date = DateTime.Parse(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\tnumber:"))
									p.Number = int.Parse(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\tdescription:"))
									p.Description = line.Substring(line.IndexOf(":") + 1);
								if (line.StartsWith("\ttype:"))
									p.Type = line.Substring(line.IndexOf(":") + 1);
								if(line.StartsWith("\tauthorizer"))
									p.Authorizer = line.Substring(line.IndexOf(":") + 1);
								if (line.StartsWith("\tdeveloper"))
									p.Developer = line.Substring(line.IndexOf(":") + 1);
								break;
							case 3:
								if (line.StartsWith("\t\t\tdate:"))
									m.Date = DateTime.Parse(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\t\t\tcode:"))
									m.Account = this.m.GetNomenclatureManager().GetAccount(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\t\t\tdebe:"))
									m.Debe = decimal.Parse(line.Substring(line.IndexOf(":") + 1));
								break;
							case 5:
								if (line.StartsWith("\t\t\tdate:"))
									m.Date = DateTime.Parse(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\t\t\tcode:"))
									m.Account = this.m.GetNomenclatureManager().GetAccount(line.Substring(line.IndexOf(":") + 1));
								if (line.StartsWith("\t\t\thaber:"))
									m.Haber = decimal.Parse(line.Substring(line.IndexOf(":") + 1));
								break;
						}
					}
				}
		}

		public void Save(string path)
		{
			using (StreamWriter file = new StreamWriter(path + "Policy.txt"))
				foreach (var d in accountingPolicies)
				{
					file.WriteLine("policy:{");
					file.WriteLine($"\tdate:{d.Date}");
					file.WriteLine($"\tnumber:{d.Number}");
					file.WriteLine($"\tdescription:{d.Description}");
					file.WriteLine($"\ttype:{d.Type}");
					file.WriteLine($"\tauthorizer:{d.Authorizer}");
					file.WriteLine($"\tdeveloper:{d.Developer}");
					{
						file.WriteLine("\tdebe:{");
						foreach (var m in d.Debe)
						{
							file.WriteLine("\t\tmovimiento:{");
							file.WriteLine($"\t\t\tdate:{m.Date}");
							file.WriteLine($"\t\t\tcode:{m.Account.Code}");
							file.WriteLine($"\t\t\tdebe:{m.Debe}");
							file.WriteLine("\t\t}");
						}
						file.WriteLine("\t}");
					}
					{
						file.WriteLine("\thaber:{");
						foreach (var m in d.Haber)
						{
							file.WriteLine("\t\tmovimiento:{");
							file.WriteLine($"\t\t\tdate:{m.Date}");
							file.WriteLine($"\t\t\tcode:{m.Account.Code}");
							file.WriteLine($"\t\t\thaber:{m.Haber}");
							file.WriteLine("\t\t}");
						}
						file.WriteLine("\t}");
					}
					file.WriteLine("}");
				}
		}

		public ref BindingList<AccountingPolicies> GetAccountingPolicies() => ref accountingPolicies;
	}
}
