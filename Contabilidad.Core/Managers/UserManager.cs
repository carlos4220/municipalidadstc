﻿using Contabilidad.Core.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Managers
{
	public class UserManager : IManager
	{
		private BindingList<Usuario> users;

		private Usuario current;

		private Boolean loggedIn;

		public UserManager(Manager m)
		{
			users = new BindingList<Usuario>();
		}

		public void Load(String path)
		{
			if (File.Exists(path + "Users.txt"))

				using (StreamReader file = new StreamReader(path + "Users.txt"))
				{
					String line;
					while ((line = file.ReadLine()) != null)
					{
						String[] lines = line.Split('|');

						UserPosition pos;
						Enum.TryParse<UserPosition>(lines[2], out pos);

						users.Add(new Usuario()
						{
							User = lines[0],
							Password = lines[1],
							Puesto = pos
						});
					}
				}
		}

		public void Save(String path)
		{
			using (StreamWriter file = new StreamWriter(path + "Users.txt"))
				foreach (var u in users)
					file.WriteLine($"{u.User}|{u.Password}|{u.Puesto}");
		}

		public RegisterErrors Login(String user, String password)
		{
			if (user == String.Empty)
				return RegisterErrors.EMPTY_USER;
			if (password == String.Empty)
				return RegisterErrors.EMPTY_PASSWORD;

			Usuario u = null;
			foreach (var us in users)
			{
				if (us.User == user)
				{
					u = us;
					break;
				}
			}

			if (u == null)
				return RegisterErrors.MISMATCH_USER;
			if (u.Password != password)
				return RegisterErrors.MISMATCH_PASSWORD;

			current = u;
			loggedIn = true;
			return RegisterErrors.OK;
		}

		public RegisterErrors Register(String user, String password, String password2, UserPosition position)
		{
			if (user == String.Empty)
				return RegisterErrors.EMPTY_USER;
			if (password == String.Empty || password2 == String.Empty)
				return RegisterErrors.EMPTY_PASSWORD;
			if (password != password2)
				return RegisterErrors.MISMATCH_PASSWORD;

			foreach (var us in users)
				if (us.User == user)
					return RegisterErrors.EXIST;

			users.Add(new Usuario()
			{
				User = user,
				Password = password,
				Puesto = position
			});

			return RegisterErrors.OK;
		}

		public RegisterErrors Modify(String user, String password, String password2, UserPosition position)
		{
			if (user == String.Empty)
				return RegisterErrors.EMPTY_USER;
			if (password == String.Empty || password2 == String.Empty)
				return RegisterErrors.EMPTY_PASSWORD;
			if (password != password2)
				return RegisterErrors.MISMATCH_PASSWORD;

			foreach (var us in users)
				if (us.User == user)
				{
					us.User = user;
					us.Password = password;
					us.Puesto = position;
				}

			return RegisterErrors.OK;
		}

		public void RemoveUser(Usuario u)
		{
			users.Remove(u);
		}

		public Boolean IsLoggedIn() => loggedIn;

		public ref Usuario GetCurrentUser() => ref current;

		public ref BindingList<Usuario> GetUsers() => ref users;

	}

	public enum RegisterErrors
	{
		OK, EMPTY_USER, EMPTY_PASSWORD, MISMATCH_PASSWORD, MISMATCH_USER, EXIST
	}
}
