﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Classes
{
	public class Usuario
	{
		public string User { get; set; }
		public string Password { get; set; }
		public UserPosition Puesto { get; set; }

	}

	public enum UserPosition { Colaborador, Contabilidad, Administrador };
}
