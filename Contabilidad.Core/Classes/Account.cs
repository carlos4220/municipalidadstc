﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Classes
{
	public class Account
	{
		public AccountType Type { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
	}

	public enum AccountType
	{
		Activos, Pasivos, Capital, Ingresos, Costos,
		Egresos, Otros
	}
}
