﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Classes
{
    public class Accounting_Party:AccountingProcesses
    {
		public List<AccountingMovement> Debe = new List<AccountingMovement>();
		public List<AccountingMovement> Haber = new List<AccountingMovement>();

	}

	public enum TypeBalance
	{
		Debe, Haber
	}
}
