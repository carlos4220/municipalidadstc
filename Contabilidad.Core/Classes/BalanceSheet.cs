﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Classes
{
	public class BalanceSheet : AccountingProcesses
	{
		public List<MovementBalance> Movement { get; set; } = new List<MovementBalance>();

		public void AgregarMovimientos(List<MovementBalance> Movements)
		{
            
			foreach (MovementBalance Value in Movements)//agrega primero los movimientos de activo corriente
				if (Value.Type == BalanceAccountType.ActivoCorriente)
					Movement.Add(Value);

			foreach (MovementBalance Value in Movements)//segundo los movimientos de activo no corriente
				if (Value.Type == BalanceAccountType.ActivoNoCorriente)
					Movement.Add(Value);

			foreach (MovementBalance Value in Movements)//tercero los de pasivo corriente
				if (Value.Type == BalanceAccountType.PasivoCorriente)
					Movement.Add(Value);

			foreach (MovementBalance Value in Movements)//cuarto los de pasivo no corriente
				if (Value.Type == BalanceAccountType.PasivoNoCorriente)
					Movement.Add(Value);

			foreach (MovementBalance Value in Movements)//quinto los de pasivo no corriente
				if (Value.Type == BalanceAccountType.Patrimonio)
					Movement.Add(Value);
		}
	}
	public enum BalanceAccountType
	{
		ActivoCorriente,
		ActivoNoCorriente,
		PasivoCorriente,
		PasivoNoCorriente,
		Patrimonio
	}
}
