﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Classes
{
    public class AccountingPolicies:AccountingProcesses
    {
		public List<AccountingMovement> Debe = new List<AccountingMovement>();
		public List<AccountingMovement> Haber = new List<AccountingMovement>();

		public string Authorizer { get; set; }
        public string Developer { get; set; }
    }

	public enum BalanceType
	{
		Deudor, Acreedor
	}
	public enum PolicyType
	{
		Ingresos, Egresos, Diario
	}
}
