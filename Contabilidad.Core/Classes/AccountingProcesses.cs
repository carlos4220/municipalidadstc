﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Classes
{
    public class AccountingProcesses
    {
        public DateTime Date { get; set; }
        public int Number { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
    }
}
