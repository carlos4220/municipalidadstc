﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Classes
{
    public class Movement
    {
        public DateTime Date { get; set; }
        public Account Account { get; set; } = new Account();
    }

    public class AccountingMovement:Movement
    {
        public decimal Debe { get; set; } = 0;
        public decimal Haber { get; set; } = 0;
    }

    public class MovementBalance:Movement
    {
        public decimal Saldo { get; set; } = 0;
        public BalanceAccountType Type { get; set; } 
    }
}
